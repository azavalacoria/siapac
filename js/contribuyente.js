$('#buscar').click(function () {
	if ($('#apellidopaterno').val().length > 1) {
		$('#list-errors').html('');
		buscar_ciudadano($('#apellidopaterno').val(), $('#apellidomaterno').val(), $('#nombres').val(), 0);
	} else{
		$('#list-errors').html('<li>El apellido paterno debe tener al menos 2 caracteres</li>');
	};
	
});

$('#sig').click(function () {
	var limite = $('#table-grid tr').size();
	buscar_ciudadano($('#apellidopaterno').val(), $('#apellidomaterno').val(), $('#nombres').val(), limite);
});

$('#sel').click(function () {
	//$('#sel').attr('type','submit');
	//$('#selecciona-ciudadano').submit();
	var checked = $('input:radio[name="clave"]:checked').length;
	if (checked != 1) {
		alert('Debes elegir una persona del listado');
	} else{
		//alert('Has elegido una persona del listado: ' + $('input:radio[name="clave"]:checked').val());
		$('#sel').attr('type','submit');
		$('#selecciona-ciudadano').submit();
	};
});

function buscar_ciudadano (apellidopaterno, apellidomaterno, nombres, inicio) {
	$.ajax({
		url: $('#siteurl').val() + "/obtener_ciudadanos",
		async: false,
		type: "POST",
		data: {apellidopaterno: apellidopaterno, apellidomaterno: apellidomaterno, nombres: nombres, inicio: inicio},
		dataType: "json",
		success: function (datos) {
			if(datos.ciudadanos>0){
				$('#table-grid').empty();
				agregar_cabecera();
				$.each(datos.ciudadanos, function (i, c) {
					agregar_fila(c.id, c.apellidopaterno, c.apellidomaterno, c.nombres, c.edad, c.colonia, c.calle, c.numero);
				});
				$('#report-box').fadeIn(800);
			}else{
				$('#head-sale').first('h3').html("<div id='info-ctb'>"+
					"<div class='form-row'><h3>No se encontró a ese contribuyente.</h3>"+
					"<a href='nuevo_contribuyente/agregar'>"+
					"¿Desea agregarlo?</a></div>"+
					"</div>");
				$('#form-box').hide('normal');
			}
		}
	});
}

function agregar_cabecera () {
	html = '<tr>' +
				'<th>' + 'Opción' + '</th>' +
				'<th>' + 'Apellido Paterno' + '</th>' + 
				'<th>' + 'Apellido Materno' + '</th>' +
				'<th>' + 'Nombres'			+ '</th>' +
				'<th>' + 'Edad'				+ '</th>' +
				'<th>' + 'Colonia'			+ '</th>' +
				'<th>' + 'Calle'			+ '</th>' +
				'<th>' + 'Número'			+ '</th>' +
			'</tr>';
	$('#table-grid').append(html);
}

function agregar_fila (id, apellidopaterno, apellidomaterno, nombres, edad, colonia, calle, numero) {
	html = '<tr>' +
				'<td>' + '<input name="clave" type="radio" group="ciudadanos" value="' + id + '">' + '</td>' +
				'<td>' + apellidopaterno + '</td>' + 
				'<td>' + apellidomaterno + '</td>' +
				'<td>' + nombres		 + '</td>' +
				'<td>' + edad			 + '</td>' +
				'<td>' + colonia		 + '</td>' +
				'<td>' + calle			 + '</td>' +
				'<td>' + numero			 + '</td>' +
			'</tr>';
	$('#table-grid').append(html);
}