$('#pagar').click(function () {
	confirmacion = confirm("¿Desea cobrar este recibo?");
	if (confirmacion) {
		accion = $('#pagar').val();
		$.ajax({
			url: $('#siteurl').val(),
			async: false,
			type: "POST",
			data: {accion: accion},
			dataType: "json",
			success: function (datos) {
				if (datos.errores == 0) {
					//imprimir_recibo(datos.folio);
					$('#list-errors').html('<li>Recibo cobrado con éxito</li>');
					$('#form-comp').fadeOut(400);
					window.location = $('#siteurl').val() + '/imprimir';
				};
			}
		});
	};
});

function imprimir_recibo (folio) {
	$.ajax({
		url: $('#siteurl').val() + "/imprimir",
		async: false,
		type: "POST",
		data: {folio: folio},
		dataType: "json",
		success: function (datos) {
			if (datos.errores == 0) {
				$('#form-comp').fadeOut(400);
				imprimir_recibo(datos.folio);
			};
		}
	});
}

$('#cancelar').click(function () {
	accion = $('#cancelar').val();
	$.ajax({
		url: $('#siteurl').val(),
		async: false,
		type: "POST",
		data: {accion: accion},
		dataType: "json",
		success: function (datos) {
			//
			if (datos.errores == 0) {
				$('#form-comp').fadeOut(400);
				$('#list-errors').fadeOut(800);
				$('#list-errors').html('<li>El recibo ha sido cancelado exitosamente.</li>');
				$('#list-errors').fadeIn(800);
			};
		}
	});
});