<?php
/**
* 
*/
class Tarifa extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_precio($periodo, $zona, $tiposervicio)
	{
		$periodo = substr($periodo, 0, 4);
		$this->db->select('precio,nombreservicio');
		$this->db->from('tarifas');
		$this->db->join('servicios','tiposervicio=idservicio');
		$this->db->where('zona', $zona);
		$this->db->where('tiposervicio', $tiposervicio);
		$this->db->like('periodo', $periodo);
		$query = $this->db->get();
		$datos = array();
		foreach ($query->result() as $dato) {
			array_push($datos, $dato->precio , $dato->nombreservicio);
		}
		return $datos;
	}

	public function agregar_tarifa($datos)
	{
		$this->db->insert('tarifas', $datos);
	}

	public function existe_tarifa($zona, $tiposervicio, $iniciovigencia, $finalvigencia)
	{
		$this->db->where("zona = $zona and tiposervicio = $tiposervicio and 
			('$iniciovigencia' between iniciovigencia and finalvigencia or
			 '$finalvigencia' between iniciovigencia and finalvigencia)");
		$this->db->from('tarifas');
		return $this->db->count_all_results();
	}

	public function existe_tarifa_anual($zona, $tiposervicio, $iniciovigencia, $finalvigencia, $cobroalcontratar)
	{
		$this->db->where("zona = $zona and cobroalcontratar = $cobroalcontratar
			and tiposervicio = $tiposervicio and 
			('$iniciovigencia' between iniciovigencia and finalvigencia or
			 '$finalvigencia' between iniciovigencia and finalvigencia)");
		$this->db->from('tarifas');
		return $this->db->count_all_results();
	}

	public function obtener_cargos_contrato($zona)
	{
		$fecha = date("Y-m-d");
		$this->db->select('nombreservicio, precio');
		$this->db->from('tarifas');
		$this->db->join('servicios','tiposervicio=idservicio');
		$this->db->where('cobroalcontratar', 1);
		$this->db->where('zona', $zona);
		//$this->db->where(" cobroalcontratar is true and $fecha between iniciovigencia and finalvigencia");
		//
		$this->db->where('iniciovigencia <=', "$fecha");
		$this->db->where('finalvigencia >=', "$fecha");
		
		$query = $this->db->get();
		return $query->result();
	}
}
?>