<?php
/**
* 
*/
class Recibo extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function crear_recibo($contrato, $modulo, $usuario)
	{
		$fecha = date_format(new DateTime(),'Y-m-d H:m:s');
		$datos = array('contrato'=>$contrato,'fechacobro'=>$fecha, 'modulo'=>$modulo, 'usuario'=>$usuario);
		$this->db->insert('recibos', $datos);
		$this->db->select('folio');
		$this->db->where(array('fechacobro'=>$fecha , 'contrato' => $contrato));
		$query = $this->db->get('recibos');
		$folio = 0;
		foreach ($query->result() as $recibo) {
			$folio = $recibo->folio;
		}
		return $folio;
	}

	function obtener_descuento($folio){
		$this->db->select('descuento');
		$this->db->where('folio',$folio);
		$query = $this->db->get('recibos');
		$descuento = 0.0;
		foreach ($query->result() as $v) {
			$descuento = $v->descuento;
		}
		return $descuento;
	}

	function cerrar_cobro($datos,$folio)
	{
		$this->db->where('folio',$folio);
		$this->db->update('recibos',$datos);
	}

	public function cancelar($folio)
	{
		$this->db->where('folio',$folio);
		$this->db->update('recibos', array('observaciones'=>'Cancelado por el cajero'));
	}

	public function agregar_descuento($datos, $folio)
	{
		$this->db->where('folio', $folio);
		$this->db->update('recibos',$datos);

	}
}
?>