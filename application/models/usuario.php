<?php
/**
* 
*/
class Usuario extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function existe($nickname, $password)
	{
		$this->db->where('nickname', $nickname);
		$this->db->where('password', $password);
		$this->db->from('usuarios');
		return $this->db->count_all_results();
	}

	public function obtener_permisos($nickname)
	{
		$this->db->select('idusuario, rol, menu');
		$this->db->from('usuarios');
		$this->db->join('roles','rol=idrol');
		$this->db->where('nickname',$nickname);
		$query = $this->db->get();

		foreach ($query->result() as $usuario) {
			$datos = array(
					'idusuario' => $usuario->idusuario,
					'rol' => $usuario->rol,
					'menu' => $usuario->menu
				);
		}
		return $datos;
	}

	public function agregar_usuario($datos)
	{
		$this->db->insert('usuarios', $datos);
	}

	public function obtener_usuarios()
	{
		$this->db->select('idusuario, nombrecompleto, nickname');
		$query = $this->db->get('usuarios');
		return $query->result();
	}

	public function obtener_usuario($idusuario)
	{
		$this->db->select('nombrecompleto, nickname, rol');
		$this->db->where('idusuario', $idusuario);
		$query = $this->db->get('usuarios');
		$usuario = null;
		foreach ($query->result() as $u) {
			$usuario = array('nombre' => $u->nombrecompleto,
				'nickname' => $u->nickname,
				'rol' => $u->rol,
				'idusuario' => $idusuario
				);
		}
		return $usuario;
	}

	public function actualizar_usuario($datos , $idusuario)
	{
		$this->db->where('idusuario',$idusuario);
		$this->db->update('usuarios', $datos);
	}
}
?>