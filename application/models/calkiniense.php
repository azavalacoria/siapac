<?php
/**
* 
*/
class Ciudadano extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function buscar_ciudadano($apellidopaterno, $apellidomaterno, $nombre, $base, $limite)
	{
		$this->db->select('cve_elect, nombre, apellidopaterno, apellidomaterno, fec_nac, colonia, calle, num_b');
		$this->db->like('apellidopaterno',$apellidopaterno);
		$this->db->like('apellidomaterno',$apellidomaterno);
		$this->db->like('nombre', $nombre);
		$query = $this->db->get('calkinienses',$base, $limite);
		return $query;
	}

	public function obtener_ciudadano($claveelector)
	{
		$this->db->select('cve_elect, nombre, apellidopaterno, apellidomaterno, fec_nac, colonia, calle, num_b');
		$this->db->where( 'cve_elect',$claveelector);
		$query = $this->db->get('calkinienses');
	}
}
?>