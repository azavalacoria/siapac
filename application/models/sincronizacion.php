<?php
/**
* 
*/
class Sincronizacion extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_ultima_sincronizacion()
	{
		$this->db->select('periodofinal');
		$this->db->from('sincronizaciones');
		$this->db->order_by('periodofinal','DESC');
		$this->db->limit(1, 0);
		$resultados = $this->db->count_all_results();
		if ($resultados > 0) {
			$query = $this->db->get('sincronizaciones');
			$periodo = "";
			foreach ($query->result() as $r) {
				$periodo = $r->periodofinal;
			}
			return $periodo;
		} else {
			return "0000-00-00";
		}
	}

	public function insertar($periodoinicial, $periodofinal, $archivo)
	{
		$datos = array('periodoinicial'=>$periodoinicial,'periodofinal'=>$periodofinal, 'archivo'=> $archivo);
		$this->db->insert('sincronizaciones', $datos);
	}
}
?>