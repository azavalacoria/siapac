<?php
/**
* 
*/
class Rol extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function listar_roles()
	{
		$this->db->select('idrol, nombre');
		$query = $this->db->get('roles');
		$roles = array(''=>'');

		foreach ($query->result() as $r) {
			$roles[$r->idrol] = $r->nombre;
		}
		return $roles;
	}
}
?>