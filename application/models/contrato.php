
<?php
/**
* 
*/
class Contrato extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function buscar_por_numero($numero)
	{
		$this->db->select('contratos.numero, apellidopaterno, apellidomaterno, nombres, nombrecolonia, 
			contratos.calle, numeroexterior, numerointerior , ultimopago');
		$this->db->from("contratos");
		$this->db->join('contribuyentes','contribuyente=idcontribuyente');
		$this->db->join('colonias','contratos.colonia=idcolonia');
		$this->db->where('contratos.numero',$numero);
		$this->db->where('contratos.activo',true);
		$query = $this->db->get();
		return $query;
	}

	public function buscar_por_nombre($apellidopaterno, $apellidomaterno, $nombres, $inicio, $limite)
	{
		$this->db->select('contratos.numero, apellidopaterno, apellidomaterno, nombres, 
			nombrecolonia, contratos.calle, numeroexterior, numerointerior, ultimopago');
		$this->db->from("contratos");
		$this->db->join('contribuyentes','contribuyente=idcontribuyente');
		$this->db->join('colonias','contratos.colonia=idcolonia');
		$this->db->like('apellidopaterno', $apellidopaterno, 'after');
		if ($apellidomaterno != '') {
			$this->db->like('apellidomaterno', $apellidomaterno, 'after');
		}
		if ($nombres != '') {
			$this->db->like('nombres', $nombres, 'after');
		}
		$this->db->where('contratos.activo',true);
		$this->db->limit($limite, $inicio);
		$query = $this->db->get();
		return $query;
	}

	public function obtener_contrato($numero)
	{
		$this->db->select('idcontrato, contratos.numero, apellidopaterno, apellidomaterno, nombres, nombrecolonia, 
			contratos.calle, numeroexterior, numerointerior, ultimopago, contratos.zona, contratos.tipodeservicio');
		$this->db->from("contratos");
		$this->db->join('contribuyentes','contribuyente=idcontribuyente');
		$this->db->join('colonias','contratos.colonia=idcolonia');
		$this->db->where('contratos.numero',$numero);
		$query = $this->db->get();
		return $query->result();
	}

	public function obtener_contrato_edicion($numero)
	{
		$this->db->select("idcontrato, 
			contratos.numero, contratos.medidor, contratos.tipodeservicio,
			contribuyentes.apellidopaterno, contribuyentes.apellidomaterno, contribuyentes.nombres, 
			contratos.calle, numeroexterior, numerointerior, ultimopago, contratos.loc, codigopostal,
			contratos.colonia, contratos.referencias, contratos.observaciones, contratos.zona");
		$this->db->from("contratos");
		$this->db->join('contribuyentes','contribuyente=idcontribuyente');
		$this->db->where('contratos.numero',$numero);
		$query = $this->db->get();
		return $query->result();
	}

	public function obtener_datos_contrato($numero)
	{
		$this->db->select('numero, medidor, nombrecolonia, calle, numeroexterior, numerointerior, 
			referencias, contratos.codigopostal, ultimopago, nombrezona, nombreservicio, observaciones');
		$this->db->from('contratos');
		$this->db->join('colonias','colonia=idcolonia');
		$this->db->join('zonas', 'contratos.zona=idzona');
		$this->db->join('servicios', 'tipodeservicio=idservicio');
		$this->db->where('contratos.numero',$numero);
		$query = $this->db->get();
		return $query->result();
	}

	public function obtener_meses_a_cobrar($numero)
	{
		$this->db->select('ultimopago, precio, nombreservicio');
		$this->db->from('contratos');
		$this->db->join('tarifas','tarifa=idtarifa');
		$this->db->join('tiposservicio','idtiposervicio=tiposervicio');
		$this->db->where('numero',$numero);
		$query = $this->db->get();
		$ultimopago = 0;
		$precio = 0.0;
		$nombreservicio = "";
		foreach ($query->result() as $d) {
			$ultimopago = $d->ultimopago;
			$precio = $d->precio;
			$nombreservicio = $d->nombreservicio;
		}
		$fechainicial = DateTime();
		$fechafinal = new DateTime($ultimopago);
		$diferencia = $fechafinal->diff($fechafinal);
		$meses = ($diferencia->y * 12) + $diferencia->m;
		$importe = $meses * $precio;
		return array('cantidad'=>$meses, 'periodo'=>$periodo,
			'concepto'=>$nombreservicio,'preciounitario'=>$precio,'importe'=>$importe);
	}

	public function obtener_adeudos($numero)
	{
		$this->db->select('adeudos, ultimopago');
		$this->db->where('numero',$numero);
		$query = $this->db->get('contratos');
		foreach ($query->result() as $c) {
			$adeudos = $c->adeudos;
			$ultimopago = $c->ultimopago;
		}
		if ($adeudos > 0) {
			$periodo = "".date_format(new DateTime($ultimopago),"Ym")."-".date('Ym');
			return array('cantidad'=>1,'periodo'=>$periodo,'concepto'=>'Adeudos',
				'preciounitario'=>$adeudos,'importe'=>$adeudos);
		} else {
			return array();
		}
	}

	public function actualizar_ultimo_pago($ultimopago, $idcontrato)
	{
		$this->db->set('ultimopago', $ultimopago);
		$this->db->where('idcontrato',$idcontrato);
		$this->db->update('contratos');
	}

	public function modificar_ultimo_pago($ultimopago, $numero)
	{
		$this->db->set('ultimopago', $ultimopago);
		$this->db->where('numero',$numero);
		$this->db->update('contratos');
	}

	public function obtener_ultimo_pago($idcontrato)
	{
		$this->db->select('ultimopago');
		$this->db->where('idcontrato',$idcontrato);
		$query = $this->db->get('contratos');
		$ultimopago = "";
		foreach ($query->result() as $pago) {
			$ultimopago = $pago->ultimopago;
		}
		return $ultimopago;
	}

	public function obtener_fecha_pago($numero)
	{
		$this->db->select('ultimopago');
		$this->db->where('numero',$numero);
		$query = $this->db->get('contratos');
		$ultimopago = "";
		foreach ($query->result() as $pago) {
			$ultimopago = $pago->ultimopago;
		}
		return $ultimopago;
	}

	public function obtener_tarifa($numero)
	{
		$this->db->select('precio');
		$this->db->from('contratos');
		$this->db->join('tarifas','tarifa=idtarifa');
		$this->db->where('numero', $numero);
		$query = $this->db->get();
		$precio = 0.0;
		foreach ($query->result() as $v) {
			$precio = $v->precio;
		}
		return $precio;
	}

	public function contrato_existe($numero)
	{
		$this->db->like('numero', $numero);
		$this->db->from('contratos');
		return $this->db->count_all_results();
	}

	public function agregar_nuevo($datos)
	{
		$this->db->insert('contratos', $datos);
	}

	public function actualizar_contrato($datos , $idcontrato)
	{
		$this->db->where('idcontrato',$idcontrato);
		$this->db->update('contratos', $datos);
	}

	public function generar_numero_contrato($prefijo)
	{
		$query = $this->db->query("
			SELECT convert( 
				substring( numero, 6, length( numero ) ) , signed integer ) AS valornumerico, numero  
				FROM contratos 
				where left(numero, 5) = '".$prefijo."' 
				ORDER BY valornumerico DESC LIMIT 0 , 1 ");
		$numero = 0;
		foreach ($query->result() as $num) {
			$numero = $num->valornumerico + 1;
		}
		return $numero;

	}

	public function obtener_datos_impresion($numero)
	{
		$this->db->select("contratos.numero, contratos.calle, contratos.numeroexterior, contratos.numerointerior, 
			nombrecolonia, nombreservicio, nombres, apellidopaterno, apellidomaterno");
		$this->db->from('contratos');
		$this->db->join('colonias','colonia=idcolonia');
		$this->db->join('contribuyentes', 'contribuyente=idcontribuyente');
		$this->db->join('servicios', 'tipodeservicio=idservicio');
		$this->db->where('contratos.numero',$numero);
		$query = $this->db->get();
		return $query->result();
	}

	public function obtener_datos_impresion_baja($numero)
	{
		$this->db->select("contratos.numero, contratos.calle, contratos.numeroexterior, contratos.numerointerior, 
			nombrecolonia, nombreservicio, nombres, apellidopaterno, apellidomaterno, fechabaja, motivobaja");
		$this->db->from('contratos');
		$this->db->join('colonias','colonia=idcolonia');
		$this->db->join('contribuyentes', 'contribuyente=idcontribuyente');
		$this->db->join('servicios', 'tipodeservicio=idservicio');
		$this->db->where('contratos.numero',$numero);
		$this->db->where('contratos.activo',false);
		$query = $this->db->get();
		return $query->result();
	}

	public function obtener_id_contrato($numero)
	{
		$this->db->select('idcontrato');
		$this->db->where('numero',$numero);
		$query = $this->db->get('contratos');
		$id = 0;
		foreach ($query->result() as $contrato) {
			$id = $contrato->idcontrato;
		}
		return $id;
	}

	public function obtener_zona($numero)
	{
		$this->db->select('zona');
		$this->db->where('numero', $numero);
		$query = $this->db->get('contratos');
		$zona = 0;
		foreach ($query->result() as $z) {
			$zona = $z->zona;
		}
		return $zona;
	}

	public function obtener_tipo_servicio($numero)
	{
		$this->db->select('tipodeservicio');
		$this->db->where('numero', $numero);
		$query = $this->db->get('contratos');
		$tipodeservicio = 0;
		foreach ($query->result() as $z) {
			$tipodeservicio= $z->tipodeservicio;
		}
		return $tipodeservicio;
	}

	public function obtener_domicilio_contrato($numero)
	{
		$this->db->select('calle, numeroexterior, numerointerior, nombrecolonia');
		$this->db->from('contratos');
		$this->db->join('colonias','idcolonia=colonia');
		$this->db->where('numero', $numero);
		$query = $this->db->get();
		$domicilio = "";
		foreach ($query->result() as $datos) {
			if ($datos->numerointerior == '0') {
				$domicilio = $datos->calle." Num ".$datos->numeroexterior." ".$datos->nombrecolonia;

			} else {
				$domicilio = $datos->calle." Num ".$datos->numeroexterior." ".$datos->numerointerior." ".$datos->nombrecolonia;
			}
		}
		return $domicilio;
	}

	public function obtener_nombre_contribuyente($numero)
	{
		$this->db->select('nombres, apellidopaterno, apellidomaterno');
		$this->db->from('contratos');
		$this->db->join('contribuyentes','contribuyente=idcontribuyente');
		$this->db->where('contratos.numero',$numero);
		$query = $this->db->get();
		$nombre = "";
		foreach ($query->result() as $datos) {
			$nombre = $datos->nombres." ".$datos->apellidopaterno." ".$datos->apellidomaterno;
		}
		return $nombre;
	}

	public function listar_contratos_colonia($calle, $limite)
	{
		$this->db->select('contratos.numero, nombres, apellidopaterno, apellidomaterno, contratos.calle, 
			contratos.numeroexterior,contratos.numerointerior, nombrecolonia, ultimopago');
		$this->db->from('contratos');
		$this->db->join('contribuyentes','idcontribuyente=contribuyente');
		$this->db->join('colonias','idcolonia=contratos.colonia');
		$this->db->where('contratos.calle', $calle);
		$this->db->order_by('nombres','asc');
		$this->db->order_by('apellidopaterno','asc');
		$this->db->order_by('apellidomaterno','asc');
		$this->db->order_by('nombrecolonia','asc');

		$this->db->limit(30, $limite);
		$query = $this->db->get();
		$datos = array();
		foreach ($query->result() as $v) {
			if ($v->numerointerior == 0) {
				$direccion = $v->calle.' '.$v->numeroexterior.'. COL. '.$v->nombrecolonia;
			} else {
				$direccion = $v->calle.' '.$v->numeroexterior.' - '.$v->numerointerior.'. COL. '.$v->nombrecolonia;
			}
			$contrato = array(
					'numero' => $v->numero,
					'nombres' => $v->nombres,
					'apellidopaterno' => $v->apellidopaterno,
					'apellidomaterno' => $v->apellidomaterno,
					'direccion' => strtoupper($direccion),
					'ultimopago' => $v->ultimopago
				);
			array_push($datos, $contrato);
		}
		return $datos;
		//return $this->db->count_all_results();
	}

	public function obtener_ultimos_contratos($periodoinicial, $periodofinal, $zona)
	{
		$this->db->select('contratos.numero, apellidopaterno, apellidomaterno, nombres, 
			nombrecolonia, contratos.calle, numeroexterior, ultimopago, contratos.zona, nombreservicio');
		//$this->db->select('numero,contribuyente');
		$this->db->from("contratos");
		$this->db->join('contribuyentes','contribuyente=idcontribuyente');
		$this->db->join('colonias','contratos.colonia=idcolonia');
		$this->db->join('zonas','contratos.zona=idzona');
		$this->db->join('servicios','idservicio=contratos.tipodeservicio');
		$this->db->where('contratos.fecha >=',$periodoinicial);
		$this->db->where('contratos.fecha <=',$periodofinal);
		$this->db->where('contratos.zona',$zona);
		$query = $this->db->get();
		//return $query->result();
		$contratos = array();
		foreach ($query->result() as $c) {
			$contrato = array(
					'numerocontrato' => $c->numero,
					'apellidopaterno' => $c->apellidopaterno,
					'apellidomaterno' => $c->apellidomaterno,
					'nombres' => $c->nombres,
					'colonia' => $c->nombrecolonia,
					'calle' => $c->calle,
					'numero' => $c->numeroexterior,
					'ultimopago' => $c->ultimopago,
					'servicio' => $c->nombreservicio
				);
			array_push($contratos, $contrato);
		}
		return $contratos;
	}

	public function baja_contrato($datos, $contrato)
	{
		$this->db->where('numero',$contrato);
		$this->db->update('contratos', $datos);
		return $contrato;
	}

	public function obtener_contratos_bajas($inicio, $limite)
	{
		$this->db->select('contratos.numero, apellidopaterno, apellidomaterno, nombres, 
			nombrecolonia, contratos.calle, numeroexterior, numerointerior, fechabaja, motivobaja');
		$this->db->from("contratos");
		$this->db->join('contribuyentes','contribuyente=idcontribuyente');
		$this->db->join('colonias','contratos.colonia=idcolonia');
		$this->db->where('contratos.activo',false);
		$this->db->order_by("fechabaja", "desc"); 
		$this->db->limit($limite, $inicio);
		$query = $this->db->get();
		return $query;
	}

}

/*
concat('Calle ',contratos.calle,' ', contratos.numeroexterior, ' ', contratos.numerointerior, ', ', nombrecolonia) as direccion, 
nombreservicio,
concat(nombres,' ', apellidopaterno, ' ', apellidomaterno) as nombrecompleto 
*/
?>