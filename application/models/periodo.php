<?php
/**
* 
*/
class Periodo extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		
	}

	public function obtener_periodos($fechainicial, $fechafinal)
	{
		$anioinicial = date("Y", strtotime($fechainicial));
		$aniofinal = date("Y", strtotime($fechafinal));
		$anio = $anioinicial;
		$items = array();
		while ($anio <= $aniofinal) {
			if ($anio == $anioinicial && $anio == $aniofinal) {
				$p = $this->_establecer_periodo($fechainicial, $fechafinal, $fechainicial, $fechafinal);
				array_push($items, $p);
			} elseif ($anio == $anioinicial) {
				$p = $this->_establecer_periodo($fechainicial, "$anioinicial-12-31", $fechainicial, "$anioinicial-12-31");
				array_push($items, $p);
			} elseif ($anio == $aniofinal) {
				$p = $this->_establecer_periodo("$aniofinal-01-01",$fechafinal , "$aniofinal-01-01", $fechafinal);
				array_push($items, $p);
			}  else {
				$p = $this->_establecer_periodo("$anio-01-01", "$anio-12-31", "$anio-01-01", "$anio-12-31");
				array_push($items, $p);
			}
			$anio++;
		}
		return $items;
	}

	private function _establecer_periodo($fechainicial, $fechafinal, $inicio, $fin)
	{
		$f1 = new DateTime($fechainicial);
		$f2 = new DateTime($fechafinal);
		$diferencia = $f1->diff($f2);
		$meses = $diferencia->m +1;
		setlocale(LC_ALL, '');
		$periodo = date("Ym", strtotime($inicio))."-".date("Ym", strtotime($fin));
		$periodousuario = strtoupper(strftime("%Y%b", strtotime($inicio))."-".strftime("%Y%b", strtotime($fin)));
		return array('cantidad'=>$meses, 'periodo'=>$periodo,'periodousuario'=>$periodousuario, 'termino'=>$fin);
	}

	//Deprecated

	public function obtener_periodo ($f1, $f2)
	{
		$anioinicial = date_format(new DateTime($f1),'Y');
		$aniofinal = date_format(new DateTime($f2),'Y');
		$items = array();
		if ($anioinicial == $aniofinal) {
		        $periodoinicial  = date_format(new DateTime($f1),"Ym");
		        $periodofinal = date_format(new DateTime($f2),"Ym");
		        $mesinicial = date_format(new DateTime($f1),"m");
		        $mesfinal = date_format(new DateTime($f2),"m");
		        $meses = (($mesfinal - $mesinicial) + 1);
		        array_push($items, array('cantidad'=>$meses, 'periodo'=>"$periodoinicial-$periodofinal"));
		} else {
			$anio = $anioinicial;
			while($anio <= $aniofinal) {
				// Aqui inicia el recorrido de los años
				if ($anio == $anioinicial) {
				    $periodoinicial = date_format(new DateTime($f1),'Ym');
				    $periodofinal = "".$anio."12";
				    $mesinicial = date_format(new DateTime($f1),'m');
				    $mesfinal = 12;
				    $meses = (($mesfinal - $mesinicial)+1);
				    array_push($items, array('cantidad'=>$meses, 'periodo'=>"$periodoinicial-$periodofinal"));
				} elseif ($anio == $aniofinal) {
				    $periodoinicial = "".$anio."01";
				    $periodofinal = date_format(new DateTime($f2),'Ym');;
				    $mesinicial = 1;
				    $mesfinal = date_format(new DateTime($f2),'m');
				    $meses = (($mesfinal - $mesinicial)+1);
				    array_push($items, array('cantidad'=> $meses, 'periodo'=>"$periodoinicial-$periodofinal"));
				} else {
				    $periodoinicial = "$anio"."01";
				    $periodofinal = "$anio"."12";
				    $mesinicial = 1;
				    $mesfinal = 12;
				    $meses = (($mesfinal - $mesinicial)+1);
				    array_push($items, array('cantidad'=>$meses, 'periodo'=>"$periodoinicial-$periodofinal"));
				}
			    $anio++;
			}
		}
		return $items;
	}
}
?>