<?php
/**
* 
*/
class Bonificacion extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_bonificacion($datos)
	{
		$this->db->insert('bonificaciones', $datos);
	}

	public function obtener_bonificaciones()
	{
		$this->db->select('iddeduccion, descripcion, porcentaje, evidencia');
		$this->db->where('activo', 1);
		$query = $this->db->get('bonificaciones');
		return $query->result();
	}

	public function obtener_tipo_bonificacion($iddeduccion)
	{
		$this->db->select('aplicaratotal');
		$this->db->where('iddeduccion', $iddeduccion);
		$query = $this->db->get('bonificaciones');
		$total = 0;
		foreach ($query->result() as $d) {
			$total = $d->aplicaratotal;
		}
		return $total;
	}

	public function obtener_porcentaje($iddeduccion)
	{
		$this->db->select('porcentaje');
		$this->db->where('iddeduccion', $iddeduccion);
		$query = $this->db->get('bonificaciones');
		$total = 0;
		foreach ($query->result() as $d) {
			$total = $d->porcentaje;
		}
		return $total;
	}
}
?>