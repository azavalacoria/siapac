<?php
/**
* 
*/
class Servicio extends CI_Model
{
	var $tabla = "servicios";
	
	function __construct()
	{
		parent::__construct();
	}

	public function listar_servicios()
	{
		$this->db->select('idservicio, nombreservicio');
		$this->db->where('cobromensual = 1');
		$query = $this->db->get($this->tabla);
		$datos['0'] = 'Seleccione';
		foreach ($query->result() as $s) {
			$datos[$s->idservicio] = $s->nombreservicio;
		}
		return $datos;
	}

	public function listar_servicios_anuales()
	{
		$this->db->select('idservicio, nombreservicio');
		$this->db->where('cobromensual = 0');
		$query = $this->db->get($this->tabla);
		$datos['0'] = 'Seleccione';
		foreach ($query->result() as $s) {
			$datos[$s->idservicio] = $s->nombreservicio;
		}
		return $datos;
	}

	public function obtener_servicios()
	{
		$this->db->select('idservicio, nombreservicio, descripcion');
		$query = $this->db->get($this->tabla);
		return $query->result();
	}

	public function existe_servicio($nombreservicio)
	{
		$this->db->like('nombreservicio',$nombreservicio);
		$this->db->from($this->tabla);
		return $this->db->count_all_results();
	}

	public function agregar_servicio($datos)
	{
		$this->db->insert($this->tabla,$datos);
	}

	public function obtener_servicio($idservicio)
	{
		$this->db->select('nombreservicio, descripcion, cobromensual');
		$this->db->where('idservicio', $idservicio);
		$query = $this->db->get($this->tabla);
		return $query->result();
	}

	public function actualizar_servicio($datos, $idservicio)
	{
		$this->db->where('idservicio', $idservicio);
		$this->db->update($this->tabla, $datos);
	}

	public function obtener_nombre_servicio($idservicio)
	{
		$this->db->select('nombreservicio');
		$this->db->where('idservicio',$idservicio);
		$query = $this->db->get($this->tabla);
		$nombre = "";
		foreach ($query->result() as $s) {
			$nombre = $s->nombreservicio;
		}
		return $nombre;
	}
}
?>