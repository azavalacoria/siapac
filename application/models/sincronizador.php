<?php
/**
* 
*/
class Sincronizador extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_ultimos_contribuyentes($periodoinicial, $periodofinal)
	{
		$this->db->distinct();
		$this->db->select('contribuyente');
		$this->db->from('recibos');
		$this->db->join('contratos','idcontrato=contrato');
		$this->db->where('date(fechacobro) >=', $periodoinicial);
		$this->db->where('date(fechacobro) <=',$periodofinal);
		//$this->db->where('exportado','0');
		//$this->db->where('total >','0');
		$queryone = $this->db->get();
		$contribuyentes = array();
		foreach ($queryone->result() as $c) {
			array_push($contribuyentes, $c->contribuyente);
		}
		$this->db->select('idcontribuyente');
		$this->db->where('date(agregado) >=', $periodoinicial);
		$this->db->where('date(agregado) <=',$periodofinal);
		$querydos = $this->db->get('contribuyentes');
		foreach ($querydos->result() as $c) {
			array_push($contribuyentes, $c->idcontribuyente);
		}
		$conts = array_unique($contribuyentes);
		$ok = sort($conts);
		if ($ok) {
			return $conts;
		} else {
			return $ok;
		}
	}

	public function listar_ultimos_contribuyentes($ultimaactualizacion, $periodofinal)
	{
		$this->db->select('idcontribuyente, claveelector, nombres, 
			apellidopaterno, apellidomaterno, fechanacimiento, loc, colonia, 
			calle, numero, telefono, celular, email, rfc');
		$this->db->where('date(agregado) >=', $ultimaactualizacion);
		$this->db->where('date(agregado) <=',$periodofinal);
		$query = $this->db->get('contribuyentes');
		return $query->result();

	}

	public function obtener_datos_contribuyente($idcontribuyente)
	{
		$this->db->select('idcontribuyente, claveelector, nombres, 
			apellidopaterno, apellidomaterno, fechanacimiento, contribuyentes.loc as loc, 
			contribuyentes.colonia as colonia, contribuyentes.calle as calle, contribuyentes.numero as numero, 
			telefono, celular, email, rfc');
		$this->db->where('idcontribuyente', $idcontribuyente);
		$query = $this->db->get('contribuyentes');
		return $query->result();
	}

	public function obtener_contribuyente($contrato)
	{
		$this->db->select('idcontribuyente, claveelector, nombres, 
			apellidopaterno, apellidomaterno, fechanacimiento, contribuyentes.loc as loc, 
			contribuyentes.colonia as colonia, contribuyentes.calle as calle, contribuyentes.numero as numero, 
			telefono, celular, email, rfc');
		$this->db->from('contribuyentes');
		$this->db->join('contratos','idcontribuyente=contribuyente');
		$this->db->where('idcontrato', $contrato);
		$query = $this->db->get();
		return $query->result();

	}

	public function listar_contratos_contribuyente($idcontribuyente, $ultimaactualizacion, $periodofinal)
	{
		$this->db->select('idcontrato, numero, medidor, loc, colonia, 
			calle, numeroexterior, numerointerior, referencias, 
			codigopostal, fecha, observaciones, latitud, longitud, 
			ultimopago, zona, tipodeservicio');
		$this->db->where('fecha  >=', $ultimaactualizacion);
		$this->db->where('fecha <=', $periodofinal);
		$this->db->where('contribuyente', $idcontribuyente);
		$query = $this->db->get('contratos');
		return $query->result();
	}

	public function listar_ultimos_contratos_activos($idcontribuyente, $periodoinicial, $periodofinal)
	{
		$this->db->distinct();
		$this->db->select('contrato');
		$this->db->from('recibos');
		$this->db->join('contratos','idcontrato=contrato');
		$this->db->where('date(fechacobro) >=', $periodoinicial);
		$this->db->where('date(fechacobro) <=',$periodofinal);
		$this->db->where('total >','0');
		//$this->db->where('exportado','0');
		$queryone = $this->db->get();
		$contratos = array();
		foreach ($queryone->result() as $c) {
			array_push($contratos, $c->contrato);
		}
		$conts = array_unique($contratos);
		$ok = sort($conts);
		if ($ok) {
			return $conts;
		} else {
			return $ok;
		}
	}

	public function listar_recibos_contrato($idcontrato, $ultimaactualizacion, $periodofinal)
	{
		$this->db->select('folios.folio, recibos.folio as idfolio, fechacobro, 
			periodo, subtotal, descuento, total, observaciones, modulo, usuario');
		$this->db->from('recibos');
		$this->db->join('folios', 'recibos.folio=folios.recibo');
		$this->db->where('date(fechacobro)  >',$ultimaactualizacion);
		$this->db->where('date(fechacobro) <=',$periodofinal);
		$this->db->where('contrato', $idcontrato);
		$this->db->where('total >','0');
		$this->db->where('exportado','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function marcar_recibo($folio)
	{
		$this->db->where('folio',$folio);
		$datos = array('exportado'=>1);
		$this->db->update('recibos',$datos);
	}

	public function listar_items_recibo($recibo)
	{
		$this->db->select('periodo, cantidad, concepto, preciounitario, importe, 
			armonizacionuno, armonizaciondos');
		$this->db->where('recibo', $recibo);
		$query = $this->db->get('items');
		return $query->result();
	}

	public function obtener_ultimos_recibos($fecha)
	{
		$this->db->select('contrato');
		$this->db->where('date(fechacobro)  >',$fecha);
		$this->db->where('date(fechacobro) <=',date('Y-m-d'));
		$this->db->group_by('contrato');
		$query = $this->db->get('recibos');
		return $query->result();
	}

	public function obtener_contrato_contribuyente($idcontrato, $ultimaactualizacion)
	{
		$this->db->select('claveelector, nombres, apellidopaterno, apellidomaterno, fechanacimiento, contribuyentes.loc as 
			localidad ,contribuyentes.colonia as cbcolonia, contribuyentes.calle as cbcalle , 
			contribuyentes.numero as cbnumero, telefono, celular, email, rfc, contratos.numero ctnumero, medidor, 
			contratos.loc as ctloc, contratos.colonia as ctcolonia, contratos.calle as ctcalle, numeroexterior, 
			numerointerior, referencias, codigopostal, fecha, observaciones, latitud, longitud, 
			ultimopago, zona, tipodeservicio');
		$this->db->from("contribuyentes");
		$this->db->join('contratos','contribuyente=idcontribuyente');
		$this->db->where('fecha  >', $ultimaactualizacion);
		$this->db->where('fecha <=', date('Y-m-d'));
		$this->db->where('idcontrato',$idcontrato);
		$query = $this->db->get();
		return $query->result();
	}

	public function obtener_contrato($idcontrato)
	{
		$this->db->select('numero, medidor, loc, colonia, calle, numeroexterior, numerointerior, referencias, 
			codigopostal, fecha, observaciones, latitud, longitud, ultimopago, zona, tipodeservicio');
		$this->db->where('idcontrato', $idcontrato);
		$query = $this->db->get('contratos');
		$contrato = null;
		foreach ($query->result() as $c) {
			$contrato = array('numero'=> $c->numero ,
				'medidor'=> $c->medidor , 
				'loc'=> $c->loc , 'colonia'=> $c->colonia ,'calle'=> $c->calle ,
				'numeroexterior'=> $c->numeroexterior ,'numerointerior'=> $c->numerointerior ,
				'referencias'=> $c->referencias , 'codigopostal'=> $c->codigopostal ,
				'fecha'=> $c->fecha ,'observaciones'=> $c->observaciones ,
				'latitud'=> $c->latitud , 'longitud'=> $c->longitud ,
				'ultimopago'=> $c->ultimopago ,'zona'=> $c->zona ,'tipodeservicio'=> $c->tipodeservicio);
		}
		return $contrato;
	}

	public function existe_contribuyente($claveelector, $nombres, $apellidopaterno, $apellidomaterno)
	{
		$this->db->where('claveelector', $claveelector);
		$this->db->where('nombres',$nombres);
		$this->db->where('apellidopaterno', $apellidopaterno);
		$this->db->where('apellidomaterno', $apellidomaterno);
		$this->db->from('contribuyentes');
		return $this->db->count_all_results();
	}

	public function insertar_contribuyente($datos)
	{
		$this->db->insert('contribuyentes', $datos);
	}

	public function actualizar_contribuyente($datos, $nombres, $apellidopaterno, $apellidomaterno)
	{
		$this->db->where('nombres',$nombres);
		$this->db->where('apellidopaterno', $apellidopaterno);
		$this->db->where('apellidomaterno', $apellidomaterno);
		$this->db->update('contribuyentes', $datos);
	}

	public function existe_contrato($numero)
	{
		$this->db->where('numero', $numero);
		$this->db->from('contratos');
		return $this->db->count_all_results();
	}

	public function obtener_id_contribuyente($claveelector, $nombres, $apellidopaterno, $apellidomaterno)
	{
		$this->db->select('idcontribuyente');
		$this->db->like('claveelector', $claveelector, 'after');
		$this->db->like('nombres', $nombres, 'after');
		$this->db->like('apellidopaterno', $apellidopaterno, 'after');
		$this->db->like('apellidomaterno', $apellidomaterno, 'after');
		$query = $this->db->get('contribuyentes');
		$id = 0;
		foreach ($query->result() as $v) {
			$id = $v->idcontribuyente;
		}
		return $id;
	}

	public function insertar_contrato($datos)
	{
		$this->db->insert('contratos', $datos);
	}

	public function obtener_id_contrato($numero)
	{
		$this->db->select('idcontrato');
		$this->db->where('numero', $numero);
		$query = $this->db->get('contratos');
		$id = 0;
		foreach ($query->result() as $c) {
			$id = $c->idcontrato;
		}
		return $id;
	}

	public function insertar_recibo($datos)
	{
		$this->db->insert('recibos', $datos);
	}

	public function obtener_id_recibo($datos)
	{
		//$fechacobro, $contrato
		$this->db->select('folio');
		$this->db->where($datos);
		$query = $this->db->get('recibos');
		$id = 0;
		foreach ($query->result() as $r) {
			$id = $r->folio;
		}
		return $id;
	}

	/*
	public function existe_recibo($datos)
	{
		$this->db->where($datos);
		$this->db->from('recibos');
		return $this->db->count_all_results();
	}
	*/
	public function existe_item($datos)
	{
		$this->db->where($datos);
		$this->db->from('items');
		return $this->db->count_all_results();
	}

	public function insertar_item($datos)
	{
		$this->db->insert('items', $datos);
	}
}
?>