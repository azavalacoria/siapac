<?php
/**
* 
*/
class Contribuyente extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar($datos)
	{
		$this->db->insert('contribuyentes', $datos);
	}

	public function buscar_contribuyente($apellidopaterno, $apellidomaterno, $nombres)
	{
		$this->db->select('idcontribuyente, nombres, apellidopaterno, apellidomaterno, colonia, fechanacimiento, calle, numero');
		//$datos = array('apellidopaterno'=>$apellidopaterno,'apellidomaterno','nombres'=>$nombres);
		$this->db->like('apellidopaterno', $apellidopaterno, 'after');
		if ($apellidomaterno != '' || $apellidomaterno != ' ' || $apellidomaterno != 0) {
			$this->db->like('apellidomaterno', $apellidomaterno, 'after');
		}
		if ($nombres != '' || $nombres != ' ' || $nombres != 0) {
			$this->db->like('nombres', $nombres, 'after');
		}
		
		$query = $this->db->get('contribuyentes');
		return $query;
	}

	public function obtener_contribuyentes()
	{
		$this->db->select('idcontribuyente, nombres, apellidopaterno, fechanacimiento, apellidomaterno, colonia, calle, numero');
		$this->db->order_by('apellidopaterno','asc');
		$this->db->order_by('apellidomaterno','asc');
		$this->db->order_by('nombres','asc');
		$query = $this->db->get('contribuyentes');
		return $query->result();
	}

	public function obtener_contribuyente($claveelector)
	{
		$this->db->select('claveelector, nombres, apellidopaterno, apellidomaterno, fechanacimiento, loc, colonia, calle, numero');
		$this->db->where('claveelector',$claveelector);
		$query = $this->db->get('contribuyentes');
		return $query->result();
	}

	public function obtener_nombre_contribuyente($idcontribuyente)
	{
		$this->db->select('nombres, apellidopaterno, apellidomaterno');
		$this->db->where('idcontribuyente',$idcontribuyente);
		$query = $this->db->get('contribuyentes');
		return $query->result();
	}

	public function existe_contribuyente($claveelector)
	{
		$this->db->select('claveelector');
		$this->db->from('contribuyentes');
		$this->db->where('claveelector', $claveelector);
		return $this->db->count_all_results();
	}
}
?>