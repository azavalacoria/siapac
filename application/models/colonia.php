<?php
/**
* 
*/
class Colonia extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_colonias($zona)
	{
		$this->db->select('idcolonia, nombrecolonia');
		$this->db->where('zona',$zona);
		$query = $this->db->get('colonias');
		return $query->result();
	}

	public function enlistar_colonias($zona)
	{
		$this->db->select('idcolonia, nombrecolonia');
		$this->db->where('zona',$zona);
		$query = $this->db->get('colonias');
		$colonias[0] = "Seleccione";
		foreach ($query->result() as $colonia) {
			$colonias[$colonia->idcolonia] = $colonia->nombrecolonia;
		}
		return $colonias;
	}

	public function obtener_localidad($colonia)
	{
		$query = 'select right(municipiolocalidad, 4) as localidad,codigopostal from colonias where idcolonia ='.$colonia;
		$query = $this->db->query($query);
		$loc = null;
		foreach ($query->result() as $colonia) {
			$loc = array(
				'localidad' => $colonia->localidad,
				'codigopostal' => $colonia->codigopostal
				);
		}
		return $loc;
	}

	public function existe_colonia($nombrecolonia, $zona)
	{
		$this->db->like('nombrecolonia', $nombrecolonia);
		$this->db->where('zona', $zona);
		$this->db->from('colonias');
		return $this->db->count_all_results();
	}

	public function agregar_colonia($datos)
	{
		$this->db->insert('colonias', $datos);
	}

	public function actualizar_zona($idcolonia, $zona)
	{
		$datos = array('zona' => $zona);
		$this->db->where('idcolonia', $idcolonia);
		$this->db->update('colonias', $datos);
		return $this->db->affected_rows();
	}

	public function obtener_nombre_colonia($idcolonia)
	{
		$this->db->select('nombrecolonia');
		$this->db->where('idcolonia',$idcolonia);
		$query = $this->db->get('colonias');
		$nombre = "";
		foreach ($query->result() as $c) {
			$nombre = $c->nombrecolonia;
		}
		return $nombre;
	}
}
?>