<?php
/**
* 
*/
class Folio extends CI_Model
{
	var $tabla = "folios";
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_folio($recibo)
	{
		$datos = array('recibo'=>$recibo);
		$this->db->insert($this->tabla, $datos);
	}

	public function obtener_folio($recibo)
	{
		$this->db->select('folio');
		$this->db->where('recibo', $recibo);
		$query = $this->db->get($this->tabla);
		$folio = 0;
		foreach ($query->result() as $v) {
			$folio = $v->folio;
		}
		return $folio;
	}
}
?>