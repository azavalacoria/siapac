<?php
/**
* 
*/
class Ciudadano extends CI_Model
{
	var $tabla = 'calkinienses';

	function __construct()
	{
		parent::__construct();
	}

	public function buscar_ciudadano($apellidopaterno, $apellidomaterno, $nombre, $inicio, $limite)
	{
		$this->db->distinct();
		$this->db->select('cve_elect as clave, nombre, apellidopaterno, apellidomaterno, colonia, calle, num_a as numero, edad');
		$this->db->like('apellidopaterno', $apellidopaterno, 'after');
		if ($apellidomaterno != '') {
			$this->db->like('apellidomaterno', $apellidomaterno, 'after');
		}
		if ($nombre != '') {
			$this->db->like('nombre', $nombre, 'after');
		}
		$this->db->order_by('apellidopaterno', 'asc');
		$this->db->order_by('apellidomaterno', 'asc');
		$this->db->order_by('nombre', 'asc');
		$this->db->limit($limite,$inicio);
		$query = $this->db->get( $this->tabla);
		return $query;
	}

	public function obtener_ciudadano($claveelector)
	{
		$this->db->distinct();
		$this->db->select('cve_elect, nombre, apellidopaterno, apellidomaterno, fec_nac, colonia, calle, num_a, local');
		$this->db->where( 'cve_elect',$claveelector);
		$query = $this->db->get($this->tabla);
		return $query->result();
	}
	
}
?>