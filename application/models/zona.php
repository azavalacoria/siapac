<?php
/**
* 
*/
class Zona extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_zonas()
	{
		$this->db->select('idzona, nombrezona');
		$query = $this->db->get('zonas');
		return $query->result();
	}

	public function existe_zona($nombrezona)
	{
		$this->db->like('nombrezona', $nombrezona);
		$this->db->from('zonas');
		return $this->db->count_all_results();
	}

	public function agregar_zona($datos)
	{
		$this->db->insert('zonas', $datos);
	}

	public function obtener_zonas_restantes($zona)
	{
		$this->db->select('idzona, nombrezona');
		$this->db->where('idzona !=', $zona);
		$query = $this->db->get('zonas');
		return $query;
	}

	public function listar_zonas()
	{
		$this->db->select('idzona, nombrezona');
		$query = $this->db->get('zonas');
		$zonas['0'] = 'Seleccionar';
		foreach ($query->result() as $z) {
			$zonas[$z->idzona] = $z->nombrezona;
		}
		return $zonas;
	}

	public function enlistar_prefijos_zonas()
	{
		$query = $this->db->query("select prefijo, nombrezona from zonas where prefijo is not null and prefijo != ''");
		$datos = array('0'=>'');
		foreach ($query->result() as $zona) {
			$prefijo = $zona->prefijo;
			$datos[$prefijo] = $prefijo;
		}
		return $datos;
	}

	public function listar_zonas_con_prefijo()
	{
		$query = $this->db->query("select idzona,nombrezona from zonas where prefijo is not null and prefijo != ''");
		$datos = array(''=>'');
		foreach ($query->result() as $zona) {
			$idzona = $zona->idzona;
			$datos[$idzona] = $zona->nombrezona;
		}
		return $datos;
	}

	public function obtener_calles_zona($zona)
	{
		$query = $this->db->query("SELECT calle FROM contratos 
			WHERE zona = $zona GROUP BY calle HAVING count( calle ) >1");
		$datos = array();
		foreach ($query->result() as $d) {
			array_push($datos, $d->calle);
		}
		return $datos;
	}
}
?>