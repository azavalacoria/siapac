<?php

/**
* 
*/
class Exportacion extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_ids_contribuyentes($periodoinicial, $periodofinal, $zona)
	{
		$this->db->select('contribuyente');
		$this->db->where('fecha >=', $periodoinicial);
		$this->db->where('fecha <=', $periodofinal);
		$this->db->where('zona', $zona);

		$query = $this->db->get('contratos');
		$contribuyentes  = array();
		foreach ($query->result() as $contrato) {
			array_push($contribuyentes, $contrato->contribuyente);
		}
		$conts = array_unique($contribuyentes);
		$ok = sort($conts);
		if ($ok) {
			return $conts;
		} else {
			return $ok;
		}
	}

	public function obtener_contratos($lista, $periodoinicial, $periodofinal, $zona)
	{
		$contribuyentes = array();
		foreach ($lista as $id) {
			$contribuyente = $this->_obtener_contribuyente($id, $periodoinicial, $periodofinal, $zona);
			array_push($contribuyentes, $contribuyente);
		}
		return $contribuyentes;
	}

	private function _obtener_contribuyente($idcontribuyente, $periodoinicial, $periodofinal, $zona)
	{
		$this->db->where('idcontribuyente', $idcontribuyente);
		$query = $this->db->get('contribuyentes');
		$contribuyente = null;
		foreach ($query->result() as $c) {
			$contribuyente = array(
					'claveelector' => $c->claveelector,
					'nombres' => $c->nombres,
					'apellidopaterno' => $c->apellidopaterno,
					'apellidomaterno' => $c->apellidomaterno,
					'fechanacimiento' => $c->fechanacimiento,
					'localidad' => $c->loc,
					'colonia' => $c->colonia,
					'calle' => $c->calle,
					'numero' => $c->numero,
					'telefono' => $c->telefono,
					'celular' => $c->celular,
					'email' => $c->email,
					'rfc' => $c->rfc,
					'agregado' => $c->agregado,
					'contratos' => $this->_obtener_ultimos_contratos($idcontribuyente, $periodoinicial, $periodofinal, $zona)
				);
		}
		return $contribuyente;
	}

	private function _obtener_ultimos_contratos($contribuyente, $periodoinicial, $periodofinal, $zona)
	{
		$this->db->where('fecha >=', $periodoinicial);
		$this->db->where('fecha <=', $periodofinal);
		$this->db->where('zona', $zona);
		$this->db->where('contribuyente', $contribuyente);
		$query = $this->db->get('contratos');
		$contratos = array();
		foreach ($query->result() as $c) {
			$contrato = array(
					'numero' => $c->numero,
					'medidor' => $c->medidor,
					'loc' => $c->loc,
					'colonia' => $this->_obtener_nombre_colonia($c->colonia, $c->zona),
					'calle' => $c->calle,
					'numeroexterior' => $c->numeroexterior,
					'numerointerior' => $c->numerointerior,
					'referencias' => $c->referencias,
					'codigopostal' => $c->codigopostal,
					'fecha' => $c->fecha,
					'observaciones' => $c->observaciones,
					'latitud' => $c->latitud,
					'longitud' => $c->longitud,
					'ultimopago' => $c->ultimopago,
					'zona' => $c->zona,
					'tipodeservicio' => $c->tipodeservicio
				);
			array_push($contratos, $contrato);
		}
		return $contratos;
	}

	private function _obtener_nombre_colonia($idcolonia, $zona)
	{
		$this->db->select('nombrecolonia');
		$this->db->where('idcolonia', $idcolonia);
		$this->db->where('zona', $zona);
		$query = $this->db->get('colonias');
		$nombre = null;
		foreach ($query->result() as $colonia) {
			$nombre = $colonia->nombrecolonia;
		}
		return $nombre;
	}

}
?>