<?php
/**
* 
*/
class Corte extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function obtener_folios_por_dia($fechainicial, $fechafinal)
	{
		$this->db->select('recibos.folio as recibo, folios.folio as numero, descuento, total');
		$this->db->from('recibos');
		$this->db->join('folios', 'recibos.folio=recibo');
		$this->db->where('date(fechacobro) >=', $fechainicial);
		$this->db->where('date(fechacobro) <=', $fechafinal);
		$this->db->where('total >', 0);
		$query = $this->db->get();
		return $query->result();
	}


	public function obtener_tipo_descuento($recibo)
	{
		$this->db->select('aplicaratotal');
		$this->db->from('descuentos');
		$this->db->join('bonificaciones', 'concepto = iddeduccion');
		$this->db->where('recibo',$recibo);
		$query = $this->db->get();

		$aplicaratotal = 0;

		foreach ($query->result() as $descuento) {
			$aplicaratotal = $descuento->aplicaratotal;
		}

		return $aplicaratotal;
	}

	public function obtener_rezago_recibo($recibo)
	{
		$this->db->select('sum(importe) as total');
		$this->db->where('recibo',$recibo);
		$this->db->where('left(periodo, 4) <',date("Y"));
		$this->db->where('length(periodo) >','7');
		$query = $this->db->get('items');
		$importe = "0";
		foreach ($query->result() as $item) {
			$importe = $item->total;;
		}
		return $importe;
	}

	public function obtener_ingreso_actual_recibo($recibo)
	{
		$this->db->select('sum(importe) as total');
		$this->db->where('recibo',$recibo);
		$this->db->where('left(periodo, 4) =',date("Y"));
		$this->db->where('length(periodo) >','7');
		$query = $this->db->get('items');
		$importe = "0";
		foreach ($query->result() as $item) {
			$importe = $item->total;
		}
		return $importe;
	}

	public function obtener_ingreso_contratacion_recibo($recibo)
	{
		$this->db->select('sum(importe) as total');
		$this->db->where('recibo',$recibo);
		$this->db->where('left(periodo, 4) =',date("Y"));
		$this->db->where('length(periodo) <=','7');
		$query = $this->db->get('items');
		$importe = "0";
		foreach ($query->result() as $item) {
			$importe = $item->total;
		}
		return $importe;
	}

}
?>