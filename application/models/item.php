<?php
/**
* 
*/
class Item extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function agregar_nuevo_item($datos)
	{
		$this->db->insert('items', $datos);
	}

	public function obtener_items($recibo)
	{
		$this->db->select('iditem, periodo, cantidad, concepto, preciounitario, importe');
		$this->db->where('recibo',$recibo);
		$query = $this->db->get('items');
		return $query->result();
	}

	public function obtener_subtotal($recibo)
	{
		$this->db->select_sum('importe', 'subtotal');
		$this->db->where('recibo',$recibo);
		$query = $this->db->get('items');
		$subtotal = 0.0;
		foreach ($query->result() as $v) {
			$subtotal = $v->subtotal;
		}
		return $subtotal;
	}

	public function buscar_items_del_periodo($periodo, $recibo, $descuento)
	{
		$this->db->select('iditem, cantidad, preciounitario');
		$this->db->like('periodo',$periodo,'after');
		$this->db->where('recibo',$recibo);
		$query = $this->db->get('items');
		$desc = 0.0;
		foreach ($query->result() as $item) {
			$desc = $item->cantidad * ($item->preciounitario * $descuento);
		}
		return $desc;
	}
}
?>