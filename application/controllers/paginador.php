<?php
/**
* 
*/
class Paginador extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if ($this->session->userdata('sesion') != 1) {
			redirect('login');
		}
		
		echo "cargando primeros resultados desde la tupla 0 ";
		$this->session->set_userdata(array('limite'=>30));
		echo "la proxima vez se cargaran desde la tupla: ";
		echo $this->session->userdata('limite');
		$this->load->view('paginador');
		
	}

	public function paginar()
	{
		$opcion = $this->input->post('accion');
		if ($opcion == 0) {
			$limite = $this->session->userdata('limite');
			echo " se cargaron los resultados desde la tupla $limite <br> Ahora el limite vale: ";
			$this->session->set_userdata(array('limite'=>($limite - 30)));
			echo $this->session->userdata('limite');
		} else {
			$limite = $this->session->userdata('limite');
			echo "mando a llamar a otros desde la tupla $limite";
			$this->session->set_userdata(array('limite'=>( $limite + 30)));
			echo " ya cargaste otra vez y el limite vale: ";

			echo $this->session->userdata('limite');
		}
		$this->load->view('paginador');
	}
}
?>