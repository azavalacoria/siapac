<?php
/**
* 
*/
class Modificar_Ultimo_Pago extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		$this->load->model('Zona','',TRUE);
		$datos['zonas'] = $this->Zona->listar_zonas();
		$this->load->view('contratos/modificar_ultimo_pago', $datos);
	}
}
?>