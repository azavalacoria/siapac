<?php

/**
* 
*/
class Exportar_Contratos extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if ($this->session->userdata('sesion') != 1) {
			redirect('login');
		} else {
			$this->_cargar_vista_seleccion();
		}
	}

	public function obtener()
	{
		$this->form_validation->set_rules('zona','zona','trim|required');
		$this->form_validation->set_rules('periodoinicial','fecha inicial','trim|required');
		$this->form_validation->set_rules('periodofinal','fecha final','trim|required');
		if ($this->form_validation->run()) {;
			$menu = $this->session->userdata('menu');
			$this->load->view($menu);
			$periodoinicial = $this->input->post('periodoinicial');
			$periodofinal = $this->input->post('periodofinal');
			$zona = $this->input->post('zona');
			$this->load->model('Contrato','contrato',TRUE);
			$contratos = $this->contrato->obtener_ultimos_contratos($periodoinicial, $periodofinal, $zona);
			if (sizeof($contratos) == 0) {
				# code...
			} else {
				$datos = array(
						'contratos' => $contratos,
						'periodoinicial' => $periodoinicial,
						'periodofinal' => $periodofinal,
						'zona' => $zona
					);
				$this->load->view('contratos/listar_contratos_exportacion', $datos);
			}
			
		} else {
			$this->index();
		}
		
	}

	public function generar_archivo()
	{
		$periodoinicial = $this->input->post('periodoinicial');
		$periodofinal = $this->input->post('periodofinal');
		$zona = $this->input->post('zona');

		$this->load->model('Exportacion','exportador',TRUE);
		$contribuyentes = $this->exportador->obtener_ids_contribuyentes($periodoinicial, $periodofinal, $zona);

		$contratos = $this->exportador->obtener_contratos($contribuyentes, $periodoinicial, $periodofinal, $zona);
		$datos = array('contribuyentes' => $contratos);
		header('Content-type: application/xml');
		$nombre = "Sincronizacion_$periodoinicial.-.$periodofinal";
		header('Content-Disposition: attachment; filename="'.$nombre.'.xml"');
		$this->load->view('contratos/exportacion_a_juntas_municipales', $datos);
	}

	private function _cargar_vista_seleccion()
	{
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
		$this->load->model('Zona','zona',TRUE);
		$datos['zonas'] = $this->zona->listar_zonas_con_prefijo();
		$this->load->view('contratos/seleccionar_zona', $datos);
	}
}
?>