<?php
/**
* 
*/
class Crear_Presupuesto extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		// $this->form_validation->set_rules('apellidopaterno','apellido paterno','trim|required|min_length[2]');
		// $this->form_validation->set_rules('apellidomaterno','apellido materno','trim');
		// $this->form_validation->set_rules('nombres','nombres','trim');
		// if ($this->form_validation->run()) {
		// 	$this->_busqueda_inicial(
		// 		$this->input->post('apellidopaterno'),
		// 		$this->input->post('apellidomaterno'),
		// 		$this->input->post('nombres')
		// 		);
		// } else {
		// 	$datos['destino'] = 'contratos/crear_presupuesto';
		// 	$this->load->view('catalogos/buscar_contribuyente',$datos);
		// }
		$this->_lanzar_vista();
	}

	public function sig_resultados()
	{
		$opcion = $this->input->post('opcion');
		switch ($opcion) {
			case 'sig':
				$this->_siguientes_resultados();
				break;
			
			default:
				$this->load->view('vacio',array('mensaje'=>'No se hallaron resultados'));
				break;
		}
	}

	public function visualizar()

	{
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
		$this->form_validation->set_rules('periodofinal','periodofinal','required');
		if ($this->form_validation->run()) {
			$periodoinicial = $this->session->userdata('ultimopago');
			$periodoinicial =  strftime("%Y-%m-%d", strtotime('first day of next month', strtotime($periodoinicial)));
			$periodofinal = $this->input->post('periodofinal');
			$zona = $this->session->userdata('zona');
			$servicio = $this->session->userdata('tipodeservicio');
			$datos = $this->_obtener_items($periodoinicial, $periodofinal, $zona, $servicio);
			$datos['numero'] = $this->session->userdata('numero');
			$datos['ultimopago'] = $this->session->userdata('ultimopago');
			$datos['periodofinal'] = $periodofinal;
			$datos['nombrecompleto'] = $this->session->userdata('nombrecompleto');
			$datos['domicilio'] = $this->session->userdata('domicilio');
			$this->load->view('contratos/visualizar_presupuesto', $datos);
		} else {
			//$this->_buscar();
			$this->_lanzar_vista();
		}
	}

	public function imprimir()
	{
		$periodoinicial = $this->session->userdata('ultimopago');
		$periodoinicial =  strftime("%Y-%m-%d", strtotime('first day of next month', strtotime($periodoinicial)));
		$periodofinal = $this->input->post('periodofinal');
		$zona = $this->session->userdata('zona');
		$servicio = $this->session->userdata('tipodeservicio');
		$datos = $this->_obtener_items($periodoinicial, $periodofinal, $zona, $servicio);
		$datos['numero'] = $this->session->userdata('numero');
		$datos['ultimopago'] = $this->session->userdata('ultimopago');
		$datos['nombrecompleto'] = $this->session->userdata('nombrecompleto');
		$datos['domicilio'] = $this->session->userdata('domicilio');


		$html = $this->load->view('plantilla_presupuesto', $datos, TRUE);
		$this->load->helper('dompdf');
		$nombrea = "Presupuesto.para.el.contrato.-.".$datos['numero'];
		pdf_create($html, "$nombrea");
	}

	private function _busqueda_inicial($apellidopaterno, $apellidomaterno, $nombres)
	{
		$this->session->set_userdata( array(
			'apellidopaterno' => $apellidopaterno,
			'apellidomaterno' => $apellidomaterno,
			'nombres' => $nombres,
			'inicio'=>0,
			'limite'=>30)
			);
		$this->_buscar();
	}

	private function _siguientes_resultados()
	{
		$inicio = $this->session->userdata('inicio');
		if ($inicio >= 0) {
			$inicio = $inicio + 30;
			$this->session->set_userdata( array('inicio' => $inicio) );
		}
		$this->_buscar();
	}

	private function _buscar()
	{
		$this->load->model('Contrato','',TRUE);
		$query = $this->Contrato->buscar_por_nombre(
			$this->session->userdata('apellidopaterno'),
			$this->session->userdata('apellidomaterno'),
			$this->session->userdata('nombres'),
			$this->session->userdata('inicio'), 30
			);
		if ($query->num_rows() == 0) {
			$this->load->view('vacio',array('mensaje'=>'No se hallaron resultados'));
		} else{
			$datos['contratos'] = $query->result();
			$datos['destino'] = 'contratos/crear_presupuesto/visualizar';
			$datos['busqueda_nueva'] = 'catalogos/visualizar_contrato/sig_resultados';
			$this->load->view('listar_contratos',$datos);
		}
	}

	private function _lanzar_vista()
	{
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
		$datos['numero'] = $this->session->userdata('numero');
		$datos['ultimopago'] = $this->session->userdata('ultimopago');
		$datos['nombrecompleto'] = $this->session->userdata('nombrecompleto');
		$datos['domicilio'] = $this->session->userdata('domicilio');
		$this->load->view('contratos/crear_presupuesto', $datos);
	}

	private function _obtener_items($periodoinicial, $periodofinal, $zona, $servicio)
	{
		$this->load->model('Periodo','',TRUE);
		$this->load->model('Tarifa','',TRUE);
		$periodos = $this->Periodo->obtener_periodos($periodoinicial, $periodofinal);
		$items = array();
		$errores = '';
		foreach ($periodos as $p) {
			$datos = $this->Tarifa->obtener_precio($p['periodo'], $zona, $servicio);
			if (sizeof($datos)) {
				$item = array( 
					"periodo"=> $p['periodousuario'],
					"cantidad"=> $p['cantidad'],
					'concepto'=>$datos[1],
					"preciounitario"=>$datos[0], 
					"importe" => ($p['cantidad'] * $datos[0]) );
					array_push($items, $item);
			} else {
				$errores = $errores.'<li>'.'No se ha declarado una tarifa para el periodo '.$item['periodousuario'].'</li>';
			}
		}
		$total = 0.0;
		foreach ($items as $i) {
			$total = $total + $i['importe'];
		}
		$datos = array('items' => $items, 'errores' => $errores, 'total' => $total);
		return $datos;
	}

}
?>