<?php
/**
* 
*/
class Obtener_colonias extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if ($this->session->userdata('sesion') != 1) {
			redirect('login');
		}
	}

	public function enlistar()
	{
		$zona = $this->input->post('zona');
		$this->load->model('Colonia','',TRUE);
		$result = $this->Colonia->obtener_colonias($zona);
		foreach ($result as $colonia) {
			echo '<option value="'.$colonia->idcolonia.'">';
			echo $colonia->nombrecolonia;
			echo "</option>";
		}
	}

	public function obtener_localidad()
	{
		$colonia = $this->input->post('colonia');
		$this->load->model('Colonia','',TRUE);
		$localidad = $this->Colonia->obtener_localidad($colonia);
		echo $localidad;
	}

	public function obtener_loc()
	{
		$colonia = $this->input->post('colonia');
		$this->load->model('Colonia','',TRUE);
		$localidad = $this->Colonia->obtener_localidad($colonia);
		echo json_encode($localidad);
	}

}
?>