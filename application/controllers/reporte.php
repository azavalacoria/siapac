<?php
/**
* 
*/
class Reporte extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function corte_diario()
	{
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
		$this->form_validation->set_rules('periodoinicial','día de corte inicial','required|trim');
		$this->form_validation->set_rules('periodofinal','día de corte final','trim');
		if ($this->form_validation->run()) {
			$periodoinicial = $this->input->post('periodoinicial');
			$periodofinal = $this->input->post('periodofinal');
			if (!isset($periodofinal) || $periodofinal == '') {
				$periodofinal = date("Y-m-d");
			}
			$this->_obtener_corte_diario($periodoinicial, $periodofinal, 0);
			//echo "$periodoinicial - $periodofinal";
			
		} else {
			$this->load->view('reportes/elegir_dia');
		}
	}

	private function _obtener_corte_diario($fechainicial, $fechafinal, $imprimible)
	{
		$this->load->model('Corte','',TRUE);
		$datos = $this->Corte->obtener_folios_por_dia($fechainicial, $fechafinal);
		if (sizeof($datos) == 0) {
			$opciones = array(
				'mensaje' => 'No se realizaron movimientos desde el '.$fechainicial.' hasta '.$fechafinal ,
				'destino' => 'home'
				);
			$this->load->view('vacio', $opciones);
		} else {
			if ($imprimible == 1) {
				$folios = $this->_generar_reporte_diario($datos, $imprimible, $fechainicial, $fechafinal);
				$this->_imprimir($folios, $fechainicial, $fechafinal);
			} else {
				$this->_generar_reporte_diario($datos, $imprimible, $fechainicial, $fechafinal);
			}
		}
	}

	private function _generar_reporte_diario($datos, $imprimible, $fechainicial, $fechafinal)
	{
		$folios = array();
		foreach ($datos as $folio) {
			$rezago = $this->Corte->obtener_rezago_recibo($folio->recibo);
			$actual = $this->Corte->obtener_ingreso_actual_recibo($folio->recibo);
			$contratacion = $this->Corte->obtener_ingreso_contratacion_recibo($folio->recibo);
			$bonificaciontotal = $this->Corte->obtener_tipo_descuento($folio->recibo);
			if ($rezago == 0) {
				$rezago = "0.0";
			}
			if ($actual == 0) {
				$actual = "0.0";
			}
			if ($contratacion == 0) {
				$contratacion = "0.0";
			}
			$folio = array(
				'folio' => $folio->numero,
				'rezago' => $rezago,
				'actual' => $actual,
				'contratacion' => $contratacion,
				'bonificaciontotal' => $bonificaciontotal,
				'bonificacion' => $folio->descuento,
				'total' => $folio->total
				);
			array_push($folios, $folio);
		}

		if ($imprimible == 1) {
			return $folios;
		} else{
			$data = array('folios'=> $folios, 'fechainicial'=>$fechainicial,'fechafinal'=>$fechafinal);
			$this->load->view('reportes/reporte_diario', $data);
		}
	}

	public function imprimir()
	{
		$this->form_validation->set_rules('fechainicial','día inicial de corte','required|trim');
		$this->form_validation->set_rules('fechafinal','día final de corte','required|trim');
		if ($this->form_validation->run()) {
			$fechainicial = $this->input->post('fechainicial');
			$fechafinal = $this->input->post('fechafinal');
			$this->_obtener_corte_diario($fechainicial, $fechafinal, 1);
		} else {
			$this->load->view('reportes/elegir_dia');
		}
	}

	private function _imprimir($folios, $fechainicial, $fechafinal)
	{
		$datos = array('folios'=>$folios);
		$html = $this->load->view('reportes/demo', $datos, TRUE);
		$nombre = 'Corte de Caja del '.$fechainicial.' al '.$fechafinal;
		$this->load->helper('dompdf');
		pdf_create($html, $nombre);
		//echo "$html";
	}
}
?>