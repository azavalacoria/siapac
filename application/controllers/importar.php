<?php
/**
* 
*/
class Importar extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		echo "hola";
	}
	private function _verificar_contribuyente($contribuyente)
	{
		// echo "<br><br>Contribuyente: ";
		// echo $contribuyente['id']."CE -".$contribuyente['clave_elector']."-".$contribuyente['nombres']."-";
		// echo $contribuyente['apellido_paterno']."-".$contribuyente['apellido_materno'];
		$clave_elector = $contribuyente['clave_elector'];
		$existecb = $this->Sincronizador->existe_contribuyente(
			trim($clave_elector),
			trim($contribuyente['nombres']), 
			trim($contribuyente['apellido_paterno']), 
			trim($contribuyente['apellido_materno'])
			);
		if ($existecb == 0) {
			$datos = array(
					'nombres'=> trim($contribuyente['nombres']),
					'claveelector' => trim($contribuyente['clave_elector']),
					'nombres' => trim($contribuyente['nombres']),
					'apellidopaterno' => trim($contribuyente['apellido_paterno']),
					'apellidomaterno' => trim($contribuyente['apellido_materno']),
					'fechanacimiento' => $this->_validar_nulo($contribuyente['fecha_nacimiento']),
					'loc' => $this->_validar_nulo($contribuyente['loc']),
					'colonia' => $this->_validar_nulo($contribuyente['colonia']),
					'calle' => $this->_validar_nulo($contribuyente['calle']),
					'numero' => $this->_validar_nulo($contribuyente['numero']),
					'telefono' => $this->_validar_nulo($contribuyente['telefono']),
					'celular' => $this->_validar_nulo($contribuyente['celular']),
					'email' => $this->_validar_nulo($contribuyente['email']),
					'rfc' => $this->_validar_nulo($contribuyente['rfc'])
				);			
			$this->Sincronizador->insertar_contribuyente($datos);
			echo "|=";
			return $this->_buscar_cb(
					trim($clave_elector), trim($contribuyente['nombres']), 
					trim($contribuyente['apellido_paterno']), trim($contribuyente['apellido_materno']) );
		} else {
			echo " <br>----- contribuyente encontrado -----<br>";
			return $this->_buscar_cb(
					trim($clave_elector), trim($contribuyente['nombres']), 
					trim($contribuyente['apellido_paterno']), trim($contribuyente['apellido_materno']) );
		}
		echo "<br><hr> <br><br>";
	}

	public function _buscar_cb($claveelector, $nombres, $apellidopaterno, $apellidomaterno)
	{
		return $this->Sincronizador->obtener_id_contribuyente($claveelector, $nombres, $apellidopaterno, $apellidomaterno);
	}

	private function _verificar_contrato($contrato, $contribuyente)
	{
		$ct = $contrato['numero'];
		$existect = $this->Sincronizador->existe_contrato(trim($ct));

		if ($existect == 0) {
			//echo " < --- $contribuyente";
			$ctcontribuyente = $contribuyente;
			$ctnumero = trim($contrato['numero']);
			$ctmedidor= trim($contrato['medidor']);
			$ctloc = $this->_validar_nulo($contrato['loc']);
			$ctcolonia = $this->_validar_nulo($contrato['colonia']);
			$ctcalle = $this->_validar_nulo($contrato['calle']);
			$ctnumeroexterior = $this->_validar_nulo($contrato['numero_exterior']);
			$ctnumerointerior = $this->_validar_nulo($contrato['numero_interior']);
			$ctreferencias = $this->_validar_nulo($contrato['referencias']);
			$ctcodigopostal = $this->_validar_nulo($contrato['codigo_postal']);
			$ctfecha = $this->_validar_nulo($contrato['fecha']);
			$ctobservaciones = $this->_validar_nulo($contrato['observaciones']);
			$ctlatitud = $this->_validar_nulo($contrato['latitud']);
			$ctlongitud = $this->_validar_nulo($contrato['longitud']);
			$ctultimopago = $this->_validar_nulo($contrato['ultimo_pago']);
			$ctzona = $this->_validar_nulo($contrato['zona']);
			$ctipodeservicio = $this->_validar_nulo($contrato['tipo_de_servicio']);

			//print_r($contrato['observaciones']);
			$datos = array(
					'numero' => $ctnumero,
					'contribuyente' => $ctcontribuyente,
					'medidor' => $ctmedidor,
					'loc' => $ctloc,
					'colonia' => $ctcolonia,
					'calle' => $ctcalle,
					'numeroexterior' => $ctnumeroexterior,
					'numerointerior' => $ctnumerointerior,
					'referencias' => $ctreferencias,
					'codigopostal' => $ctcodigopostal,
					'fecha' => $ctfecha,
					'observaciones' => null,
					'latitud' => $ctlatitud,
					'longitud' => $ctlongitud,
					'ultimopago' => $ctultimopago,
					'zona' => $ctzona,
					'tipodeservicio' => $ctipodeservicio
				);
			$this->Sincronizador->insertar_contrato($datos);
			echo "=";
			return $ctnumero;
		} else {
			echo "<br> - el contrato $ct no sera agregado $existect <br>";
			return 0;
		}

	}

	private function _verificar_recibo($recibo, $numerocontrato)
	{
		$contrato = $this->Sincronizador->obtener_id_contrato($numerocontrato);
		$datos = array(
			'contrato' => $contrato,
			'fechacobro' => $this->_validar_nulo($recibo['fecha_cobro']),
			'periodo' => $this->_validar_nulo($recibo['periodo']),
			'subtotal' => $this->_validar_nulo($recibo['subtotal']),
			'descuento' => $this->_validar_nulo($recibo['descuento']),
			'total' => $this->_validar_nulo($recibo['total']),
			'observaciones' => $this->_validar_nulo($recibo['observaciones']),
			'modulo' => $this->_validar_nulo($recibo['modulo']),
			'usuario' => $this->_validar_nulo($recibo['usuario'])
			);
		$this->Sincronizador->insertar_recibo($datos);
		echo "=";
		return $this->Sincronizador->obtener_id_recibo($datos);
	}

	public function _verificar_item($item ,$folio)
	{
		$datos = array(
			'periodo' => $this->_validar_nulo($item['periodo']),
			'cantidad' => $this->_validar_nulo($item['cantidad']),
			'concepto' => $this->_validar_nulo($item['concepto']),
			'preciounitario' => $this->_validar_nulo($item['precio_unitario']),
			'importe' => $this->_validar_nulo($item['importe']),
			'armonizacionuno' => $this->_validar_nulo($item['armonizacion_uno']),
			'armonizaciondos' => $this->_validar_nulo($item['armonizacion_dos']),
			);
		$existeit = $this->Sincronizador->existe_item($datos);
		//echo "-$existeit-"; 
		if($existeit == 0){
			$datos['recibo'] = $folio;
			$this->Sincronizador->insertar_item($datos);
			echo "=";
		} else {
			echo "<br>----- no se agrego item -----<br>";
		}
		
	}

	private function _validar_nulo($String)
	{
		try {
		    $String = trim($String);
			if ($String== 'NULL' || $String == ' NULL ') {
				return null;
			} else {
				return $String;
			}
		} catch (Exception $e) {
			echo $e->getMessage();
			echo "TRError: ";
			print_r($valor);
			log_message(print_r($valor));
			//var_dump($valor);
		}
		
	}
/////////////////////
	private function obtenercontratos($contratos)
	{
		foreach ($contratos as $recibo) {
			echo sizeof($recibo->recibo)." ";
			print_r($recibo->recibo);
			echo "<br>";
			$this->obteneritem($recibo->items);
		}
	}

	private function obteneritem($items)
	{
		foreach ($items as $item) {
			echo sizeof($item->item)."<--- Item <br>";
		}
	}

	public function contar_resultados()
	{
		$this->load->model('Matcher','',TRUE);
		echo strftime("%R");
		$datos = $this->Matcher->obtener();
		foreach ($datos as $v) {
			$agregado = $v->fecha." 10:00:00";
			$this->Matcher->asignar($v->idcontribuyente, $agregado);
		}

		echo "<br>";
		echo strftime("%R");
	}

	public function mostrar()
	{
		// $this->output->enable_profiler(TRUE);
		$path = "sync/exportar2.xml";
		
		$array = json_decode(json_encode((array)simplexml_load_file($path)),1);
		print_r($array);
	}

	public function simplexml()
	{
		$path = "sync/Sincronizacion_0000-00-00.-.2013-09-21.xml";
		if (file_exists($path)) {
			$dom = new DOMDocument();
			$dom->load($path);
			$dom->preserveWhiteSpace = FALSE;
			$this->load->model('Sincronizador','',TRUE);
		}
		
		if ($dom) {
			$ct = simplexml_import_dom($dom);
			$c = 0;
			$idcontribuyente = 1;
			echo strftime("Importacion iniciada a las %r <br>");
			while (isset($ct->contribuyente[$c])) {
				$idcontribuyente = $this->_verificar_contribuyente(
						array(
						'id' => trim($ct->contribuyente[$c]->id),
						'clave_elector' => trim($ct->contribuyente[$c]->clave_elector),
						'nombres' => trim($ct->contribuyente[$c]->nombres),
						'apellido_paterno' => trim($ct->contribuyente[$c]->apellido_paterno),
						'apellido_materno' => trim($ct->contribuyente[$c]->apellido_materno),
						'fecha_nacimiento' => trim($ct->contribuyente[$c]->fecha_nacimiento),
						'colonia' => trim($ct->contribuyente[$c]->colonia),
						'calle' => trim($ct->contribuyente[$c]->calle),
						'numero' => trim($ct->contribuyente[$c]->numero),
						'loc' => trim($ct->contribuyente[$c]->loc),
						'telefono' => trim($ct->contribuyente[$c]->telefono),
						'celular' => trim($ct->contribuyente[$c]->celular),
						'email' => trim($ct->contribuyente[$c]->email),
						'rfc' => trim($ct->contribuyente[$c]->rfc)
						)
					);
				echo " $c ";
				//*/
				if (isset($ct->contribuyente[$c]->contratos) && $idcontribuyente != 0) {
					foreach ($ct->contribuyente[$c]->contratos as $contratos) {
						foreach ($contratos as $key => $contrato) {
							$acontrato = array(
								'numero' => trim($contrato->numero),
								'medidor' => trim($contrato->medidor),
								'loc' => trim($contrato->loc),
								'colonia' => trim($contrato->colonia),
								'calle' => trim($contrato->calle),
								'numero_exterior' => trim($contrato->numero_exterior),
								'numero_interior' => trim($contrato->numero_interior),
								'referencias' => trim($contrato->referencias),
								'fecha' => trim($contrato->fecha),
								'codigo_postal' => trim($contrato->codigo_postal),
								'observaciones' => trim($contrato->observaciones),
								'ultimo_pago' => trim($contrato->ultimo_pago),
								'latitud' => trim($contrato->latitud),
								'longitud' => trim($contrato->longitud),
								'zona' => trim($contrato->zona),
								'tipo_de_servicio' => trim($contrato->tipo_de_servicio),
								);
							$idcontrato = $this->_verificar_contrato($acontrato, $idcontribuyente);
							if (isset($contrato->recibos)) {
								foreach ($contrato->recibos as $recibos) {
									foreach ($recibos as $recibo) {
										$arecibo = array(
											'fecha_cobro' => trim($recibo->fecha_cobro),
											'periodo' => trim($recibo->periodo),
											'subtotal' => trim($recibo->subtotal),
											'descuento' => trim($recibo->descuento),
											'total' => trim($recibo->total),
											'observaciones' => trim($recibo->observaciones),
											'modulo' => trim($recibo->modulo),
											'usuario' => trim($recibo->usuario)
											);
										$idrecibo = $this->_verificar_recibo($arecibo,$idcontrato);
										if (isset($recibo->items)) {
											foreach ($recibo->items as $items) {
												foreach ($items as $item) {
													$aitem = array(
														'periodo' => trim($item->periodo),
														'cantidad' => trim($item->cantidad),
														'concepto' => trim($item->concepto),
														'precio_unitario' => trim($item->precio_unitario),
														'importe' => trim($item->importe),
														'armonizacion_uno' => trim($item->armonizacion_uno),
														'armonizacion_dos' => trim($item->armonizacion_dos)
														);
													$this->_verificar_item($aitem, $idrecibo);
												}
											}
										}
									}
								}
								
							}
						}
					}
					

				}
				echo "> <br>";
				$idcontribuyente = 0;
				$c++;
			}
			echo "se recorrieron $c registros";
			echo strftime("<br><br>Importacion finalizada a las %r <br>");
		}

	}
}
?>