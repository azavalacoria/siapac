
<?php
/**
* 
*/
class Peticiones_Ajax extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function contrato_existe()
	{
		$numero = $this->input->post('numero');
		$this->load->model('Contrato','',TRUE);
		echo $this->Contrato->contrato_existe($numero);
	}

	public function obtener_colonias()
	{
		$zona = $this->input->post('zona');
		if ($zona == 0) {
			echo json_encode(array('encontradas'=>0));
		} else {
			$this->load->model('Colonia','',TRUE);
			$result = $this->Colonia->obtener_colonias($zona);
			$datos = array();
			foreach ($result as $colonia) {
				array_push($datos, array("id"=> $colonia->idcolonia, 'nombre'=> $colonia->nombrecolonia));
			}
			echo json_encode($datos);
		}
	}

	public function obtener_calles()
	{
		$zona = $this->input->post('zona');
		if ($zona == 0) {
			echo json_encode(array('encontradas'=>0));
		} else {
			$this->load->model('Zona','',TRUE);
			$result = $this->Zona->obtener_calles_zona($zona);
			$datos = array();
			foreach ($result as $colonia) {
			 	array_push($datos, array("id"=> $colonia, 'nombre'=> $colonia));
			}
			echo json_encode($datos);
		}
	}

	public function actualizar_zona_colonia()
	{
		$idcolonia = $this->input->post('idcolonia');
		$zona = $this->input->post('zona');
		if (!isset($idcolonia) && !isset($zona)) {
			echo json_encode(array('afectadas' => 0));
		} else {
			$this->load->model('Colonia','',TRUE);
			$afectadas = $this->Colonia->actualizar_zona($idcolonia, $zona);
			echo json_encode(array('afectadas' => $afectadas));
		}
	}

	public function obtener_zonas_restantes()
	{
		$zona = $this->input->post('zona');
		if ($zona == 0) {
			echo json_encode(array('encontrados'=>0));
		} else {
			$this->load->model('Zona','',TRUE);
			$data = $this->Zona->obtener_zonas_restantes($zona);
			$encontrados = $data->num_rows();
			$datos['encontados'] = $encontrados;
			if ($encontrados > 0) {
				$zonas = array();
				foreach ($data->result() as $zona) {
					array_push($zonas, array('id'=>$zona->idzona, 'nombre'=>$zona->nombrezona));
				}
				$datos['zona'] = $zonas;
			}
			echo json_encode($datos);
		}
	}

	public function obtener_localidad()
	{
		$colonia = $this->input->post('colonia');
		if ($localidad != '' && strlen($colonia) > 3) {
			# code...
		}
	}

	public function obtener_contrato()
	{
		$contrato = $this->input->post('contrato');
		$this->load->model('Contrato','',TRUE);
		$existe = $this->Contrato->contrato_existe($contrato);
		if ($existe == 0) {
			$datos = array(
				'mensaje'=>'No se ha encontrado el número de contrato. ',
				'etiqueta' => '¿Desea buscar otra vez?',
				'destino' => site_url('catalogos/visualizar_contrato/buscar')
				);
			$this->_datos_no_encontrados($datos);
		} else {
			echo $this->_cargar_vista_edicion_contrato($contrato);
		}
	}

	public function listar_contratos_colonias_up()
	{
		$colonia = $this->input->post('colonia');
		$limite = $this->input->post('limite');
		if ($limite < 2) {
			$limite = 0;
			$this->session->set_userdata(array('cont_limite'=>30));
		} else {
			$limite = $this->session->userdata('cont_limite');
			$lim = $limite + 30;
			$this->session->set_userdata(array('cont_limite'=>$lim));
		}
		$this->load->model('Contrato','',TRUE);
		$datos = $this->Contrato->listar_contratos_colonia($colonia, $limite);
		echo json_encode($datos);
	}

	public function relistar_contratos_colonias()
	{
		$colonia = $this->input->post('colonia');
		$limite = $this->input->post('limite');
		$limite = $this->session->userdata('cont_limite');
		$limite = $limite - 30;
		$this->load->model('Contrato','',TRUE);
		$datos = $this->Contrato->listar_contratos_colonia($colonia, $limite);
		echo json_encode($datos);
	}

	private function _cargar_vista_edicion_contrato($contrato)
	{
		$this->load->model('Contrato','',TRUE);
		$datos['contrato'] = $this->Contrato->obtener_contrato_edicion($contrato);

		$zona_contrato = $this->Contrato->obtener_zona($contrato);
		$this->load->model('Zona','',TRUE);
		$datos['zonas'] = $this->Zona->listar_zonas();

		$this->load->model('Colonia','',TRUE);
		$datos['colonias'] = $this->Colonia->enlistar_colonias($zona_contrato);

		$this->load->model('Servicio','',TRUE);
		$datos['servicios'] = $this->Servicio->listar_servicios();
		
		$html = $this->load->view('catalogos/editar_contrato', $datos, TRUE);
		return $html;
	}

	private function _datos_no_encontrados($datos)
	{
		$html = $this->load->view('vacio',$datos,TRUE);
		echo "$html";
	}

	public function generar_numero_contrato()
	{
		
		$prefijo = $this->input->post('prefijo');
		if ($prefijo == 'CALK') {
			$prefijo = "$prefijo/";
		} else {
			$prefijo = $prefijo.'_';
		}
		if (isset($prefijo)) {
			
			$this->load->model('Contrato','',TRUE);
			$numero = $this->Contrato->generar_numero_contrato($prefijo);
			if ($numero == 0) {
				$numero = 1;
			}
			echo json_encode(array('numero'=>$numero));
		} else {
			echo json_encode(array('numero'=>0,'st'=>'0'));
		}
	}

	public function obtener_datos_contrato()
	{
		$numero = $this->input->post('numero');
		$this->load->model('Contrato', '', TRUE);
		$datos['nombre'] = $this->Contrato->obtener_nombre_contribuyente($numero);
		$datos['domicilio'] = $this->Contrato->obtener_domicilio_contrato($numero);
		$datos['contrato'] = $numero;
		$datos['ultimopago'] = $this->Contrato->obtener_fecha_pago($numero);
		echo json_encode($datos);
	}

	public function actualizar_ultimo_pago_contrato()
	{
		$numero = $this->input->post('numero');
		$ultimopago = $this->input->post('ultimopago');
		$this->load->model('Contrato', '', TRUE);
		$this->Contrato->modificar_ultimo_pago($ultimopago, $numero);

		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
	}
}
?>