<?php
/**
* 
*/
class Cobro_Parcial extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		// $menu = $this->session->userdata('menu');
		// $this->load->view($menu);
		
	}

	public function index()
	{	

		if ($this->session->userdata('sesion') != 1) {
			redirect('login');
		}
		$folio = $this->session->userdata('folio');
		$accion = $this->input->post('accion');
		if ($folio > 0) {
			switch ($accion) {
				case 'pagar':
					$this->_cobrar();
					break;
				case 'cancelar':
					$this->_cancelar();
					break;
				default:
					$this->_cargar_vista();
					break;
			}
		} else  {
			$menu = $this->session->userdata('menu');
			$this->load->view($menu);
			$this->load->view('recibo_no_encontrado');
		}
	}

	public function editar()
	{
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
		$accion = $this->input->post('accion');
		switch ($accion) {
			case 'agregar':
				$this->_cargar_vista_agregar_item();
				break;
			case 'bonificar':
				$this->load->model('Bonificacion','',TRUE);
				$datos['descs'] = $this->Bonificacion->obtener_bonificaciones();
				$this->load->view('agregar_descuento',$datos);
				break;
			default:
				redirect('cobro_parcial');
				break;
		}
	}

	public function agregar_item()
	{
		//$this->load->view('header');
		$this->form_validation->set_rules('periodoinicial','periodo inicial','required');
		$this->form_validation->set_rules('periodofinal','periodo final','required');

		if ($this->form_validation->run()) {
			$accion = $this->input->post('accion');
			if ($accion == 'cancelar') {
				redirect('cobro_parcial');
			} else {
				$this->_agregar_item($this->input->post('periodoinicial'), $this->input->post('periodofinal'));
			}
			
		} else {
			$this->_cargar_vista_agregar_item();
		}
	}

	public function agregar_descuento()
	{
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
		$this->session->set_userdata(array('bon'=>0));
		$this->form_validation->set_rules('descuento','bonificacion','required');
		if ($this->form_validation->run()) {
			$this->_aplicar_descuento($this->input->post('descuento'));
			// $descuento = 0.01 * $this->input->post('descuento');
			// $this->session->set_userdata(array('bon'=>$this->input->post('descuento')));
			// $this->session->set_userdata(array('pdescuento'=>$this->input->post('descuento')));
			// $this->load->model('Item','',TRUE);
			// $desc = $this->Item->buscar_items_del_periodo(date('Y'), $this->session->userdata('folio'), $descuento);
			// $this->load->model('Recibo','',TRUE);
			// $this->Recibo->agregar_descuento(array('descuento'=>$desc), $this->session->userdata('folio'));
			// redirect('cobro_parcial');
		} else {
			$this->load->model('Bonificacion','',TRUE);
			$datos['descs'] = $this->Bonificacion->obtener_bonificaciones();
			$this->load->view('agregar_descuento',$datos);
		}
	}

	private function _aplicar_descuento($iddescuento)
	{
		$this->load->model('Bonificacion','',TRUE);
		$bonificaciontotal = $this->Bonificacion->obtener_tipo_bonificacion($iddescuento);
		$porcentaje = $this->Bonificacion->obtener_porcentaje($iddescuento);
		$folio = $this->session->userdata('folio');
		$entero = $porcentaje / 0.01;
		if ($bonificaciontotal == 1) {
			$this->load->model('Item','',TRUE);
			$subtotal = $this->Item->obtener_subtotal($folio);
			$descuento = $subtotal * $porcentaje;
			$this->load->model('Recibo','',TRUE);
			$this->Recibo->agregar_descuento(array('descuento'=>$descuento), $folio);
			$this->session->set_userdata(array('bon' => $entero));
			$this->session->set_userdata(array('pdescuento' => $entero ));
			$this->load->model('Descuento','',TRUE);
			$this->Descuento->agregar_descuento(array('recibo'=>$folio,'concepto'=>$iddescuento,'monto'=>$descuento));
			redirect('cobro_parcial');
		} else {
			$this->load->model('Item','',TRUE);
			$desc = $this->Item->buscar_items_del_periodo(date('Y'), $folio, $porcentaje);
			$this->load->model('Recibo','',TRUE);
			$this->Recibo->agregar_descuento(array('descuento'=>$desc), $folio);
			$this->load->model('Descuento','',TRUE);
			$this->Descuento->agregar_descuento(array('recibo'=>$folio,'concepto'=>$iddescuento,'monto'=>$desc));
			redirect('cobro_parcial');
		}
	}

	private function _agregar_item($periodoinicial, $periodofinal)
	{
		//echo $this->session->userdata('zona');
		//echo $this->session->userdata('tipodeservicio')."<br>";
		$this->load->model('Periodo','',TRUE);
		$periodos= $this->Periodo->obtener_periodos($periodoinicial, $periodofinal);
		//echo "<br>";
		$this->load->model('Tarifa','',TRUE);
		$this->load->model('Item','',TRUE);
		$items = array();
		foreach ($periodos as $item) {
			$datos = $this->Tarifa->obtener_precio($item['periodo'], $this->session->userdata('zona'),$this->session->userdata('tipodeservicio'));
			if (sizeof($datos)) {
				$items = array( "recibo"=>$this->session->userdata('folio'),
					"periodo"=> $item['periodousuario'] , "cantidad"=> $item['cantidad'],'concepto'=>$datos[1],
					"preciounitario"=>$datos[0], "importe" => ($item['cantidad'] * $datos[0]) );
				$this->session->set_userdata(array('nuevoultimopago'=>$item['termino']));
				$this->Item->agregar_nuevo_item($items);
			} else {
				$errores = $this->session->userdata('errores_periodo');
				$errores = $errores.'<li>'.'No se ha declarado una tarifa para el periodo '.$item['periodousuario'].'</li>';
				$this->session->set_userdata(array('errores_periodo'=> $errores));
			}
		}
		redirect('cobro_parcial');
	}
	
	private function _cargar_vista()
	{
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
		$this->load->model('Item','',TRUE);
		$this->load->model('Recibo','',TRUE);
		$datos['items'] = $this->Item->obtener_items($this->session->userdata('folio'));
		$datos['subtotal'] = $this->Item->obtener_subtotal($this->session->userdata('folio'));
		$datos['descuento'] = $this->Recibo->obtener_descuento($this->session->userdata('folio'));
		$this->load->view('cobro_manual', $datos);
	}

	private function _cargar_vista_agregar_item()
	{
		//$this->load->view('header');
		$this->load->view('cobro_manual_agregar_item');
	}

	public function no_agregar_item()
	{
		redirect('cobro_parcial');
	}

	private function _cobrar()
	{
		$this->load->model('Recibo','',TRUE);
		$this->load->model('Item','',TRUE);
		$descuento = $this->Recibo->obtener_descuento($this->session->userdata('folio'));
		$subtotal = $this->Item->obtener_subtotal($this->session->userdata('folio'));
		if ($subtotal == 0) {
			$datos = array('mensaje'=>'No hay meses a cobrar','destino'=>site_url('cobro_parcial'));
			$this->load->view('vacio', $datos );
		} else {
			try {

				$ultimopago = $this->session->userdata('ultimopago');
				$nuevoultimopago = $this->session->userdata('nuevoultimopago');

				$periodoinicial = strftime('%Y%b', strtotime($ultimopago));
				$periodofinal = strftime('%Y%b', strtotime($nuevoultimopago));
				$periodoultimopago = strtoupper($periodoinicial).'-'.strtoupper($periodofinal);


				$datos = array(
								'periodo' => $periodoultimopago,
								'subtotal'=>$subtotal,
								'descuento'=>$descuento,
								'total'=>($subtotal-$descuento)
								);
				$this->Recibo->cerrar_cobro($datos, $this->session->userdata('folio'));
				$this->load->model('Contrato','',TRUE);
				$this->Contrato->actualizar_ultimo_pago($this->session->userdata('nuevoultimopago'), $this->session->userdata('contrato'));
				$this->load->model('Folio','',TRUE);
				$this->Folio->agregar_folio($this->session->userdata('folio'));
				$this->session->set_userdata(array('folio_recibo'=>$this->Folio->obtener_folio($this->session->userdata('folio'))));
				$this->session->set_userdata(array('errores_periodo'=>''));
				echo json_encode(array('errores'=>'0','folio'=>$this->session->userdata('folio')));
			} catch (Exception $e) {
				echo $e;	
			}
			//$this->_imprimir();
		}
		
	}

	private function _cancelar()
	{
		$this->load->model('Recibo','',TRUE);
		$this->Recibo->cancelar($this->session->userdata('folio'));
		$this->load->model('Contrato','',TRUE);
		$this->Contrato->actualizar_ultimo_pago($this->session->userdata('ultimopago'),$this->session->userdata('contrato'));
		$this->session->set_userdata(array('folio' => 0, 'errores_periodo'=>''));
		//redirect('buscar_contrato');
		echo json_encode(array('errores'=> $this->session->userdata('folio')));
	}

	public function imprimir()
	{
		//$this->load->view('buscar_contrato');
		$this->load->model('Item','',TRUE);
		$this->load->model('Recibo','',TRUE);
		$datos['items'] = $this->Item->obtener_items($this->session->userdata('folio'));
		$datos['subtotal'] = $this->Item->obtener_subtotal($this->session->userdata('folio'));
		$datos['descuento'] = $this->Recibo->obtener_descuento($this->session->userdata('folio'));
		$datos['folio_recibo'] = $this->session->userdata('folio_recibo');
		$this->load->helper('dompdf');

		$html = $this->load->view('recibo_cobro_automatico', $datos,TRUE);
		
		//$numero = $this->session->userdata('numero');
		$nombre = $this->session->userdata('nombrecompleto');
		$folio = $this->session->userdata('folio_recibo');
		$fecha = date('d-m-Y');
		
		$this->session->set_userdata(array('folio' => 0));
		pdf_create($html, "$folio.-.$nombre.-.$fecha");
	}
}
?>