<?php
/**
* 
*/
class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		if ($this->session->userdata('sesion') == 1) {
			$idusuario = $this->session->userdata('idusuario');
			$rol = $this->session->userdata('rol');
			$menu = $this->session->userdata('menu');

			$this->load->view($menu);

		} else {
			redirect('login');
		}
	}

	public function salir()
	{
		$this->session->set_userdata(array('sesion'=>0));
		redirect('login');
	}
}
?>