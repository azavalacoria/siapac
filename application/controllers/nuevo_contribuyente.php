<?php
/**
* 
*/
class Nuevo_Contribuyente extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('sesion') != 1) {
			redirect('login');
		}
		$this->form_validation->set_rules('apellidopaterno','apellido paterno','required|min_length[2]');
		if ($this->form_validation->run()) {
			$datos = array(
				'apellidopaterno'=> $this->input->post('apellidopaterno'),
				'apellidomaterno'=> $this->input->post('apellidomaterno'),
				'nombres' => $this->input->post('nombres'),
				'inicio' => 0,
				'limite' => 30
			);
			$this->session->set_userdata($datos);
			$this->_cargar_listado();
		} else {
			$this->load->view('buscar_ciudadano');
		}
	}

	private function _buscar_siguientes()
	{
		$inicio = $this->session->userdata('inicio');
		if ( $this->session->userdata('inicio') >= 0) {
			$this->session->set_userdata(array('inicio'=>($inicio + 30)));
		}
		$this->session->set_userdata(array('inicio'=>($inicio + 30)));
		$this->_cargar_listado();
	}

	private function _buscar_anteriores()
	{
		$inicio = $this->session->userdata('inicio');
		if ($inicio >= 30) {
			$this->session->set_userdata( array( 'inicio' =>($inicio - 30 ) ) );
			$this->_cargar_listado();
		}
	}

	private function _cargar_listado()
	{
		$apellidopaterno = strtoupper($this->session->userdata('apellidopaterno'));
		$apellidomaterno = strtoupper($this->session->userdata('apellidomaterno'));
		$nombres = strtoupper($this->session->userdata('nombres'));
		//echo "$apellidopaterno $apellidomaterno $nombres";
		$inicio = $this->session->userdata('inicio');
		$limite = $this->session->userdata('limite');
		//echo " $inicio $limite";
		$this->load->model('Ciudadano','',TRUE);
		$resultados = $this->Ciudadano->buscar_ciudadano($apellidopaterno, $apellidomaterno, $nombres, $inicio, $limite);
		$datos['encontrados'] = $resultados->num_rows();
		$datos['ciudadanos'] = $resultados->result();
		$this->load->view('listar_ciudadanos', $datos);
	}
	public function buscar()
	{
		$opcion = $this->input->post('opcion');
		if ( $opcion == 1) {
			$this->_buscar_siguientes();
		} else {
			$this->_buscar_anteriores();
		}
	}

	public function limpiar()
	{
		$this->_limpiar();
		$this->index();
	}

	private function _limpiar()
	{
		$this->session->unset_userdata(array('inicio'=> 0,'limite'=>0));
		$this->session->unset_userdata(array('apellidopaterno'=> null,'apellidomaterno'=> null, 'nombres'=> null));
	}

	public function seleccionar()
	{
		$this->form_validation->set_rules('clave','opcion','required');
		if ($this->form_validation->run()) {
			$this->load->model('Contribuyente','',TRUE);
			$existe = $this->Contribuyente->existe_contribuyente($this->input->post('clave'));
			if ($existe == 1) {
				$result['mensaje'] = "El ciudadano ya se encuentra registrado como contribuyente, <br> verifique sus datos de busqueda";
				$result['destino'] = site_url('nuevo_contribuyente');
				$this->_limpiar();
				$this->load->view('vacio', $result);
			} else {
				$this->load->model('Ciudadano','',TRUE);
				$datos['contribuyente'] = $this->Ciudadano->obtener_ciudadano($this->input->post('clave'));
				$this->load->view('nuevo_contribuyente', $datos);
			}
		} else {
			$this->_cargar_listado();
		}
		//$this->load->view('buscar_ciudadano');
	}

	public function agregar()
	{
		$this->form_validation->set_rules('claveelector','clave de elector','trim');
		$this->form_validation->set_rules('nombres','nombres','trim|required');
		$this->form_validation->set_rules('apellidopaterno','apellido paterno','trim|required');
		$this->form_validation->set_rules('apellidomaterno','apellido materno','trim|required');
		$this->form_validation->set_rules('fechanacimiento','fecha de nacimiento','trim|required');
		$this->form_validation->set_rules('colonia','colonia','trim|required');
		$this->form_validation->set_rules('localidad','localidad','trim|required');
		$this->form_validation->set_rules('calle','calle','trim|required');
		$this->form_validation->set_rules('numero','número exterior','trim|required');
		$this->form_validation->set_rules('telefono','teléfono','trim|numeric');
		$this->form_validation->set_rules('celular','celular','trim|numeric');
		$this->form_validation->set_rules('email','correo electrónico','trim|valid_email');
		if ($this->form_validation->run()) {
			$claveelector = strtoupper($this->input->post('claveelector'));
			// ACCNRM24020204H400
			//if ($claveelector = '' || $claveelector = " " || $claveelector = 0) 
			if ($claveelector == '' ) {
				$claveelector = $this->_generar_clave_elector(
					strtoupper($this->input->post('apellidopaterno')),
					strtoupper($this->input->post('apellidomaterno')),
					strtoupper($this->input->post('nombres')),
					strtoupper($this->input->post('fechanacimiento'))
				);
			}
			$this->load->model('Contribuyente','',TRUE);
			$existe = $this->Contribuyente->existe_contribuyente($claveelector);
			//echo "$claveelector";
			if ($existe == 0) {
				$rfc = $this->_validar_nulo($this->input->post('rfc'));
				$telefono = $this->_validar_nulo($this->input->post('telefono'));
				$celular = $this->_validar_nulo($this->input->post('celular'));
				$datos = array(
					'claveelector' => strtoupper($claveelector),
					'nombres' => strtoupper($this->input->post('nombres')),
					'apellidopaterno' => strtoupper($this->input->post('apellidopaterno')),
					'apellidomaterno' => strtoupper($this->input->post('apellidomaterno')),
					'fechanacimiento' => $this->input->post('fechanacimiento'),
					'colonia' => strtoupper($this->input->post('colonia')),
					'loc' => strtoupper($this->input->post('localidad')),
					'calle' => strtoupper($this->input->post('calle')),
					'numero'=> strtoupper($this->input->post('numero')),
					'telefono' => $telefono,
					'celular' => $celular,
					'email' => $this->input->post('email'),
					'rfc' => $rfc
					);
				$this->Contribuyente->agregar($datos);
				$result['mensaje'] = "Contribuyente agregado con éxito.";
				$this->_limpiar();
				$this->load->view('vacio', $result);
			} else {
				$result['mensaje'] = "Error, el contribuyente ya ha sido dado de alta.";
				$this->_limpiar();
				$this->load->view('vacio', $result);
			}
			
		} else {
			$this->load->view('nuevo_contribuyente_recargado');
		}

	}

	public function no_encontrado()
	{
		$this->load->view('nuevo_contribuyente_recargado');
	}

	public function todos()
	{
		$this->load->model('Contribuyente','',TRUE);
		$datos['contribuyentes'] = $this->Contribuyente->obtener_contribuyentes();
		$this->load->view('listar_contribuyentes', $datos);
	}

	private function _validar_nulo($valor)
	{
		if ($valor == '' || $valor == 0) {
			$valor = null;
		} else {
			$valor = strtoupper($valor);
		}
		return $valor;
	}

	private function _generar_clave_elector($apellidopaterno, $apellidomaterno, $nombres, $fechanacimiento)
	{
		$digitoap = substr($apellidopaterno, 0, 2);
		$digitoam = substr($apellidomaterno, 0, 1);
		$digitonb = substr($nombres, 0, 1);
		
		$anio = substr($fechanacimiento, 2, 2);
		$mes =  substr($fechanacimiento, 5, 2);
		$dia =  substr($fechanacimiento, 8, 2);

		$aleatorio = rand(10000, 99999);

		return "".$digitoap.$digitoam.$digitonb.$anio.$mes.$dia.$aleatorio;
	}
}
?>