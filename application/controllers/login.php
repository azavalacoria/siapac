<?php
/**
* 
*/
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->view('simple_header');
	}

	public function index()
	{
		if ($this->session->userdata('sesion') == 1) {
			redirect('home');
		} else {
			redirect('login/iniciar');
		}
	}
	public function iniciar()
	{

		$this->form_validation->set_rules('usuario','nombre de usuario','trim|required');
		$this->form_validation->set_rules('password','contraseña', 'trim|required');
		if ($this->form_validation->run()) {
			$nickname = $this->input->post('usuario');
			$password = sha1($this->input->post('password'));
			$this->load->model('Usuario', '', TRUE);
			$existe = $this->Usuario->existe($nickname, $password);
			if ($existe == 1) {
				$this->session->set_userdata(array('sesion'=>1));
				$permisos = $this->Usuario->obtener_permisos($nickname);
				if (sizeof($permisos) > 0 ) {
					$this->session->set_userdata($permisos);
					redirect('home');
				}
			} else {
				$this->load->view('vacio',array('mensaje'=>'No se hallaron resultados'));
			}
		} else {
			$this->load->view('login');
		}
	}
}
?>