<?php
/**
* 
*/
class Import extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
		$this->load->view('importacion/cargar_archivo');
	}

	public function subir_archivo()
	{
		$ruta = FCPATH.'sync';
		$nombre = strftime('exportacion_-_%Y_%m_%d_-_%H_%M_%S.xml');
		$config['upload_path'] = $ruta;
		$config['allowed_types'] = '*';
		$config['max_size'] = '12048';
		$config['file_name'] = $nombre;
		$config['overwrite'] = TRUE;
		$config['remove_space'] = TRUE;
		$config['encrypt_name'] = TRUE;
		$archivo = $this->input->post('archivo');

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('archivo')) {
			$archivo = $ruta."/".$nombre;
			$menu = $this->session->userdata('menu');
			$this->load->view($menu);
			if (file_exists($archivo)) {
				$datos['mensaje'] = "Su archivo $archivo fue añadido correctamente";
				$datos['destino'] = site_url('import');
				$this->load->view('vacio', $datos);
				$this->simplexml($archivo);
			}
			
		} else {
			$error = array('error'=> $this->upload->display_errors());

			foreach ($error as $e) {
				echo "$e <br>";
			}
		}
	}

	public function simplexml($archivo)
	{
		//$path = "sync/Sincronizacion_2013-09-21.-.2013-09-21 22-23-26.xml";
		$path = $archivo;
		if (file_exists($path)) {
			$dom = new DOMDocument();
			$dom->load($path);
			$dom->preserveWhiteSpace = FALSE;
			$this->load->model('Sincronizador','',TRUE);
		}
		$salida = array();
		if ($dom) {
			$ct = simplexml_import_dom($dom);
			$c = 0;
			//echo strftime("Importacion iniciada a las %r <br>");
			while (isset($ct->contrato[$c])) {
				
				$existe = $this->_existe_contrato($ct->contrato[$c]->numero);
				if ($existe > 0) {
					$cnumero = "".$ct->contrato[$c]->numero;
					$salidacontrato = array('numero' => $cnumero);
					$salidarecibos = array();

					$idcontrato = $this->_obtener_id_contrato(trim($ct->contrato[$c]->numero));
					$ultimopago = trim($ct->contrato[$c]->ultimo_pago);

					if ($ct->contrato[$c]->recibos) {
						foreach ($ct->contrato[$c]->recibos as $recibos) {
							foreach ($recibos as $recibo) {
								$arecibo = array(
										'fecha_cobro' => trim($recibo->fecha_cobro),
										'periodo' => trim($recibo->periodo),
										'subtotal' => trim($recibo->subtotal),
										'descuento' => trim($recibo->descuento),
										'total' => trim($recibo->total),
										'observaciones' => trim($recibo->observaciones),
										'modulo' => 1,
										'usuario' => 1
								);
								$idrecibo = $this->_verificar_recibo($arecibo, $idcontrato);
								array_push($salidarecibos, "Se ha creado el recibo con el id: $idrecibo. ");
								if (isset($recibo->items) && $idrecibo > 0 ) {
									foreach ($recibo->items as $items) {
										foreach ($items as $item) {
											$aitem = array(
												'periodo' => trim($item->periodo),														'cantidad' => trim($item->cantidad),
												'concepto' => trim($item->concepto),
												'precio_unitario' => trim($item->precio_unitario),
												'importe' => trim($item->importe),
												'armonizacion_uno' => trim($item->armonizacion_uno),
												'armonizacion_dos' => trim($item->armonizacion_dos)
											);
											$this->_verificar_item($aitem, $idrecibo);
										}
									}
								}
							}
						}
						$this->_actualizar_ultimo_pago_contrato($ultimopago, $idcontrato);
						array_push($salidarecibos, "El el ultimo pago del contrato ha sido actualizado. ");
						$salidacontrato['salidarecibos'] = $salidarecibos;
					} else {
						array_push($salidarecibos, "El  contrato no tiene recibos. ");
						$salidacontrato['salidarecibos'] = $salidarecibos;
					}
				} else {
					array_push($salidacontrato, "El numero de contrato no existe. ");
				}
				$c++;
				array_push($salida, $salidacontrato);
			}
			//echo "se recorrieron $c registros";
			//echo strftime("<br><br>Importacion finalizada a las %r <br>");
			echo "<br>";

			echo "<br>";
			print_r($salida);
		} else {
			echo "el archivo no contiene datos";
		}

	}

	private function _existe_contrato($numero)
	{
		$this->load->model('Contrato','',TRUE);
		$existe = $this->Contrato->contrato_existe($numero);
		return $existe;
	}

	private function _obtener_id_contrato($numero)
	{
		$this->load->model('Contrato','',TRUE);
		$idcontrato = $this->Contrato->obtener_id_contrato($numero);
		return $idcontrato;
	}

	private function _actualizar_ultimo_pago_contrato($ultimopago, $idcontrato)
	{
		$this->load->model('Contrato','',TRUE);
		$idcontrato = $this->Contrato->actualizar_ultimo_pago($ultimopago, $idcontrato);
		return $idcontrato;
	}

	private function _verificar_recibo($recibo, $idcontrato)
	{
		//$contrato = $this->Sincronizador->obtener_id_contrato($numerocontrato);
		$datos = array(
			'contrato' => $idcontrato,
			'fechacobro' => $this->_validar_nulo($recibo['fecha_cobro']),
			'periodo' => $this->_validar_nulo($recibo['periodo']),
			'subtotal' => $this->_validar_nulo($recibo['subtotal']),
			'descuento' => $this->_validar_nulo($recibo['descuento']),
			'total' => $this->_validar_nulo($recibo['total']),
			'observaciones' => $this->_validar_nulo($recibo['observaciones']),
			'modulo' => $this->_validar_nulo($recibo['modulo']),
			'usuario' => $this->_validar_nulo($recibo['usuario'])
			);
		$this->load->model('Sincronizador','', TRUE);
		$existe = $this->Sincronizador->existe_recibo($datos);
		if ($existe == 0) {
			$this->Sincronizador->insertar_recibo($datos);
			//echo "Existe = $existe";
			return $this->Sincronizador->obtener_id_recibo($datos);
		} else {
			//echo "El recibo existe, no se agrego <br>";
		}
		
		
	}

	public function _verificar_item($item ,$folio)
	{
		$datos = array(
			'recibo' => $folio,
			'periodo' => $this->_validar_nulo($item['periodo']),
			'cantidad' => $this->_validar_nulo($item['cantidad']),
			'concepto' => $this->_validar_nulo($item['concepto']),
			'preciounitario' => $this->_validar_nulo($item['precio_unitario']),
			'importe' => $this->_validar_nulo($item['importe']),
			'armonizacionuno' => $this->_validar_nulo($item['armonizacion_uno']),
			'armonizaciondos' => $this->_validar_nulo($item['armonizacion_dos']),
			);
		//echo "------>";
		//print_r($item['armonizacion_uno']);
		//echo "<------ <br>";
		//print_r($datos);
		//echo "<br>";
		$this->load->model('Sincronizador','', TRUE);
		$existeit = $this->Sincronizador->existe_item($datos);
		if($existeit == 0){
			$datos['recibo'] = $folio;
			$this->Sincronizador->insertar_item($datos);
			//echo "item agregado<br>";
		} else {
			//echo "<br>----- no se agrego item -----<br>";
		}
	}

	private function _validar_nulo($String)
	{
		try {
		    $String = trim($String);
			switch ($String) {
				case ' NULL ':
					$String = NULL;
					break;
				case 'NULL':
					$String = NULL;
					break;
				case '':
					$String = "-";
					break;
				case ' ':
					$String = " ";
					break;
				case NULL:
					$String = NULL;
					break;
				default:
					break;
			}
		} catch (Exception $e) {
			echo $e->getMessage();
			echo "TRError: ";
			print_r($valor);
			log_message(print_r($valor));
			//var_dump($valor);
		}
		return $String;
	}
}
?>