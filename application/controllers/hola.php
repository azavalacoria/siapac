<?php
/**
* 
*/
class Hola extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('Periodo','',TRUE);
		$this->load->model('Tarifa','',TRUE);
		$periodos = $this->Periodo->obtener_periodos('2010-01-01','2013-01-27');
		$items = array();
		$errores = '';
		foreach ($periodos as $p) {
			$datos = $this->Tarifa->obtener_precio($p['periodo'], 7, 1);
			if (sizeof($datos)) {
				$item = array( 
					"periodo"=> $p['periodousuario'],
					"cantidad"=> $p['cantidad'],
					'concepto'=>$datos[1],
					"preciounitario"=>$datos[0], 
					"importe" => ($p['cantidad'] * $datos[0]) );
					array_push($items, $item);
			} else {
				$errores = $errores.'<li>'.'No se ha declarado una tarifa para el periodo '.$item['periodousuario'].'</li>';
			}
		}
		$total = 0.0;
		foreach ($items as $i) {
			$total = $total + $i['importe'];
		}
		$datos = array('items' => $items, 'errores' => $errores, 'total' => $total);
		echo json_encode($datos);
	}

	public function rep()
	{
		$this->form_validation->set_rules('hola','gol','trim');
		if ($this->form_validation->run()) {
			$hola = $this->input->post('hola');
			if ($hola == 1) {
				echo "gol";
			} else {
				echo "nada";
			}
			
		} else {
			$this->load->view('hola');
		}
		
	}

	public function contu()
	{
		$this->load->model('Contrato','',TRUE);
		$datos = $this->Contrato->listar_contratos_colonia('1122', 60);
		echo json_encode($datos);
	}

	public function red()
	{
		$uno = 3280.1135371179;
		$dos =  481.8864628821;
		$u = round($uno, 3);
		$d = round($dos, 3);
		echo round($u , 2);
		echo "<br>";
		echo round($d , 2);
	}
	public function ar()
	{
		$this->load->model('Sincronizador','',TRUE);
		$contribuyentes = $this->Sincronizador->obtener_ultimos_contribuyentes('2013-06-02',date("Y-m-d"));
		foreach ($contribuyentes as $id => $contribuyente) {
			echo "$id - $contribuyente <br>";
		}
		
	}

	public function sb()
	{
		setlocale(LC_ALL, '');
		//$periodo = strtoupper();
		echo strftime("%Y%b", strtotime(date('Y-01-01')));
	}

	public function contrato()
	{
		//echo $this->session->userdata('bon');
		$contrato = "CALK/1279";
		$this->load->model('Contrato','',TRUE);
		$datos['contrato'] = $this->Contrato->obtener_contrato_edicion($contrato);

		$zona_contrato = $this->Contrato->obtener_zona($contrato);
		$this->load->model('Zona','',TRUE);
		$datos['zonas'] = $this->Zona->listar_zonas();

		$this->load->model('Colonia','',TRUE);
		$datos['colonias'] = $this->Colonia->enlistar_colonias($zona_contrato);

		$this->load->model('Servicio','',TRUE);
		$datos['servicios'] = $this->Servicio->listar_servicios();
		
		$html = $this->load->view('catalogos/editar_contrato', $datos, TRUE);
		echo $html;
	}

	public function recibo()
	{
		//$this->load->view('recibo_cobro_automatico');
		$st = $this->session->userdata('errores_periodos');
		$st = $st."-<br>hola";
		echo "$st";
	}

	public function sinc()
	{
		$this->load->model('Sincronizacion','', TRUE);
		$periodoinicial = $this->Sincronizacion->obtener_ultima_sincronizacion();
		if ($periodoinicial == '0000-00-00') {
			$periodofinal = date('Y-m-d');
			echo "Sincronizacion de $periodoinicial a $periodofinal";
		} else {
			echo "cargare la vista";
		}
	}

	public function cargos_contrato()
	{
		$this->load->model('Tarifa','',TRUE);
		$data = $this->Tarifa->obtener_cargos_contrato(7);
		echo sizeof($data);
		echo "<br>";
		setlocale(LC_ALL, '');
		$periodo = strtoupper(strftime("%Y%h-%Y%h"));
		$cargos = array();
		foreach ($data as $cargo) {
			$cargo = array(
				"periodo" => $periodo,
				"cantidad" => 1,
				"concepto" => $cargo->nombreservicio,
				"preciounitario" => $cargo->precio,
				"importe" => $cargo->precio
				);
			print_r($cargo);
			echo "<br>";
			array_push($cargos, $cargo);
		}
	}

	public function obtener_reporte()
	{
		$this->load->model('Corte','',TRUE);
		$recibo = "223";
		$rezago = $this->Corte->obtener_rezago_recibo($recibo);
		$actual = $this->Corte->obtener_ingreso_actual_recibo($recibo);
		$contratacion = $this->Corte->obtener_ingreso_contratacion_recibo($recibo);
		if ($rezago == 0) {
			echo "Rezagos: 0";
		} else {
			echo "Rezagos: $rezago ";
		}
		if ($actual == 0) {
			echo " - Actual: 0 ";
		} else {
			echo " - Actual: $actual -";
		}
		if ($contratacion == 0) {
			echo " - Contratacion: 0";
		} else {
			echo " - Contratacion: $contratacion";
		}
	}

	public function obtener_folios()
	{
		$this->load->model('Corte','',TRUE);
		$datos = $this->Corte->obtener_folios_por_dia('2013-07-02');
		$folios = array();
		foreach ($datos as $folio) {
			$rezago = $this->Corte->obtener_rezago_recibo($folio->recibo);
			$actual = $this->Corte->obtener_ingreso_actual_recibo($folio->recibo);
			$contratacion = $this->Corte->obtener_ingreso_contratacion_recibo($folio->recibo);
			if ($rezago == 0) {
				$rezago = "0";
			}
			if ($actual == 0) {
				$actual = "0";
			}
			if ($contratacion == 0) {
				$contratacion = "0";
			}
			$folio = array(
				'folio' => $folio->numero,
				'rezago' => $rezago,
				'actual' => $actual,
				'contratacion' => $contratacion,
				'bonificacion' => $folio->descuento,
				'total' => $folio->total
				);
			array_push($folios, $folio);
		}
		$this->load->view('reportes/reporte_diario', array('folios'=> $folios));
	}

	public function presupuesto()
	{
		$contrato = "CALK/1374";

		$this->load->model('Contrato','',TRUE);
		$id_contrato = $this->Contrato->obtener_id_contrato($contrato);
		$ultimo_pago = $this->Contrato->obtener_ultimo_pago($id_contrato);
		$zona = $this->Contrato->obtener_zona($contrato);
		$tipo_de_servicio = $this->Contrato->obtener_tipo_servicio($contrato);


		$this->load->model('Periodo','',TRUE);
		$periodos= $this->Periodo->obtener_periodos($ultimo_pago, date("Y-m-d"));

		$this->load->model('Tarifa','',TRUE);

		$items = array();
		foreach ($periodos as $item) {
			$datos = $this->Tarifa->obtener_precio($item['periodo'], $zona, $tipo_de_servicio);
			if (sizeof($datos)) {
				$i = array(
					"periodo"=> $item['periodousuario'] , 
					"cantidad"=> $item['cantidad'],
					'concepto'=>$datos[1],
					"preciounitario"=>$datos[0],
					"importe" => ($item['cantidad'] * $datos[0])
					);
				array_push($items, $i);
			} else {
				$errores = $this->session->userdata('errores_periodo');
				$errores = $errores.'<li>'.'No se ha declarado una tarifa para el periodo '.$item['periodousuario'].'</li>';
				$this->session->set_userdata(array('errores_periodo'=> $errores));
			}
		}

		foreach ($items as $item) {
			print_r($item);
			echo "<br>";
		}
	}

	public function cont()
	{
		$this->load->model('Contrato','',TRUE);
		$numero = $this->Contrato->generar_numero_contrato('BECA/');
		echo "$numero";
	}
}
?>