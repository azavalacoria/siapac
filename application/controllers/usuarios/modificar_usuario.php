<?php
/**
* 
*/
class Modificar_Usuario extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{

		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		} else {
			$this->_cargar_vista_seleccion();
		}
	}

	public function _cargar_vista_seleccion()
	{
		$this->load->model('Usuario','usuario',TRUE);
		$datos['usuarios'] = $this->usuario->obtener_usuarios();
		$this->load->view('usuarios/seleccionar_usuario', $datos);

	}

	public function seleccionar()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('opcion','usuario','required');
		if ($this->form_validation->run()) {
			$opcion = $this->input->post('opcion');
			$this->_cargar_vista_modificacion($opcion);
		} else {
			$this->_cargar_vista_seleccion();
		}
		
	}

	public function _cargar_vista_modificacion($opcion)
	{
		$this->load->model('Rol','rol',TRUE);
		$datos['roles'] = $this->rol->listar_roles();

		if (isset($opcion)) {
			$this->load->model('Usuario','usuario',TRUE);
			$usuario = $this->usuario->obtener_usuario($opcion);
			if (isset($usuario)) {
				$datos['usuario'] = $usuario;
				$this->load->view('usuarios/modificar_usuario', $datos);
			} else {
				$this->_cargar_vista_seleccion();
			}
		} elseif ($opcion == null) {
			$this->_cargar_vista_seleccion();
		} else {
			$datos['idusuario'] = $this->input->post('idusuario');
			$this->load->view('usuarios/modificar_usuario_recargado', $datos);
		}
	}

	public function modificar()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('nombre','nombre completo','required');
		$this->form_validation->set_rules('nickname','nombre de usuario','required|alpha_numeric');
		$this->form_validation->set_rules('password','contraseña','matches[vpassword]');
		$this->form_validation->set_rules('vpassword','repetir contraseña','matches[password]');
		$this->form_validation->set_rules('rol','rol','required');
		if ($this->form_validation->run()) {
			$password = $this->input->post('password');
			$idusuario = $this->input->post('idusuario');
			$usuario = null;
			if (strlen($password) > 0) {
				$usuario = array(
					'nombrecompleto'=>$this->input->post('nombre'),
					'nickname' => $this->input->post('nickname'),
					'password' => sha1($this->input->post('password')),
					'rol' => $this->input->post('rol')
					);
			} else {
				$usuario = array(
					'nombrecompleto'=>$this->input->post('nombre'),
					'nickname' => $this->input->post('nickname'),
					'rol' => $this->input->post('rol')
					);
			}

			$this->load->model('Usuario','usuario', TRUE);
			$this->usuario->actualizar_usuario($usuario, $idusuario);
			$datos = array(
				'mensaje'=>'Su usuario se modificó con éxito.',
				'destino'=>site_url('usuarios/agregar_usuario'));
			$this->load->view('vacio', $datos );
		} else {
			$this->_cargar_vista_modificacion($this->input->post('idusuario'));
		}
		
	}
}
?>