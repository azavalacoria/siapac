<?php

/**
* 
*/
class Agregar_Usuario extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('nombre','nombre completo del usuario','required');
		$this->form_validation->set_rules('nickname','nombre de sesión','required|alpha_numeric');
		$this->form_validation->set_rules('vpassword','repetir contraseña','matches[password]');
		$this->form_validation->set_rules('password','contraseña','required');
		$this->form_validation->set_rules('rol','rol','required|integer');

		if ($this->form_validation->run()) {
			$usuario = array(
					'nombrecompleto' => $this->input->post('nombre'),
					'nickname' => $this->input->post('nickname'),
					'password' => sha1($this->input->post('password')),
					'rol' => $this->input->post('rol'),
					'modulo' => 1
				);
			
			$this->load->model('Usuario','usuario', TRUE);
			$this->usuario->agregar_usuario($usuario);
			$datos = array(
				'mensaje'=>'Su usuario se agregó con éxito.',
				'destino'=>site_url('usuarios/agregar_usuario'));
			$this->load->view('vacio', $datos );
			
			//print_r($usuario);
		} else {
			$this->_cargar_vista();
		}
		

	}

	public function _cargar_vista()
	{
		$this->load->model('Rol','rol',TRUE);
		$datos['roles'] = $this->rol->listar_roles();
		$this->load->view('usuarios/agregar_usuario',$datos);
	}
}
?>