<?php
/**
* 
*/
class Cobro_Automatico extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{	
		if ($this->session->userdata('sesion') != 1) {
			redirect('login');
		}
		$folio = $this->session->userdata('folio');
		if ($folio > 0) {
			$accion = $this->input->post('accion');
			if ($accion == "pagar") {
				$this->_cobrar();
			} elseif ($accion == "cancelar") {
				$this->_cancelar();
			} else {
				$this->_cargar_vista();
			}
		} else  {
			$this->load->view('recibo_no_encontrado');
		}
	}

	public function calcular()
	{
		$folio = $this->session->userdata('folio');
		if ($folio > 0) {
			$this->_calcular_meses();
		}
	}

	private function _calcular_meses()
	{
		$this->load->model('Contrato','',TRUE);
		$ultimopago = $this->Contrato->obtener_ultimo_pago($this->session->userdata('contrato'));
		$fi = new DateTime($ultimopago);
		$ff = new DateTime(date("Y-m-d"));
		$intervalo = $fi->diff($ff);
		$dif = $intervalo->y + $intervalo->m;
		if ($dif == 0 ) {
			$datos = array('mensaje'=>'Está al día con su pago, intente en el apartado de Cobro Parcial');
			$this->load->view('vacio', $datos);
		} else {
			$periodoinicial = strftime("%Y-%m-%d", strtotime("first day of next month", strtotime($ultimopago)));
			$periodofinal = date("Y-m-d");
			$this->_generar_items($periodoinicial, $periodofinal);
		}
		
	}

	private function _generar_items($periodoinicial, $periodofinal)
	{
		$this->load->model('Periodo','',TRUE);
		$periodos= $this->Periodo->obtener_periodos($periodoinicial, $periodofinal);
		if (count($periodos) <= 0) {
			$this->error();
		} else {
			$this->load->model('Tarifa','',TRUE);
			$this->load->model('Item','',TRUE);
			$items = array();
			foreach ($periodos as $item) {
				$datos = $this->Tarifa->obtener_precio($item['periodo'], $this->session->userdata('zona'),$this->session->userdata('tipodeservicio'));
				echo count($datos);
				if (count($datos) <= 0 ) {
					echo "No encontraron tarifas";
				} else {
					$items = array( "recibo"=>$this->session->userdata('folio'),
						"periodo"=> $item['periodousuario'] , "cantidad"=> $item['cantidad'],'concepto'=>$datos[1],
						"preciounitario"=>$datos[0], "importe" => ($item['cantidad'] * $datos[0]) );
					$this->Item->agregar_nuevo_item($items);
				}
			}
			redirect('cobro_automatico');
		}
	}

	private function _cargar_vista()
	{
		$this->load->model('Item','',TRUE);
		$this->load->model('Recibo','',TRUE);
		$datos['items'] = $this->Item->obtener_items($this->session->userdata('folio'));
		$datos['subtotal'] = $this->Item->obtener_subtotal($this->session->userdata('folio'));
		$datos['descuento'] = $this->Recibo->obtener_descuento($this->session->userdata('folio'));
		$this->load->view('cobro_automatico', $datos);
	}

	private function _cobrar()
	{
		$this->load->model('Recibo','',TRUE);
		$this->load->model('Item','',TRUE);
		$this->load->model('Contrato','',TRUE);
		$descuento = $this->Recibo->obtener_descuento($this->session->userdata('folio'));
		$subtotal = $this->Item->obtener_subtotal($this->session->userdata('folio'));
		$datos = array('subtotal'=>$subtotal,'descuento'=>$descuento,'total'=>($subtotal-$descuento));
		$this->Recibo->cerrar_cobro($datos, $this->session->userdata('folio'));
		$this->Contrato->actualizar_ultimo_pago(date("Y-m-d"), $this->session->userdata('contrato'));

		$this->load->model('Folio','',TRUE);
		$this->Folio->agregar_folio($this->session->userdata('folio'));
		$this->session->set_userdata(array('folio_recibo'=>$this->Folio->obtener_folio($this->session->userdata('folio'))));

		$this->_imprimir();
	}

	private function _cancelar()
	{
		$this->load->model('Recibo','',TRUE);
		$this->load->model('Contrato','',TRUE);
		$this->Contrato->actualizar_ultimo_pago($this->session->userdata('ultimopago'),$this->session->userdata('contrato'));
		$this->Recibo->cancelar($this->session->userdata('folio'));
		$this->session->set_userdata(array('folio' => 0));	
	}

	private function _imprimir()
	{
		$this->load->model('Item','',TRUE);
		$this->load->model('Recibo','',TRUE);
		$datos['items'] = $this->Item->obtener_items($this->session->userdata('folio'));
		$datos['subtotal'] = $this->Item->obtener_subtotal($this->session->userdata('folio'));
		$datos['descuento'] = $this->Recibo->obtener_descuento($this->session->userdata('folio'));
		$datos['folio_recibo'] = $this->session->userdata('folio_recibo');

		$this->load->helper('dompdf');

		$html = $this->load->view('recibo_cobro_automatico', $datos,TRUE);

		$this->session->set_userdata(array('folio' => 0));
		pdf_create($html, "recibo");
	}

	public function error()
	{
		echo "error";
	}
}
?>