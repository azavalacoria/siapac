<?php

/**
* 
*/
class Buscar_Contrato extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	function index()
	{
		if ($this->session->userdata('sesion') != 1) {
			redirect('login');
		}
		if (!$this->form_validation->run()) {
			$this->load->view('buscar_contrato');
		}
		
	}

	public function por_nombre()
	{
		$this->form_validation->set_rules('apellidopaterno','apellido paterno','trim|alpha_numeric|required');
		$this->form_validation->set_rules('apellidomaterno','apellido materno','trim');
		$this->form_validation->set_rules('apellidomaterno','nombres','trim');
		if ($this->form_validation->run()) {
			$this->_buscar_por_nombre(
				strtoupper($this->input->post('apellidopaterno')),
				strtoupper($this->input->post('apellidomaterno')),
				strtoupper($this->input->post('nombres'))
				);
		} else {
			$this->load->view('buscar_contrato');
		}
	}

	public function por_contrato()
	{
		$this->form_validation->set_rules('contrato','contrato','required');
		if ($this->form_validation->run()) {
			$this->_buscar_por_contrato($this->input->post('contrato'));
		} else {
			$this->load->view('buscar_contrato');
		}
	}

	private function _buscar_por_nombre($apellidopaterno , $apellidomaterno, $nombres)
	{
		$this->load->model('Contrato','',TRUE);
		$query = $this->Contrato->buscar_por_nombre($apellidopaterno, $apellidomaterno, $nombres , 0 , 30);
		if ($query->num_rows() == 0) {
			$this->load->view('vacio',array('mensaje'=>'No se hallaron resultados'));
		} else{
			$datos['contratos'] = $query->result();
			$datos['destino'] = 'cobrar';
			$datos['nueva_busqueda'] = "";
			$this->load->view('listar_contratos',$datos);
		}
	}

	private function _buscar_por_contrato($numero)
	{
		$this->load->model('Contrato','',TRUE);
		$query = $this->Contrato->buscar_por_numero($numero);
		if ($query->num_rows() == 0) {
			$this->load->view('vacio',array('mensaje'=>'No se hallaron resultados'));
		} else{
			$datos['patron'] = $numero;
			$datos['contratos'] = $query->result();
			$datos['destino'] = "cobrar";
			$this->load->view('listar_contratos',$datos);
		}
	}


}
?>