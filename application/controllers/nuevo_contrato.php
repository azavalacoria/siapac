<?php
/**
* 
*/
class Nuevo_Contrato extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('sesion') != 1) {
			redirect('login');
		} else {
			$this->buscar_contribuyente();
		}
	}

	public function buscar_contribuyente()
	{
		$this->form_validation->set_rules('apellidopaterno','apellido paterno','required|min_length[2]');
		if ($this->form_validation->run()) {
			$this->_buscar_contribuyente();
		} else {
			$this->load->view('buscar_contribuyente');
		}
	}

	private function _buscar_contribuyente()
	{
		$apellidopaterno = strtoupper($this->input->post('apellidopaterno'));
		$apellidomaterno = strtoupper($this->input->post('apellidomaterno'));
		$nombres = strtoupper($this->input->post('nombres'));

		$this->load->model('Contribuyente','',TRUE);
		$query = $this->Contribuyente->buscar_contribuyente($apellidopaterno, $apellidomaterno, $nombres);
		if ($query->num_rows() == 0) {
			$datos = array(
				'mensaje' => 'No se encontró a ese contribuyente. ',
				'etiqueta' => '¿Desea agregarlo?',
				'destino' => site_url('nuevo_contribuyente'));
			$this->load->view('vacio', $datos);
		} else {
			$datos['encontrados'] = $query->num_rows();
			$datos['contribuyentes'] = $query->result();
			$datos['run'] = 1;
			$this->load->view('listar_contribuyentes', $datos);
		}
	}

	public function buscar()
	{
		$inicio = $this->input->post('inicio');
		if ($inicio >= 0) {
			$this->session->set_userdata(array('inicio'=> 30));
			$this->_cargar_listado();
		}
	}

	private function _cargar_listado()
	{
		$this->load->model('Contribuyente','',TRUE);
		$query = $this->Contribuyente->obtener_contribuyente();
		$datos['encontrados'] = $query->num_rows();
		$datos['contribuyentes'] = $query->result();
		$this->load->view('listar_contribuyentes');
	}

	public function crear()
	{
		$this->form_validation->set_rules('contribuyente','Contribuyente','required');
		if ($this->form_validation->run()) {
			$this->session->set_userdata(array('idcontribuyente'=> $this->input->post('contribuyente')));
			$this->_cargar_vista_nuevo_contrato("Crear nuevo contrato");
		} else {
			$this->load->view('buscar_contribuyente');
		}
	}

	private function _cargar_vista_nuevo_contrato($mensaje)
	{
		$datos['mensaje'] = $mensaje;
		$this->load->model('Servicio','',TRUE);
		$datos['servicios'] = $this->Servicio->listar_servicios();
		$this->load->model('Contribuyente','',TRUE);
		$datos['contribuyente'] = $this->Contribuyente->obtener_nombre_contribuyente($this->session->userdata('idcontribuyente'));
		$this->load->model('Zona','',TRUE);
		$datos['zonas'] = $this->Zona->obtener_zonas();
		$datos['prefijos'] = $this->Zona->enlistar_prefijos_zonas();
		$this->load->model('Contrato','',TRUE);
		$numero = $this->input->post('numero');
		if (strlen($numero) > 0) {
			$datos['numerocontrato'] = strtoupper($numero);
			echo "exite $numero";
		} else {
			//$datos['numerocontrato'] = strtoupper($this->Contrato->generar_numero_contrato('CALK/'));
		}
		
		//$datos['numerocontrato'] = strtoupper($this->Contrato->generar_numero_contrato('CALK/'));
		$zona = $this->input->post('zona');
		if ($zona > 0) {
			$this->load->model('Colonia','',TRUE);
			$datos['colonias'] = $this->Colonia->enlistar_colonias($zona);
		} else {
			$datos['colonias'] = array('0'=>'Seleccione');
		}
		$this->load->view('nuevo_contrato', $datos);
		//echo $datos['numerocontrato'];
	}

	public function agregar()
	{
		$this->form_validation->set_rules('prefijo','iniciales','trim|required|min_length[2]');
		$this->form_validation->set_rules('numero','numero de contrato','trim|required|integer');
		$this->form_validation->set_rules('medidor','medidor','trim|required');
		$this->form_validation->set_rules('tiposervicio','tipo de servicio','trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('zona','zona','trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('colonia','colonia','trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('localidad','localidad','trim|required|max_length[4]');
		$this->form_validation->set_rules('calle','calle','trim|required');
		$this->form_validation->set_rules('numeroexterior','numero exterior','trim|required');
		$this->form_validation->set_rules('numerointerior','numero interior','trim|required');
		$this->form_validation->set_rules('cp','codigo postal','trim|required|max_length[5]');
		$this->form_validation->set_rules('ultimopago','último pago','trim|required');
		$this->form_validation->set_rules('referencias','referencias','trim');
		if ($this->form_validation->run()) {
			$numero = $this->_generar_numero_contrato($this->input->post('prefijo'), $this->input->post('numero'));
			$existe = $this->_contrato_existe($numero);
			if ($existe == 0) {
				$referencias = $this->input->post('referencias');
				if (!isset($referencias)) {
					$referencias = null;
				}
				$datos = array(
					'numero' => strtoupper($numero),
					'contribuyente' => $this->input->post('contribuyente'),
					'medidor' => $this->input->post('medidor'),
					'loc' => $this->input->post('localidad'),
					'colonia' => $this->input->post('colonia'),
					'calle' => $this->input->post('calle'),
					'numeroexterior' => $this->input->post('numeroexterior'),
					'numerointerior' => $this->input->post('numerointerior'),
					'referencias' => $referencias,
					'codigopostal' => $this->input->post('cp'),
					'fecha' => date("Y-m-d"),
					'observaciones' => $this->input->post('observaciones'),
					'ultimopago' => $this->input->post('ultimopago'),
					'zona' => $this->input->post('zona'),
					'tipodeservicio'=> $this->input->post('tiposervicio')
				);
				$this->load->model('Contrato','',TRUE);
				$this->Contrato->agregar_nuevo($datos);
				$this->load->model('Servicio','',TRUE);
				$nombreservicio = $this->Servicio->obtener_nombre_servicio($this->input->post('tiposervicio'));
				$this->load->model('Colonia','',TRUE);
				$nombrecolonia = $this->Colonia->obtener_nombre_colonia($this->input->post('colonia'));
				$direccion = "calle ".$datos['calle']." numero exterior ".$datos['numeroexterior']." ";
				if (!isset($datos['numerointerior']) || $datos['numerointerior'] == 0) {
					$direccion = $direccion.", ".$nombrecolonia;
				} else {
					$direccion = $direccion." Número interior ".$datos['numerointerior'].", ".$nombrecolonia;
				}
				$this->session->set_userdata(
					array(
						'ncontrato' => $numero,
						'tipotoma' => $nombreservicio,
						'cdireccion' => strtoupper($direccion),
						'nuevocontrato_zona' => $this->input->post('zona')
						)
					);
				$cobrable = $this->input->post('cobrable');
				if ($cobrable == 0) {
					$this->session->set_userdata(array('nuevocontrato_folio'=>0));
					$data['mensaje'] = "Contrato agregado con éxito"." sin cobrear";
					$this->load->view('imprimir_recibo_no_cobrable', $data);
					
				} else {
					$this->_crear_recibo($this->session->userdata('ncontrato'), $this->session->userdata('nuevocontrato_zona') );
					//echo "te estoy cobrrando";
				}
			} else {
				$this->_cargar_vista_nuevo_contrato("Error: Número de contrato duplicado");
			}
		} else {
			$this->_cargar_vista_nuevo_contrato("Verifique los siguientes datos:");
		}
	}

	private function _generar_numero_contrato($prefijo, $numero)
	{
		if ($prefijo != 'CALK') {
			return $prefijo."_".$numero;
		} else {
			return $prefijo."/".$numero;
		}
		
	}

	private function _crear_recibo($contrato, $zona)
	{
		$this->load->model('Contrato','',TRUE);
		$idcontrato = $this->Contrato->obtener_id_contrato($contrato);
		$this->load->model('Recibo','',TRUE);
		$recibo = $this->Recibo->crear_recibo($idcontrato, 1, 1);
		$this->session->set_userdata(array('nuevocontrato_folio'=>$recibo, 'nuevocontrato_numero'=>$contrato ));
		$this->_agregar_cargos_contrato($recibo, $zona);
		//echo "agregado items";
	}

	private function _agregar_cargos_contrato($recibo, $zona)
	{
		$this->load->model('Tarifa','',TRUE);
		$data = $this->Tarifa->obtener_cargos_contrato($zona);
		$existen_cargos = sizeof($data);
		if ($existen_cargos > 0) {
			setlocale(LC_ALL, '');
			$periodo = strtoupper(strftime("%Y%b"));
			//$periodo = $this->_generar_mes_periodo();
			$cargos = array();
			$this->load->model('Item','',TRUE);
			foreach ($data as $cargo) {
				$cargos = array(
					"recibo" => $recibo,
					"periodo" => $periodo,
					"cantidad" => 1,
					"concepto" => $cargo->nombreservicio,
					"preciounitario" => $cargo->precio,
					"importe" => $cargo->precio
					);
				$this->Item->agregar_nuevo_item($cargos);
			}
			$this->_cerrar_recibo_contrato();
		}
	}

	private function _cerrar_recibo_contrato()
	{
		$recibo = $this->session->userdata('nuevocontrato_folio');
		$this->load->model('Recibo','',TRUE);

		$descuento = $this->Recibo->obtener_descuento($recibo);
		$this->load->model('Item','',TRUE);
		$subtotal = $this->Item->obtener_subtotal($recibo);
		$data['items'] = $this->Item->obtener_items($recibo);
		$datos = array( 'subtotal' => $subtotal, 'descuento' => $descuento, 'total' => ($subtotal - $descuento) );
		$this->Recibo->cerrar_cobro($datos, $recibo);
		$this->load->model('Folio','',TRUE);
		$this->Folio->agregar_folio($recibo);
		$this->session->set_userdata(array('folio_recibo'=>$this->Folio->obtener_folio($recibo)));
		$this->session->set_userdata(array('errores_periodo'=>''));
		$data['mensaje'] = "Contrato agregado con éxito";
		$this->load->view('imprimir_recibo', $data);
	}

	public function imprimir()
	{
		$this->load->helper('dompdf');
		$html = $this->load->view('plantilla_contrato', array(),TRUE);
		pdf_create($html, "contrato");
	}

	public function imprimir_recibo_cobro()
	{
		$this->load->model('Item','',TRUE);
		$this->load->model('Recibo','',TRUE);
		$this->load->model('Contrato','',TRUE);
		$numero =  $this->session->userdata('ncontrato');
		$domicilio = $this->Contrato->obtener_domicilio_contrato( $numero );
		$nombrecontribuyente = $this->Contrato->obtener_nombre_contribuyente( $numero );
		$this->session->set_userdata(
			array('nombrecompleto'=>$nombrecontribuyente, 'domicilio'=>$domicilio,'numero'=>$numero)
			);
		$datos['items'] = $this->Item->obtener_items($this->session->userdata('nuevocontrato_folio'));
		$datos['subtotal'] = $this->Item->obtener_subtotal($this->session->userdata('nuevocontrato_folio'));
		$datos['descuento'] = $this->Recibo->obtener_descuento($this->session->userdata('nuevocontrato_folio'));
		$datos['folio_recibo'] = $this->session->userdata('folio_recibo');
		$this->load->helper('dompdf');

		$html = $this->load->view('recibo_cobro_automatico', $datos,TRUE);
		$this->session->set_userdata(array('folio' => 0));
		pdf_create($html, "recibo");
	}

	private function _contrato_existe($numero)
	{
		$this->load->model('Contrato','',TRUE);
		return $this->Contrato->contrato_existe($numero);
	}

	

	

	public function cobrar_contrato()
	{
		$recibo = $this->session->userdata('nuevocontrato_folio');
		$numero = $this->session->userdata('nuevocontrato_numero');
		
		$this->load->model('Contrato','',TRUE);
		$domicilio = $this->Contrato->obtener_domicilio_contrato($numero);
		$nombrecontribuyente = $this->Contrato->obtener_nombre_contribuyente($numero);
		$this->load->model('Item','',TRUE);
		$items = $this->Item->obtener_items($recibo);
		$datos = array('contrato'=>$numero,'nombrecompleto'=>$nombrecontribuyente,
			'domicilio'=>$domicilio,'items'=>$items);
		//echo "$recibo $numero $domicilio $nombrecontribuyente";
		$this->load->view('cobro_contrato',$datos);
	}

	public function cobrar()
	{
		$accion = $this->input->post('accion');
		if ($accion == 'pagar') {
			$this->_cerrar_recibo_contrato();
		} else {
			# code...
		}
		
	}

}
?>