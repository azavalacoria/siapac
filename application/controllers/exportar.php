<?php
/**
* 
*/
class Exportar extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
		$this->load->model('Sincronizacion','', TRUE);
		$periodoinicial = $this->Sincronizacion->obtener_ultima_sincronizacion();
		if ($periodoinicial == '0000-00-00') {
			$periodofinal = date('Y-m-d');
			
			$datos['contribuyentes'] = $this->_obtener_contribuyentes($periodoinicial , $periodofinal);
			$this->Sincronizacion->insertar($periodoinicial, $periodofinal, $datos['contribuyentes']);
			header('Content-type: application/xml');
			$nombre = "Sincronizacion_$periodoinicial.-.$periodofinal";
			header('Content-Disposition: attachment; filename="'.$nombre.'.xml"');
			$this->load->view('feed', $datos);
		} else {
			$datos['periodoinicial'] = $periodoinicial;
			$menu = $this->session->userdata('menu');
			$this->load->view($menu);
			$this->load->view('exportacion',$datos);
		}
	}

	public function manual()
	{
		$this->form_validation->set_rules('periodoinicial','fecha inicial','trim|required');
		$this->form_validation->set_rules('periodofinal','fecha final','trim|required');
		if ($this->form_validation->run() ){
			$periodoinicial = $this->input->post('periodoinicial');
			$periodofinal = $this->input->post('periodofinal');
			$this->load->model('Sincronizacion','',TRUE);
			//$this->Sincronizacion->insertar($periodoinicial, $periodofinal);
			$datos['contribuyentes'] = $this->_obtener_contribuyentes_manual($periodoinicial , $periodofinal);
			$archivo = $this->load->view('feed', $datos, TRUE);
			$this->Sincronizacion->insertar($periodoinicial, $periodofinal, $archivo);
			header('Content-type: application/xml');
			$nombre = "Sincronizacion_$periodoinicial.-.$periodofinal";
			header('Content-Disposition: attachment; filename="'.$nombre.'.xml"');
			$this->load->view('feed', $datos);
		} else {
			$menu = $this->session->userdata('menu');
			$this->load->view($menu);
			$this->load->view('exportacion');
		}
	}

	public function exportardos()
	{
		$datos = $this->_obtener_contribuyentes('2013-07-20', '2013-07-31');
		foreach ($datos as $d) {
			print_r($d);
			echo "<br>";
		}
		//echo json_encode($datos);
	}

	private function _obtener_contribuyentes($periodoinicial , $periodofinal)
	{
		$this->load->model('Sincronizador','',TRUE);
		$contribuyentes = array();
		$datos = $this->Sincronizador->listar_ultimos_contribuyentes($periodoinicial , $periodofinal);
		foreach ($datos as $c) {
			$contribuyente = array( 
					'claveelector' => $this->_validar_nulo($c->claveelector),
					'nombres' => $this->_validar_nulo($c->nombres), 
					'apellidopaterno' => $this->_validar_nulo($c->apellidopaterno), 
					'apellidomaterno' => $this->_validar_nulo($c->apellidomaterno), 
					'fechanacimiento'=> $this->_validar_nulo($c->fechanacimiento), 
					'localidad' => $this->_validar_nulo($c->loc),
					'colonia' => $this->_validar_nulo($c->colonia), 
					'calle' => $this->_validar_nulo($c->calle), 
					'numero'=> $this->_validar_nulo($c->numero), 
					'telefono' => $this->_validar_nulo($c->telefono), 
					'celular'=> $this->_validar_nulo($c->celular), 
					'email' => $this->_validar_nulo($c->email), 
					'rfc' => $this->_validar_nulo($c->rfc),
					'contratos' => $this->_obtener_contratos($c->idcontribuyente, $periodoinicial, $periodofinal)
				);
			array_push($contribuyentes, $contribuyente);
		}
		return $contribuyentes;
	}

	private function _obtener_contribuyentes_manual($periodoinicial, $periodofinal)
	{
		$this->load->model('Sincronizador','',TRUE);
		$contribuyentes = array();
		$datos = $this->Sincronizador->obtener_ultimos_contribuyentes($periodoinicial , $periodofinal);
		foreach ($datos as $idx => $id) {
			$datos = $this->Sincronizador->obtener_datos_contribuyente($id);
			foreach ($datos as $c) {
				$contribuyente = array(
					'claveelector' => $this->_validar_nulo($c->claveelector),
					'nombres' => $this->_validar_nulo($c->nombres), 
					'apellidopaterno' => $this->_validar_nulo($c->apellidopaterno), 
					'apellidomaterno' => $this->_validar_nulo($c->apellidomaterno), 
					'fechanacimiento'=> $this->_validar_nulo($c->fechanacimiento), 
					'localidad' => $this->_validar_nulo($c->loc),
					'colonia' => $this->_validar_nulo($c->colonia), 
					'calle' => $this->_validar_nulo($c->calle), 
					'numero'=> $this->_validar_nulo($c->numero), 
					'telefono' => $this->_validar_nulo($c->telefono), 
					'celular'=> $this->_validar_nulo($c->celular), 
					'email' => $this->_validar_nulo($c->email), 
					'rfc' => $this->_validar_nulo($c->rfc),
					'contratos' => $this->_obtener_ultimos_contratos($c->idcontribuyente, $periodoinicial, $periodofinal)
				);
				array_push($contribuyentes, $contribuyente);
			}
		}
		return $contribuyentes;
	}

	private function _obtener_contratos($idcontribuyente , $periodoinicial, $periodofinal)
	{
		$contratos = array();
		$this->load->model('Sincronizador','',TRUE);
		$datoscontratos = $this->Sincronizador->listar_contratos_contribuyente($idcontribuyente, $periodoinicial, $periodofinal);
		foreach ($datoscontratos as $c) {
			$contrato = array(
				'numero' => $this->_validar_nulo($c->numero), 
				'medidor'=> $this->_validar_nulo($c->medidor), 
				'loc' => $this->_validar_nulo($c->loc), 
				'colonia' => $this->_validar_nulo($c->colonia), 
				'calle' => $this->_validar_nulo($c->calle), 
				'numeroexterior' => $this->_validar_nulo($c->numeroexterior), 
				'numerointerior' => $this->_validar_nulo($c->numerointerior), 
				'referencias' => $this->_validar_nulo($c->referencias), 
				'codigopostal' => $this->_validar_nulo($c->codigopostal), 
				'fecha' => $this->_validar_nulo($c->fecha), 
				'observaciones' => $this->_validar_nulo($c->observaciones), 
				'latitud' => $this->_validar_nulo($c->latitud), 
				'longitud' => $this->_validar_nulo($c->longitud), 
				'ultimopago' => $this->_validar_nulo($c->ultimopago), 
				'zona' => $this->_validar_nulo($c->zona), 
				'tipodeservicio' => $this->_validar_nulo($c->tipodeservicio),
				'recibos' => $this->_obtener_recibos($c->idcontrato, $periodoinicial, $periodofinal)
				);
			array_push($contratos, $contrato);
		}
		return $contratos;
	}

	private function _obtener_ultimos_contratos($idcontribuyente , $periodoinicial, $periodofinal)
	{
		$contratos = array();
		$this->load->model('Sincronizador','',TRUE);
		$datoscontratos = $this->Sincronizador->listar_ultimos_contratos_activos($idcontribuyente, $periodoinicial, $periodofinal);
		foreach ($datoscontratos as $idcontrato) {
			$contrato = $this->Sincronizador->obtener_contrato($idcontrato);
			/*
			$contrato = array(
				'numero' => $this->_validar_nulo($c->numero), 
				'medidor'=> $this->_validar_nulo($c->medidor), 
				'loc' => $this->_validar_nulo($c->loc), 
				'colonia' => $this->_validar_nulo($c->colonia), 
				'calle' => $this->_validar_nulo($c->calle), 
				'numeroexterior' => $this->_validar_nulo($c->numeroexterior), 
				'numerointerior' => $this->_validar_nulo($c->numerointerior), 
				'referencias' => $this->_validar_nulo($c->referencias), 
				'codigopostal' => $this->_validar_nulo($c->codigopostal), 
				'fecha' => $this->_validar_nulo($c->fecha), 
				'observaciones' => $this->_validar_nulo($c->observaciones), 
				'latitud' => $this->_validar_nulo($c->latitud), 
				'longitud' => $this->_validar_nulo($c->longitud), 
				'ultimopago' => $this->_validar_nulo($c->ultimopago), 
				'zona' => $this->_validar_nulo($c->zona), 
				'tipodeservicio' => $this->_validar_nulo($c->tipodeservicio),
				'recibos' => $this->_obtener_recibos($c->idcontrato, $periodoinicial, $periodofinal)
				);
			*/
			$contrato['recibos'] = $this->_obtener_recibos($idcontrato, $periodoinicial, $periodofinal);
			array_push($contratos, $contrato);
		}
		return $contratos;
	}

	private function _obtener_recibos($idcontrato, $periodoinicial, $periodofinal) {
		$recibos = array();
		$this->load->model('Sincronizador','',TRUE);
		$datosrecibos = $this->Sincronizador->listar_recibos_contrato($idcontrato, $periodoinicial, $periodofinal);
		foreach ($datosrecibos as $r) {
			$folio = $r->folio;
			$recibo = array(
				'folio' => $this->_validar_nulo($folio),
				'fechacobro' => $this->_validar_nulo($r->fechacobro),
				'periodo' => $this->_validar_nulo($r->periodo),
				'subtotal' => $this->_validar_nulo($r->subtotal),
				'descuento' => $this->_validar_nulo($r->descuento),
				'total' => $this->_validar_nulo($r->total),
				'observaciones' => $this->_validar_nulo($r->observaciones),
				'modulo' => $this->_validar_nulo($r->modulo),
				'usuario' => $this->_validar_nulo($r->usuario), 
				'items' => $this->_obtener_items($r->idfolio)
				);
			array_push($recibos, $recibo);
			$this->Sincronizador->marcar_recibo($r->idfolio);
		}
		return $recibos;
	}

	private function _obtener_items($recibo)
	{
		$items = array();
		$this->load->model('Sincronizador','',TRUE);
		$datositems = $this->Sincronizador->listar_items_recibo($recibo);
		foreach ($datositems as $i) {
			$item = array(
				'periodo' => $this->_validar_nulo($i->periodo),
				'cantidad' => $this->_validar_nulo($i->cantidad),
				'concepto' => $this->_validar_nulo($i->concepto),
				'preciounitario' => $this->_validar_nulo($i->preciounitario),
				'importe' => $this->_validar_nulo($i->importe),
				'armonizacionuno' => $this->_validar_nulo($i->armonizacionuno),
				'armonizaciondos' => $this->_validar_nulo($i->armonizaciondos)
				);
			array_push($items, $item);
		}
		return $items;
	}

	private function _validar_nulo($valor)
	{
		if (!isset($valor) || $valor == '') {
			return "NULL";
		}
		else {
			return $valor;
		}
	}

	public function obtener()
	{
		$periodoinicial = '2013-06-01';
		$this->load->model('Sincronizador','', TRUE);
		$ultimosrecibos = $this->Sincronizador->obtener_ultimos_recibos($periodoinicial);
		$contribuyentes = array();
		foreach ($ultimosrecibos as $ultimosrecibo) {
			$contrato = $ultimosrecibo->contrato;
			$datos = $this->Sincronizador->obtener_contribuyente($contrato);
			foreach ($datos as $c) {
				$contribuyente = array( 
						'claveelector' => $this->_validar_nulo($c->claveelector),
						'nombres' => $this->_validar_nulo($c->nombres), 
						'apellidopaterno' => $this->_validar_nulo($c->apellidopaterno), 
						'apellidomaterno' => $this->_validar_nulo($c->apellidomaterno), 
						'fechanacimiento'=> $this->_validar_nulo($c->fechanacimiento), 
						'localidad' => $this->_validar_nulo($c->loc),
						'colonia' => $this->_validar_nulo($c->colonia), 
						'calle' => $this->_validar_nulo($c->calle), 
						'numero'=> $this->_validar_nulo($c->numero), 
						'telefono' => $this->_validar_nulo($c->telefono), 
						'celular'=> $this->_validar_nulo($c->celular), 
						'email' => $this->_validar_nulo($c->email), 
						'rfc' => $this->_validar_nulo($c->rfc),
						'contratos' => $this->_obtener_contratos($c->idcontribuyente, $periodoinicial)
					);
				array_push($contribuyentes, $contribuyente);
			}
		}
		$this->load->view('feed',array('contribuyentes'=>$contribuyentes));
	}
}
?>