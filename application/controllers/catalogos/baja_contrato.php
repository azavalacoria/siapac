<?php

class Baja_Contrato extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		//$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$contrato = $this->input->post('contrato');
		$motivo = $this->input->post('motivo');
		$this->_baja_contrato($contrato,$motivo);
	}

	public function _baja_contrato($contrato,$motivo)
	{
		if ($contrato){
			$datos = array(
				'fechabaja' => date_format(new DateTime(),'Y-m-d H:m:s'),
				'activo' => false,
				'motivobaja' => strtoupper($motivo)
			);
		$this->load->model('Contrato','',TRUE);
		$this->Contrato->baja_contrato($datos, $contrato);
		$data = array(
				'mensaje'=>'Fue dado de baja con éxito el contrato con el número '.$contrato,
				'destino' => site_url('home')
				);
		$this->load->view('vacio', $data);
		}else{
			$data = array(
				'mensaje'=>'Hubo un error en la aplicación',
				'destino' => site_url('home')
				);
		$this->load->view('vacio', $data);
		}
	}
}
?>