<?php
/**
* 
*/
class Agregar_Tarifa_Anual extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('zona','zona','required|is_natural_no_zero');
		$this->form_validation->set_rules('servicio','tipo de servicio','required|is_natural_no_zero');
		$this->form_validation->set_rules('fechainicial','fecha inicial','required');
		$this->form_validation->set_rules('fechafinal','fecha final','required');
		$this->form_validation->set_rules('precio','precio','required|decimal');

		if ($this->form_validation->run()) {
			$zona = $this->input->post('zona');
			$tiposervicio = $this->input->post('servicio');
			$iniciovigencia = $this->input->post('fechainicial');
			$finalvigencia = $this->input->post('fechafinal');
			$cobroalcontratar = $this->input->post('cobroalcontratar');
			if (!isset($cobroalcontratar)) {
				$cobroalcontratar = 0;
			}
			$existe = $this->_existe_tarifa($zona, $tiposervicio, $iniciovigencia, $finalvigencia, $cobroalcontratar);
			if ($existe == 0) {
				$data = array(
					'periodo'=>$periodo = $this->_obtener_periodo($iniciovigencia, $finalvigencia),
					'zona'=>$zona,
					'iniciovigencia' => $iniciovigencia,
					'finalvigencia' => $finalvigencia,
					'tiposervicio'=>$tiposervicio,
					'precio'=>$this->input->post('precio'),
					'cobroalcontratar' => $cobroalcontratar
				);
				$this->load->model('Tarifa','',TRUE);
				$this->Tarifa->agregar_tarifa($data);
				$datos['mensaje'] = "Tarifa agregada con éxito";
				$this->load->view('vacio',$datos);
			} else {
				$this->_cargar_vista("Ya ha sido declarada una tarifa similar");
			}
			
		} else {
			$this->_cargar_vista(null);
		}
	}

	private function _obtener_periodo($inicio , $fin)
	{
		$fechainicial = date("Ym", strtotime($inicio));
		$fechafinal = date("Ym", strtotime($fin));
		return "$fechainicial-$fechafinal";
	}

	private function _cargar_vista($mensaje)
	{
		$datos['error'] = $mensaje;
		$datos['zonas'] = $this->_obtener_zonas();
		$datos['servicios'] = $this->_obtener_servicios();
		$this->load->view('catalogos/agregar_tarifa_anual',$datos);
	}

	private function _existe_tarifa($zona, $tiposervicio, $iniciovigencia, $finalvigencia)
	{
		$this->load->model('Tarifa','',TRUE);
		return $this->Tarifa->existe_tarifa($zona, $tiposervicio, $iniciovigencia, $finalvigencia);
	}

	private function _obtener_zonas()
	{
		$this->load->model('Zona','',TRUE);
		$datos = $this->Zona->obtener_zonas();
		$zonas["0"] = "Seleccionar";
		foreach ($datos as $zona) {
			$zonas[$zona->idzona] = $zona->nombrezona;
		}
		return $zonas;
	}

	private function _obtener_servicios()
	{
		$this->load->model('Servicio','',TRUE);
		$servicios = $this->Servicio->listar_servicios_anuales();
		return $servicios;
	}
}
?>