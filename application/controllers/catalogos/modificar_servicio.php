<?php
/**
* 
*/
class Modificar_Servicio extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('id','servicio','required');
		if ($this->form_validation->run()) {
			$this->_modificar_servicio($this->input->post('id'));
		} else {
			$this->_cargar_vista_seleccion();
		}
	}

	public function actualizar()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('session') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('nombre','nombre del servicio','required');
		$this->form_validation->set_rules('descripcion','descripcion','required');
		if ($this->form_validation->run()) {
			$cobromensual = $this->input->post('cobromensual');
			if ($cobromensual != 1) {
				$cobromensual = 0;
			}
			$datos = array('nombreservicio' => $this->input->post('nombre'),
				'descripcion' => $this->input->post('descripcion'),
				'cobromensual' => $cobromensual
				);
			$this->load->model('Servicio','',TRUE);
			$this->Servicio->actualizar_servicio($datos, $this->session->userdata('idservicio'));
		} else {
			$this->load->view('catalogos/modificar_servicio_recargado');
		}
	}

	private function _cargar_vista_seleccion()
	{
		$this->load->model('Servicio','',TRUE);
		$datos['destino'] = 'catalogos/modificar_servicio';
		$datos['servicios'] = $this->Servicio->obtener_servicios();
		$this->load->view('catalogos/listar_servicios', $datos);
	}

	private function _modificar_servicio($servicio)
	{
		$this->session->set_userdata(array('idservicio'=>$servicio));
		$this->load->model('Servicio','',TRUE);
		$datos['servicio'] = $this->Servicio->obtener_servicio($servicio);
		$this->load->view('catalogos/modificar_servicio', $datos);
	}
}
?>