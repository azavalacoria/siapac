<?php
/**
* 
*/
class Agregar_Tipo_Servicio extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('nombre','nombre del servicio','required');
		$this->form_validation->set_rules('descripcion','descripción del servicio','required');
		if ($this->form_validation->run()) {
			$existe = $this->_existe_servicio($this->input->post('nombre'));
			if($existe == 0) {
				$cobromensual = $this->input->post('cobromensual');
				if (!isset($cobromensual)) {
					$cobromensual = 0;
				}
				$this->load->model('Servicio','',TRUE);
				$data = array(
					'nombreservicio' => $this->input->post('nombre'),
					'descripcion' => $this->input->post('descripcion'),
					'cobromensual' => $cobromensual
					);
				$this->Servicio->agregar_servicio($data);
				$datos['mensaje'] = "Nuevo servicio agregado con éxito";
				$this->load->view('vacio', $datos);
			} else {
				$datos['error'] = "Ya se ha declarado un servicio con ese nombre.";
				$this->load->view('agregar_tipo_servicio',$datos);
			}
		} else {
			$this->load->view('catalogos/agregar_tipo_servicio');
		}
	}

	private function _existe_servicio($nombre)
	{
		$this->load->model('Servicio','',TRUE);
		return $this->Servicio->existe_servicio($nombre);
	}
}
?>