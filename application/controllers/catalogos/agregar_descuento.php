<?php
/**
* 
*/
class Agregar_Descuento extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}

		$this->form_validation->set_rules('descripcion', 'descripcion', 'trim|required');
		$this->form_validation->set_rules('porcentaje', 'porcentaje', 'trim|required|integer|greater_than[0]|less_than[101]');
		$this->form_validation->set_rules('evidencia', 'evidencia', 'trim');
		if ($this->form_validation->run()) {
			$activo = $this->input->post('activo');
			$total = $this->input->post('total');
			echo "-$activo- <br>";
			echo "-$total-<br>";
			
			$evidencia = $this->input->post('evidencia');
			$porcentaje = 0.01 * $this->input->post('porcentaje');
			if (isset($activo) && $activo == '1') {
				$activo = 1;
			} else {
				$activo = 0;
			}
			if (isset($total) && $total == '1') {
				$total = 1;
			} else {
				$total = 0;
			}
			if (!isset($evidencia) || $evidencia == '') {
				$evidencia = null;
			}
			
			$datos = array(
				'descripcion' => $this->input->post('descripcion'),
				'porcentaje' => $porcentaje,
				'evidencia' => $evidencia,
				'activo' => $activo,
				'aplicaratotal' => $total
				);
			$this->_agregar_descuento($datos);
		} else {
			$this->load->view('catalogos/agregar_descuento');
		}
	}

	private function _agregar_descuento($datos)
	{
		$this->load->model('Bonificacion','',TRUE);
		$this->Bonificacion->agregar_bonificacion($datos);
		$result = array(
			'mensaje'=>'Bonificación agregada con éxito.' ,
			'destino'=>site_url()
			);
		$this->load->view('vacio', $result);
	}
}
?>