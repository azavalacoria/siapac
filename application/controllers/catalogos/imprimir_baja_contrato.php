<?php
/**
* 
*/
class Imprimir_Baja_Contrato extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$numero = $this->input->post('numero');
		if (strlen($numero) > 0) {
			$this->_imprimir($numero);
		} else {
			echo "nanche ";
			echo strlen("");
		}
	}

	private function _imprimir($numero)
	{
		$this->load->model('Contrato','',TRUE);
		$result = $this->Contrato->obtener_datos_impresion_baja($numero);
		$datos = array();
		$c = 0;
		foreach ($result as $contrato) {
			$c++;
			$direccion = "";
			if ($contrato->numerointerior == "0") {
				$direccion = "Calle ".$contrato->calle." ".$contrato->numeroexterior." ".$contrato->nombrecolonia;
			} else {
				$direccion = "Calle ".$contrato->calle." ".$contrato->numeroexterior." ".$contrato->numerointerior." ".$contrato->nombrecolonia;
			}
			$nombrecompleto = 
			$datos['numero'] = strtoupper($contrato->numero);
			$datos['direccion'] = strtoupper($direccion);
			$datos['servicio'] = $contrato->nombreservicio;
			$datos['fechabaja'] = $contrato->fechabaja;
			$datos['motivobaja'] = $contrato->motivobaja;
			$datos['nombrecompleto'] = "".$contrato->nombres." ".$contrato->apellidopaterno." ".$contrato->apellidomaterno;
		}
		//print_r ($datos);
		
		$html = $this->load->view('plantilla_contrato_baja',$datos, TRUE);
		$this->load->helper('dompdf');
		pdf_create($html, "contrato_baja.pdf");
	}
}

/*
contratos.numero, contratos.calle, contratos.numeroexterior, contratos.numerointerior, 
			nombrecolonia, nombreservicio, nombres, apellidopaterno, apellidomaterno"
*/
?>