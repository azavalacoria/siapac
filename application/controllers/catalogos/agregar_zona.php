<?php
/**
* 
*/
class Agregar_Zona extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('nombre', 'nombre de la zona', 'required');
		if ($this->form_validation->run()) {
			$existe = $this->_existe_zona($this->input->post('nombre'));
			if ($existe == 0) {
				$this->load->model('Zona', '', TRUE);
				$zona = array('nombrezona'=>$this->input->post('nombre'));
				$this->Zona->agregar_zona($zona);
				$datos['mensaje'] = "Zona agregada con éxito";
				$this->load->view('vacio', $datos);
			} else {
				$datos['error'] = "Ya existe una zona con el mismo nombre.";
				$this->load->view('agregar_zona', $datos);
			}
		} else {
			$this->load->view('catalogos/agregar_zona');
		}
	}

	private function _existe_zona($nombre)
	{
		$this->load->model('Zona','',TRUE);
		return $this->Zona->existe_zona($nombre);
	}
}
?>