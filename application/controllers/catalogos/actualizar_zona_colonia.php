<?php
/**
* 
*/
class Actualizar_Zona_Colonia extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->_cargar_vista();
	}

	private function _cargar_vista()
	{
		$this->load->model('Zona','',TRUE);
		$datos['zonas'] = $this->Zona->obtener_zonas();
		$this->load->view('catalogos/actualizar_zona_colonias', $datos);
	}
}
?>