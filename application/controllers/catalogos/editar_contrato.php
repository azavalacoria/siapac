<?php
/**
* 
*/
class Editar_Contrato extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('numero','numero de contrato','trim|required');
		$this->form_validation->set_rules('medidor','medidor','trim|required');
		$this->form_validation->set_rules('tiposervicio','tipo de servicio','trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('zona','zona','trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('colonia','colonia','trim|required|is_natural_no_zero');
		$this->form_validation->set_rules('localidad','localidad','trim|required|max_length[4]');
		$this->form_validation->set_rules('calle','calle','trim|required');
		$this->form_validation->set_rules('numeroexterior','numero exterior','trim|required');
		$this->form_validation->set_rules('numerointerior','numero interior','trim|required');
		$this->form_validation->set_rules('codigopostal','codigo postal','trim|required|max_length[5]');
		//$this->form_validation->set_rules('ultimopago','último pago','trim|required');
		$this->form_validation->set_rules('referencias','referencias','trim');
		if ($this->form_validation->run()) {
			
			if (!isset($referencias)) {
					$referencias = null;
				}
			// 'ultimopago' => $this->input->post('ultimopago'),
			$datos = array(
				'medidor' => $this->input->post('medidor'),
				'loc' => $this->input->post('localidad'),
				'colonia' => $this->input->post('colonia'),
				'calle' => $this->input->post('calle'),
				'numeroexterior' => $this->input->post('numeroexterior'),
				'numerointerior' => $this->input->post('numerointerior'),
				'referencias' => $referencias,
				'codigopostal' => $this->input->post('codigopostal'),
				'fecha' => date("Y-m-d"),
				'observaciones' => $this->input->post('observaciones'),
				
				'zona' => $this->input->post('zona'),
				'tipodeservicio'=> $this->input->post('tiposervicio')
			);
			$idcontrato = $this->input->post('idcontrato');
			$this->load->model('Contrato','',TRUE);
			$this->Contrato->actualizar_contrato($datos, $idcontrato);
			$data = array(
				'mensaje'=>'Contrato actualizado con éxito',
				'destino' => site_url('home')
				);
			$this->load->view('vacio', $data);
		} else {
			$this->_cargar_vista_nuevo_contrato("Verifique los siguientes datos:");
		}
	}

	private function _cargar_vista_nuevo_contrato($mensaje)
	{
		$datos['mensaje'] = $mensaje;
		$this->load->model('Servicio','',TRUE);
		$datos['servicios'] = $this->Servicio->listar_servicios();
		$this->load->model('Zona','',TRUE);
		$datos['zonas'] = $this->Zona->listar_zonas();
		$zona = $this->input->post('zona');
		if ($zona > 0) {
			$this->load->model('Colonia','',TRUE);
			$datos['colonias'] = $this->Colonia->enlistar_colonias($zona);
		} else {
			$datos['colonias'] = array('0'=>'Seleccione');
		}
		$this->load->view('catalogos/editar_contrato_recargado', $datos);
	}
}
?>