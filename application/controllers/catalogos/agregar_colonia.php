<?php
/**
* 
*/
class Agregar_Colonia extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('rol') != 1 || $this->session->userdata('sesion') != 1) {
			redirect('home');
		}
		$this->form_validation->set_rules('nombre','nombre','required');
		$this->form_validation->set_rules('cp','código postal','required|exact_length[5]');
		$this->form_validation->set_rules('zona','zona','is_natural_no_zero|required');
		$this->form_validation->set_rules('localidad','localidad','integer|required|min_length[3]');
		if ($this->form_validation->run()) {
			$existe = $this->_existe_colonia($this->input->post('nombre'), $this->input->post('zona'));
			if ($existe == 0) {
				$data = array( 'nombrecolonia' => $this->input->post('nombre'),
					'zona' => $this->input->post('zona'),
					'municipiolocalidad' => $this->input->post('localidad'),
					'codigopostal' => $this->input->post('cp')
					);
				$this->load->model('Colonia','',TRUE);
				$this->Colonia->agregar_colonia($data);
				$datos['mensaje'] = "Colonia agregada con éxito";
				$this->load->view('vacio', $datos);
			} else {
				$this->_cargar_vista("Error, ya existe una colonia con esos datos");
			}
		} else {
			$this->_cargar_vista(null);
		}
	}

	private function _cargar_vista($mensaje)
	{
		$datos['error'] = $mensaje;
		$datos['zonas'] = $this->_obtener_zonas();
		$this->load->view('catalogos/agregar_colonia',$datos);
	}

	private function _obtener_zonas()
	{
		$this->load->model('Zona','',TRUE);
		$datos = $this->Zona->obtener_zonas();
		$zonas['0'] = "Seleccionar:";
		foreach ($datos as $zona) {
			$zonas[$zona->idzona] = $zona->nombrezona;
		}
		return $zonas;
	}

	private function _existe_colonia($nombre, $zona)
	{
		$this->load->model('Colonia','',TRUE);
		return $this->Colonia->existe_colonia($nombre, $zona);
	}
}
?>