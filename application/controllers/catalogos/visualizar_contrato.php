<?php
/**
* 
*/
class Visualizar_Contrato extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function buscar()
	{
		$this->form_validation->set_rules('apellidopaterno','apellido paterno','trim|required|min_length[2]');
		$this->form_validation->set_rules('apellidomaterno','apellido materno','trim');
		$this->form_validation->set_rules('nombres','nombres','trim');
		if ($this->form_validation->run()) {
			$this->_busqueda_inicial(
				$this->input->post('apellidopaterno'),
				$this->input->post('apellidomaterno'),
				$this->input->post('nombres')
				);
		} else {
			$datos['destino'] = 'catalogos/visualizar_contrato/buscar';
			$this->load->view('catalogos/buscar_contribuyente',$datos);
		}
	}

	public function sig_resultados()
	{
		$opcion = $this->input->post('opcion');
		switch ($opcion) {
			case 'sig':
				$this->_siguientes_resultados();
				break;
			
			default:
				$this->load->view('vacio',array('mensaje'=>'No se hallaron resultados'));
				break;
		}
	}

	public function visualizar()
	{
		$this->form_validation->set_rules('contrato','contrato','required');
		if ($this->form_validation->run()) {
			$contrato = $this->input->post('contrato');
			$this->load->model('Contrato','',TRUE);
			$datos['contrato'] = $this->Contrato->obtener_datos_contrato($contrato);
			$this->load->view('catalogos/visualizar_contrato', $datos);
		} else {
			$this->_buscar();
		}
	}

	private function _busqueda_inicial($apellidopaterno, $apellidomaterno, $nombres)
	{
		$this->session->set_userdata( array(
			'apellidopaterno' => $apellidopaterno,
			'apellidomaterno' => $apellidomaterno,
			'nombres' => $nombres,
			'inicio'=>0,
			'limite'=>30)
			);
		$this->_buscar();
	}

	private function _siguientes_resultados()
	{
		$inicio = $this->session->userdata('inicio');
		if ($inicio >= 0) {
			$inicio = $inicio + 30;
			$this->session->set_userdata( array('inicio' => $inicio) );
		}
		$this->_buscar();
	}

	private function _buscar()
	{
		$this->load->model('Contrato','',TRUE);
		$query = $this->Contrato->buscar_por_nombre(
			$this->session->userdata('apellidopaterno'),
			$this->session->userdata('apellidomaterno'),
			$this->session->userdata('nombres'),
			$this->session->userdata('inicio'), 30
			);
		if ($query->num_rows() == 0) {
			$datos = array(
				'mensaje' => 'No se encontró a ese contribuyente. ',
				'etiqueta' => '¿Desea agregarlo?',
				'destino' => site_url('nuevo_contribuyente'));
			$this->load->view('vacio', $datos);
		} else{
			$datos['contratos'] = $query->result();
			$datos['destino'] = 'catalogos/visualizar_contrato/visualizar';
			$datos['busqueda_nueva'] = 'catalogos/visualizar_contrato/sig_resultados';
			$this->load->view('listar_contratos',$datos);
		}
	}

/*------------------------------------------INICIO BAJAS CONTRATOS------------------------------------------*/
	public function contratos_bajas()
	{
		$this->session->set_userdata( array(
			'inicio'=>0,
			'limite'=>30)
			);
		$this->listar_contratos_bajas();
	}

	private function listar_contratos_bajas()
	{
		$this->load->model('Contrato','',TRUE);
		$query = $datos['contrato'] = $this->Contrato->obtener_contratos_bajas(
			$this->session->userdata('inicio'), 30
			);
		if ($query->num_rows() == 0) {
			$this->load->view('vacio',array('mensaje'=>'Actualmente no hay contratos dados de baja.'));
		} else{
			$datos['contratos'] = $query->result();
			$datos['anteriores'] = 'catalogos/visualizar_contrato/anteriores_resultados_bajas';
			$datos['busqueda_nueva'] = 'catalogos/visualizar_contrato/siguientes_resultados_bajas';
			$this->load->view('listar_contratos_bajas',$datos);
		}
	}

		public function anteriores_resultados_bajas()
	{
		$inicio = $this->session->userdata('inicio');
		if ($inicio >= 0) {
			$inicio = $inicio - 30;
			$this->session->set_userdata( array('inicio' => $inicio) );
		}
		$this->listar_contratos_bajas();
	}

	public function siguientes_resultados_bajas()
	{
		$inicio = $this->session->userdata('inicio');
		if ($inicio >= 0) {
			$inicio = $inicio + 30;
			$this->session->set_userdata( array('inicio' => $inicio) );
		}
		$this->listar_contratos_bajas();
	}
/*------------------------------------------FIN BAJAS CONTRATOS------------------------------------------*/


}
?>