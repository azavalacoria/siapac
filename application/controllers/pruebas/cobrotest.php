<?php

/**
* 
*/
class CobroTest extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		setlocale(LC_ALL, null);
		$ultimopago = '2012-01-01'; //$this->session->userdata('ultimopago');
		$nuevoultimopago = '2013-01-01'; //$this->session->userdata('nuevoultimopago');

		$periodoinicial = strftime('%Y%b', strtotime($ultimopago));
		$periodofinal = strftime('%Y%b', strtotime($nuevoultimopago));
		echo strtoupper($periodoinicial).'-'.strtoupper($periodofinal);
	}
}
?>