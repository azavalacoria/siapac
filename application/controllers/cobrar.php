<?php
/**
* 
*/
class Cobrar extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$menu = $this->session->userdata('menu');
		$this->load->view($menu);
	}

	public function index()
	{
		if ($this->session->userdata('sesion') != 1) {
			redirect('login');
		}
		$elegir = $this->input->post('elegir');
		$contrato = $this->input->post('contrato');
		if (!empty($contrato)) {
			if ($elegir == 'on') {
				$this->_cobro($contrato);
			}
		} else {
			redirect('buscar_contrato');
		}
	}

	public function elegir_cobro()
	{
		$tipo = $this->input->post('tipocobro');

		switch ($tipo) {
			case 'parcial':
				$this->_cobro_manual();
				break;
			case 'presupuesto':
				$this->_crear_presupuesto();
				break;
			case 'auto':
				$this->_cobro_automatico();
				break;
			default:
				$this->load->view('elegir_pago');
				break;
		}
	}

	private function _cobro($numero)
	{
		$this->load->model('Contrato','',TRUE);
		$datos = $this->Contrato->obtener_contrato($numero);
		foreach ($datos as $d) {
			$domicilio = '';
			if ($d->numerointerior == '0') {
				$domicilio = ($d->calle." ".$d->numeroexterior.", ".$d->nombrecolonia);
			} else {
				$domicilio = ($d->calle." ".$d->numeroexterior.", ".$d->numerointerior." ".$d->nombrecolonia);
			}
			
			$contrato = array( 'contrato'=>$d->idcontrato, 'numero'=>$d->numero,
				'nombrecompleto'=>($d->nombres." ".$d->apellidopaterno." ".$d->apellidomaterno),
				'domicilio'=> $domicilio, 'ultimopago'=> $d->ultimopago, 'zona'=>$d->zona,
				'tipodeservicio'=>$d->tipodeservicio);
		}
		$this->session->set_userdata($contrato);
		
		$this->load->view('elegir_pago');
	}

	private function _cobro_automatico()
	{
		redirect('cobro_automatico/calcular');
	}

	private function _cobro_manual()
	{
		$datos = array('folio'=> $this->_nuevo_recibo($this->session->userdata('contrato')));
		$this->session->set_userdata($datos);
		$ultimopago = $this->session->userdata('ultimopago');
		$periodoinicial =  strftime("%Y-%m-%d", strtotime('first day of next month', strtotime($ultimopago)));
		$this->session->set_userdata(array('periodoinicial'=>$periodoinicial));
		redirect('cobro_parcial');
	}

	private function _nuevo_recibo($contrato)
	{
		$this->load->model('Recibo','',TRUE);
		$idrecibo = $this->Recibo->crear_recibo($contrato, 1 , 1);
		return $idrecibo;
	}

	private function _crear_presupuesto()
	{
		$datos['numero'] = $this->session->userdata('numero');
		$datos['periodoinicial'] =  strftime(
			"%Y-%m-%d", strtotime('first day of next month', strtotime($this->session->userdata('ultimopago'))));
		$datos['ultimopago'] = $this->session->userdata('ultimopago');
		$datos['nombrecompleto'] = $this->session->userdata('nombrecompleto');
		$datos['domicilio'] = $this->session->userdata('domicilio');
		$this->load->view('contratos/crear_presupuesto', $datos);
	}
}
?>