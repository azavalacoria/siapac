<?php
/**
* 
*/
class Busquedas_Ajax extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//echo json_encode(array('hola'=>'hola'));
	}

	public function obtener_ciudadanos()
	{
		$apellidopaterno = strtoupper($this->input->post('apellidopaterno'));
		$apellidomaterno = strtoupper($this->input->post('apellidomaterno'));
		$nombres = strtoupper($this->input->post('nombres'));
		$inicio = $this->input->post('inicio');
		$limite = 0;
		if ($inicio >= 30) {
			$inicio = $this->session->userdata('cont_busqueda_ciudadano');
			$limite = $inicio + 30;
			$this->session->set_userdata(array('cont_busqueda_ciudadano'=>$limite));
		} else {
			$inicio = 0;
			$limite = 30;
			$this->session->set_userdata(array('cont_busqueda_ciudadano'=>$limite));
		}
		$this->load->model('Ciudadano','',TRUE);
		$resultados = $this->Ciudadano->buscar_ciudadano($apellidopaterno, $apellidomaterno, $nombres, $inicio, 30);
		$datos['encontrados'] = $resultados->num_rows();
		$datos['desde'] = $inicio;
		$datos['hasta'] = $limite;
		$datos['ciudadanos'] = $this->_enlista_ciudadanos($resultados->result());
		//$this->load->view('listar_ciudadanos', $datos);
		echo json_encode($datos);
	}

	private function _enlista_ciudadanos($datos)
	{
		$ciudadanos = array();
		foreach ($datos as $d) {
			$ciudadano = array(
				'id' => $d->clave,
				'apellidopaterno' => $d->apellidopaterno,
				'apellidomaterno' => $d->apellidomaterno,
				'nombres' => $d->nombre,
				'edad' => $d->edad,
				'colonia' => $d->colonia,
				'calle' => $d->calle,
				'numero' => $d->numero
				);
			array_push($ciudadanos, $ciudadano);
		}
		return $ciudadanos;
	}

}
?>