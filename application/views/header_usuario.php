<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
	<title>SIAPAC</title>
	<link rel="SHORTCUT ICON" href="<?php echo base_url('favicon.ico'); ?>">
	<?php echo link_tag('css/style.css'); ?>
	<?php echo link_tag('css/menu.css'); ?>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/hb-smooth-menu.js"></script>
	<script type="text/javascript">
	ddsmoothmenu.init({
 		mainmenuid: "smoothmenu1", 
 		orientation: 'h', 
 		classname: 'ddsmoothmenu', 
 		contentsource: "markup" 
	});
	</script>
</head>
<body>
	<div id="screen">
		<div id="header">
			<div id="logo">
				<div id="img-logo"></div>
			</div>
		</div>
		<div id="content">
		<div id="bar">
			<div id="smoothmenu1" class="ddsmoothmenu">
				<ul>
					<li>
						<a href="<?php echo site_url('home'); ?>">
							Inicio
						</a>
			        </li>

			    <!--

			    -->
			    	<!--
			        <li>
			            <a href="#">Contribuyentes</a>
			            <ul>
			                <li>
			                	<a href="<?php //echo site_url('nuevo_contribuyente'); ?>">
			                		Nuevo contribuyente
			                	</a>
			                </li>
			                
			                <li>
			                	<a href="#">
			                		Listar contribuyentes
			                	</a>
							</li>
							
			            </ul>
			        </li>


			        <li>
			            <a href="#">Contratos</a>
			            <ul>
			                <li>
			                    <a href="<?php //echo site_url('nuevo_contrato/buscar_contribuyente'); ?>">
			                    	Nuevo contrato
			                    </a>
			                </li>
			                <li>
			                    <a href="<?php //echo site_url('catalogos/visualizar_contrato/buscar') ?>">
			                    	Buscar contratos
			                    </a>
			                </li>
			            </ul>
			        </li>

			        -->
			        <li>
			            <a href="#">Cobros</a>
			            <ul>
			                <li>
			                    <a href="<?php echo site_url('buscar_contrato'); ?>">
			                    	Realizar cobro
			                    </a>
			                </li>
			                <!--<li>
			                	<a href="<?php //echo site_url('buscar_contrato'); ?>">
			                		Realizar cobro parcial
			                	</a>
			                </li>-->
			            </ul>
			        </li>
			        
			        <li>
			        	<a href="#">Sincronizar</a>
			        	<ul>
			        		<li>
			        			<a href="<?php echo site_url('exportar'); ?>">
			        				Exportar datos</a>
			        		</li>
			        		<li>
			        			<a href="<?php echo site_url('import') ?>">
			        				Importar datos
			        			</a>
			        		</li>
			        	</ul>
			        </li>
			        <li>
			        	<a href="#">Reportes</a>
			        	<ul>
			        		<li>
			        			<a href="<?php echo site_url('reporte/corte_diario'); ?>">
			        				Corte diario</a>
			        		</li>
			        	</ul>
			        </li>
					<!--
			        <li>
			            <a href="#">Catálogos</a>
			            <ul>
			                <li>
			                    <a href="#">Tipos de servicio</a>
			                    <ul>
				                    <li>
				                    	<a href="<?php //echo site_url('catalogos/agregar_tipo_servicio'); ?>">
				                    		Agregar tipo de servicio
				                    	</a>
				                    </li>
				                    <li>
				                    	<a href="<?php //echo site_url('catalogos/modificar_servicio') ?>">
				                    		Modificar tipo de servicio
				                    	</a>
				                    </li>
			                    </ul>
			                </li>
			                <li>
			                    <a href="#">Zonas</a>
			                    <ul>
				                    <li>
					                    <a href="<?php //echo site_url('catalogos/agregar_zona'); ?>">
					                    	Agregar zona
					                    </a>
					                </li>
				                    <li>
				                    	<a href="<?php //echo site_url('catalogos/actualizar_zona_colonia'); ?>">Modificar zona</a>
				                    </li>
			                    </ul>
			                </li>
			                <li>
			                    <a href="#">Colonias</a>
			                    <ul>
			                    	<li>
					                    <a href="<?php //echo site_url('catalogos/agregar_colonia'); ?>">
					                    	Agregar colonia
					                    </a>
					                </li>
				                    <li>
					                    <a href="<?php //echo site_url(); ?>">
					                    	Modificar colonia
					                    </a>
					                </li>
					            </ul>
			                </li>
			                <li>
			                    <a href="#">Tarifas</a>
			                    <ul>
				                    <li>
										<a href="<?php //echo site_url('catalogos/agregar_tarifa'); ?>">
											Agregar tarifa mensual
										</a>
									</li>
									<li>
										<a href="<?php //echo site_url('catalogos/agregar_tarifa_anual'); ?>">
											Agregar tarifa de contrato
										</a>
									</li>
				                    <li>
				                    	<a href="#">Modificar tarifa</a>
				                    </li>
			                    </ul>
			                </li>
			            </ul>
			        </li>

			    -->
			        <li>
			            <a href="<?php echo site_url('home/salir'); ?>">
			            	Salir
			            </a>
			        </li>
				</ul>
			</div>
		</div>
		<div id="main">