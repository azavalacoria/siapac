<div id="form-box">
	<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
	<?php echo form_open('cobro_parcial/agregar_descuento'); ?>
	<table>
		<tr>
			<th>Opción</th>
			<th>Concepto</th>
			<th>Porcentaje de descuento</th>
			<th>Evidencia</th>
		</tr>
	<?php foreach ($descs as $b) { ?>
		<tr>
			<td>
				<?php echo form_radio(
					array('name'=>'descuento','value'=>$b->iddeduccion,'group'=>'bonificacion')
					); ?>
			</td>
			<td><?php echo $b->descripcion; ?></td>
			<td><?php echo ($b->porcentaje / 0.01)."%"; ?></td>
			<td><?php echo $b->evidencia; ?></td>
		</tr>
	<?php } ?>
	</table>
	<!--
	<div class="form-row">
		<div class="normal-row">
			<label77>Porcentaje Descuento</label>
			<?php //echo form_input(array('name'=>'descuento', 'value'=>set_value('descuento'))); ?>
		</div>
		
	</div>
	-->
	<div>
		<br>
		<?php echo form_submit(array('name'=>'agregar','value'=>'Agregar','class'=>'submit')); ?>
	</div>
	<?php echo form_close(); ?>
</div>