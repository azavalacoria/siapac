<div style="margin-left: 40px">
	<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
</div>
<div id="report-box">
	<h4>
		Mostrando <?php echo $encontrados; ?> resultado(s).
	</h4>
	<table>
		<thead>
			<th style="border-left: 0">Opción</th>
			<th>Apellido Paterno</th>
			<th>Apellido Materno</th>
			<th>Nombres</th>
			<th>Edad</th>
			<th>Colonia</th>
			<th>Calle</th>
			<th>Número</th>
		</thead>
		<tbody>
<?php echo form_open('nuevo_contribuyente/seleccionar'); ?>
			<?php foreach ($ciudadanos as $c) { ?>
				<tr>
					<td><?php echo form_radio(array('name'=>'clave','value'=>$c->clave,'group'=>'ciudadanos')); ?></td>
					<td><?php echo $c->apellidopaterno; ?></td>
					<td><?php echo $c->apellidomaterno; ?></td>
					<td><?php echo $c->nombre; ?></td>
					<td><?php echo $c->edad; ?></td>
					<td><?php echo $c->colonia; ?></td>
					<td><?php echo $c->calle; ?></td>
					<td><?php echo $c->numero; ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?php if ($encontrados != 0) {?>
		<div style="margin-top: 25px">
			<div style="text-align: center; float: left">
				<?php echo form_submit(array('name'=>'seleccionar','value'=>'Seleccionar','id'=>'submit')); ?>
				<?php echo form_close(); ?>
			</div>
			<div style="float: right">
				<?php echo form_open('nuevo_contribuyente/limpiar')?>
				<?php echo form_submit(array('name'=>'reiniciar','value'=>'Reiniciar','id'=>'submit')) ?>
				<?php echo form_close(); ?>
			</div>
			<div style="padding-right: 40px; text-align: left; float: right">
			<?php echo form_open('nuevo_contribuyente/buscar'); ?>
			<?php if ($this->session->userdata('inicio') > 30 ): ?>
				<?php echo form_button(array('name'=>'opcion', 'value'=>'0', 'type'=>'submit','content'=>'Anteriores','id'=>'submit')) ?>
			<?php endif ?>
			<?php echo form_button(array('name'=>'opcion', 'value'=>'1', 'type'=>'submit','content'=>'Siguientes','id'=>'submit')) ?>
			<?php echo form_close(); ?>
			</div>
		</div>
	<?php } else { ?>
		<div style="margin-top: 25px">
			<div style="float: right">
				<strong>No se encontraron resultados para ese ciudadano, 
					<a href="<?php echo site_url('nuevo_contribuyente/no_encontrado')?>">
					¿desea agregarlo directamente?
					</a>
				</strong>
				
			</div>
		</div>
	<?php } ?>
</div>