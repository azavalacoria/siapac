<div id="form-box">
	<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
	<div>
		<?php echo form_open('usuarios/modificar_usuario/seleccionar'); ?>
		<table>
			<thead>
				<th>Opción</th>
				<th>Nombre Completo</th>
				<th>Nombre de Usuario</th>
			</thead>
			
			<tbody>
				<?php foreach ($usuarios as $usuario) { ?>
					<tr>
						<td class="opcion">
							<?php
							echo form_radio(
								array('name'=>'opcion','group'=>'usuarios','value'=>$usuario->idusuario)
								); 
							?>
						</td>
						<td>
							<span>
								<?php echo strtoupper($usuario->nombrecompleto); ?>
							</span>
						</td>
						<td>
							<span>
								<?php echo $usuario->nickname; ?>
							</span>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<div>
			<?php echo form_submit(array('name'=>'elegir','value'=>'Elegir Usuario','class'=>'submit')); ?>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>