<div id="form-box">
	<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
	<div>
		<?php echo form_open('usuarios/modificar_usuario/modificar') ?>
		<?php echo form_hidden('idusuario',$idusuario);
		?>
		<div class="form-row">
			<div class="normal-row">
				<?php echo form_label('Nombre Completo'); ?>
				<?php echo form_input(
						array(
								'name' => 'nombre',
								'value' => set_value('nombre'),
								'id' => 'nombre'
							)
					); ?>
			</div>
			<div class="normal-row">
				<?php echo form_label('Nombre de Sesión'); ?>
				<?php echo form_input(
						array(
								'name' => 'nickname',
								'value' => set_value('nickname'),
								'id' => 'nickname'
							)
					); ?>
			</div>
		</div>
		<div class="form-row">
			<div class="normal-row">
				<?php echo form_label('Contraseña'); ?>
				<?php echo form_password(
						array(
								'name' => 'password',
								'value' => set_value('password'),
								'id' => 'password'
							)
					); ?>
			</div>
			<div class="normal-row">
				<?php echo form_label('Repetir Contraseña'); ?>
				<?php echo form_password(
						array(
								'name' => 'vpassword',
								'value' => set_value('vpassword'),
								'id' => 'vpassword'
							)
					); ?>
			</div>
		</div>
		<div class="form-row">
			<div class="normal-row">
				<?php echo form_label('Rol'); ?>
				<?php echo form_dropdown('rol',$roles, set_value('rol')); ?>
			</div>
		</div>
		<div><?php echo form_submit(array('name'=>'agregar','value'=>'Agregar Usuario','class'=>'submit')); ?></div>
		<?php echo form_close(); ?>
	</div>
</div>