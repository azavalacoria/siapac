<div id="message">
	<h3>Reporte de cobro</h3>
</div>
<div id="report-box">
	<?php
	$sumtotal = 0;
	$rezago = 0;
	$actual = 0;
	$contratacion = 0;
	$bonificacion = 0;
	?>
	<table>
		<thead>
			<th>Folio</th>
			<th>Rezago</th>
			<th><?php echo date('Y'); ?></th>
			<th>Conexión</th>
			<th>Bonificación</th>
			<th>Total</th>
		</thead>
		<tbody>
			<?php foreach ($folios as $recibo) { ?>
			<tr>
				<td><?php printf("%s",$recibo['folio']); ?></td>
				<td><?php printf("$ %.2f",$recibo['rezago']); ?></td>
				<?php $rezago = $rezago + $recibo['rezago'] ; ?>
				<td><?php printf("$ %.2f",$recibo['actual']); ?></td>
				<?php $actual = $actual + $recibo['actual'] ; ?>
				<td><?php printf("$ %.2f",$recibo['contratacion']); ?></td>
				<?php $contratacion = $contratacion + $recibo['contratacion']; ?>
				<td><?php printf("$ %.2f",$recibo['bonificacion']); ?></td>
				<?php $bonificacion = $bonificacion + $recibo['bonificacion']; ?>
				<td><?php printf("$ %.2f",$recibo['total']); ?></td>
				<?php $sumtotal = $sumtotal + $recibo['total']; ?>
			</tr>
			<?php } ?>
			<tr>
				<td colspan="6" ></td>
			</tr>
			<tr>
				<td colspan="6" ></td>
			</tr>
			<tr id="totales">
				<th></th>
				<th><?php printf("$ %.2f", $rezago); ?></th>
				<th><?php printf("$ %.2f", $actual); ?></th>
				<th><?php printf("$ %.2f", $contratacion); ?></th>
				<th><?php printf("$ %.2f", $bonificacion); ?></th>
				<th><?php printf("$ %.2f", $sumtotal); ?></th>
			</tr>
		</tbody>
	</table>
	
	<br>

</div>
<?php
if (isset($fechainicial)) {
?>
	<?php echo form_open('reporte/imprimir'); ?>
	<?php echo form_input(array('name'=>'fechainicial','value'=>$fechainicial,'type'=>'hidden')); ?>
	<?php echo form_input(array('name'=>'fechafinal','value'=>$fechafinal,'type'=>'hidden')); ?>
	<?php echo form_submit(array('id'=>'submit','name'=>'imprimir','value'=>'Imprimir')); ?>
	<?php echo form_close(); ?>
<?php } ?>
