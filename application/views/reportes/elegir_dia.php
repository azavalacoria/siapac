<div id="form-box">
	<ul id="list-errors">
		<?php echo validation_errors('<li>','</li>'); ?>
	</ul>
	<h3>Seleccionar día para reporte de corte</h3>
	<?php echo form_open('reporte/corte_diario', array('id'=>'reporte')); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Día de corte inicial'); ?>
			<?php echo form_input(array('name'=>'periodoinicial', 'id' =>'periodoinicial','READONLY'=>TRUE ,'value'=>set_value('periodoinicial'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Día de corte final'); ?>
			<?php echo form_input(array('name'=>'periodofinal', 'id' =>'periodofinal','READONLY'=>TRUE ,'value'=>set_value('periodofinal'))); ?>
		</div>
	<div>
		<?php echo form_submit(array('name'=>'agregar','id'=>'agregar','value'=>'agregar','content'=>'Agregar','class'=>'boton')); ?>
	</div>
	<?php echo form_close(); ?>
</div>
<?php echo link_tag('css/jquery-ui.css'); ?>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js'); ?>"></script>