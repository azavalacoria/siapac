<?php
$folioinicial = 0;
$foliofinal = 0;
$total_folios = 0;
$rezagodesc = 0;
$actualdesc = 0;
$sumtotal = 0;
$rezago = 0;
$actual = 0;
$contratacion = 0;
$bonificacion = 0;

foreach ($folios as $recibo) {
	if ($folioinicial == 0) {
		$folioinicial = $recibo['folio'];
		$foliofinal = $recibo['folio'];
	} else {
		$foliofinal = $recibo['folio'];
	}
	
	$rezago = $rezago + $recibo['rezago'] ;
	$actual = $actual + $recibo['actual'];
	$contratacion = $contratacion + $recibo['contratacion'];
	$bonificacion = $bonificacion + $recibo['bonificacion'];
	$sumtotal = $sumtotal + $recibo['total'];

	$total_folios = $total_folios + 1;
}

$gt = $rezago + $actual;
$unidad = $gt / 100;
$prezago = ($rezago / $unidad) * 0.01;
$pactual = ($actual / $unidad) * 0.01;
//$pcontra = ($contratacion / $unidad) * 0.01;
$tt = $sumtotal - $contratacion;
$rezagodesc = $tt * $prezago;
$actualdesc = $tt * $pactual;

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<?php echo link_tag('css/report.css') ?> 
</head>
<body>
<div class="pagina">

	<div class="datos-direccion" >
			<h2 class="ayuntamiento">H. AYUNTAMIENTO DE CALKIN&#237;</h2>
			<h3 class="organizacion">DIRECCI&#243;N DE AGUA POTABLE</h3>
	</div>
	<div>
		<p class="fecha">
			<?php setlocale(LC_TIME, '') ?>
			Calkin&#237;, Campeche a <?php echo strftime('%#d de %B de %Y'); ?>.
		</p>
	</div>
	<div>
		<p class="mayusculas">
			POR ESTE MEDIO SE INGRESA A TESORERIA MUNICIPAL LA CANTIDAD DE: 
			<?php $sumtotal = ($actual + $rezago) - $bonificacion; ?>
			<span class="cantidad"><?php echo "$ ".number_format($sumtotal, 2, '.',' '); ?></span>
		</p>
		<table class="desgloce-ingresos">
			<tr>
				<th>Agua</th>
				<td >
					<span class="signo-pesos">
						$
					</span>
					<span class="suma-monetaria">
						<?php //echo number_format($actualdesc, 2, '.',' '); ?>
						<?php echo number_format( ($actual + $contratacion) , 2, '.',' '); ?>
					</span>
					
				</td>
			</tr>
			<tr>
				<th>Rezago</th>
				<td>
					<span class="signo-pesos">
						$
					</span>
					<span class="suma-monetaria">
						<?php echo number_format($rezago, 2, '.',' '); ?>
					</span>
				</td>
			</tr>
			<tr>
				<th>Bonificación</th>
				<td>
					<span class="signo-pesos">
						$
					</span>
					<span class="suma-monetaria">
						<?php //echo number_format($contratacion, 2, '.',' '); ?>
						<?php echo number_format($bonificacion, 2, '.',' '); ?>
					</span>
				</td>
			</tr>
			<tr><td colspan="2"><br></td></tr>
			<tr>
				<th>
					Total
				</th>
				<td>
					<span class="signo-pesos">
						$
					</span>
					<span class="suma-monetaria">

						<?php echo number_format($sumtotal, 2, '.',' '); ?>
					</span>
				</td>
			</tr>
		</table>
		<br>
		<p class="mayusculas">
			POR CONCEPTO DE: RECAUDACI&#243;N DE AGUA POTABLE SEGÚN RECIBOS CON FOLIOS DEL
			<?php echo " $folioinicial AL $foliofinal."; ?>
		</p>
		<table class="firmas-autorizacion">
			<tr>
				<td>
					<p class="firmas-titulo">Entreg&#243;</p>
				</td>
				<td>
					<p class="firmas-titulo">VO.Bo</p>
				</td>
				<td>
					<p class="firmas-titulo">Recibi&#243;</p>
				</td>
			</tr>
			<tr>
				<td>
					<br><br><br><br>
					<hr class="firmas-linea">
				</td>
				<td>
					<br><br><br><br>
					<hr class="firmas-linea">
				</td>
				<td>
					<br><br><br><br>
					<hr class="firmas-linea">
				</td>
			</tr>
			<tr>
				<td>
					<p class="firmas-nombre">C. Juan Paulino Uc Chan</p>
					<p class="firmas-nombre">Enc. De Cobro de  Calkini</p>
				</td>
				<td>
					<p class="firmas-nombre">Profr. Juan Pablo Colli Dzul</p>
					<p class="firmas-nombre">Director de Agua Potable</p>
				</td>
				<td>
					<p class="firmas-nombre">LAE. Fernando Canul  Herrera</p>
					<p class="firmas-nombre">Tesorero Municipal</p>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="pagina">
	<div class="datos-direccion" >
			<h2 class="ayuntamiento">H. AYUNTAMIENTO DE CALKIN&#237;</h2>
			<h3 class="organizacion">DIRECCI&#243;N DE AGUA POTABLE</h3>
	</div>
	<div>
		<p class="fecha">
			<?php setlocale(LC_TIME, '') ?>
			Calkin&#237;, Campeche a <?php echo strftime('%#d de %B de %Y'); ?>.
		</p>
	</div>
	<div>
		<p class="mayusculas">
			POR ESTE MEDIO SE INGRESA A TESORERIA MUNICIPAL LA CANTIDAD DE: 
			<span class="cantidad"><?php echo "$ ".number_format($sumtotal, 2, '.',' '); ?></span>
		</p>
		<table class="desgloce-ingresos">
			<tr>
				<th>Agua</th>
				<td >
					<span class="signo-pesos">
						$
					</span>
					<span class="suma-monetaria">
						<?php echo number_format( ($actual + $contratacion) , 2, '.',' '); ?>
					</span>
					
				</td>
			</tr>
			<tr>
				<th>Rezago</th>
				<td>
					<span class="signo-pesos">
						$
					</span>
					<span class="suma-monetaria">
						<?php echo number_format($rezago, 2, '.',' '); ?>
					</span>
				</td>
			</tr>
			<tr>
				<th>Bonificación</th>
				<td>
					<span class="signo-pesos">
						$
					</span>
					<span class="suma-monetaria">
						<?php echo number_format($bonificacion, 2, '.',' '); ?>
					</span>
				</td>
			</tr>
			<tr><td colspan="2"><br></td></tr>
			<tr>
				<th>
					Total
				</th>
				<td>
					<span class="signo-pesos">
						$
					</span>
					<span class="suma-monetaria">
						<?php echo number_format($sumtotal, 2, '.',' '); ?>
					</span>
				</td>
			</tr>
		</table>
		<br>
		<p class="mayusculas">
			POR CONCEPTO DE: RECAUDACI&#243;N DE AGUA POTABLE SEGÚN RECIBOS CON FOLIOS DEL
			<b><?php echo " $folioinicial AL $foliofinal."; ?></b>
		</p>

		<table class="firmas-autorizacion">
			<tr>
				<td>
					<p class="firmas-titulo">Entreg&#243;</p>
				</td>
				<td>
					<p class="firmas-titulo">VO.Bo</p>
				</td>
				<td>
					<p class="firmas-titulo">Recibi&#243;</p>
				</td>
			</tr>
			<tr>
				<td>
					<br><br><br><br>
					<hr class="firmas-linea">
				</td>
				<td>
					<br><br><br><br>
					<hr class="firmas-linea">
				</td>
				<td>
					<br><br><br><br>
					<hr class="firmas-linea">
				</td>
			</tr>
			<tr>
				<td>
					<p class="firmas-nombre">C. Juan Paulino Uc Chan</p>
					<p class="firmas-nombre">Enc. De Cobro de  Calkini</p>
				</td>
				<td>
					<p class="firmas-nombre">Profr. Juan Pablo Colli Dzul</p>
					<p class="firmas-nombre">Director de Agua Potable</p>
				</td>
				<td>
					<p class="firmas-nombre">LAE. Fernando Canul  Herrera</p>
					<p class="firmas-nombre">Tesorero Municipal</p>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="pagina">
		<h3>Deposito Calkini</h3>
		<table class="detalles">
			<tr>
				<th>Folio</th>
				<th>Rezago</th>
				<th><?php echo date('Y'); ?></th>
				<th>Conexión</th>
				<th>Bonificación</th>
				<th>Total</th>
			</tr>
			<?php foreach ($folios as $recibo) { ?>
			<tr>
				<td><?php printf("%s",$recibo['folio']); ?></td>
				<td><?php printf("$ %.2f",$recibo['rezago']); ?></td>
				<td><?php printf("$ %.2f",$recibo['actual']); ?></td>
				<td><?php printf("$ %.2f",$recibo['contratacion']); ?></td>
				<td><?php printf("$ %.2f",$recibo['bonificacion']); ?></td>
				<td><?php printf("$ %.2f",$recibo['total']); ?></td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan="6">
					<br>
				</td>
			</tr>
			<tr id="totales">
				<th></th>
				<th><?php printf("$ %.2f", $rezago); ?></th>
				<th><?php printf("$ %.2f", $actual); ?></th>
				<th><?php printf("$ %.2f", $contratacion); ?></th>
				<th><?php printf("$ %.2f", $bonificacion); ?></th>
				<th><?php printf("$ %.2f", $sumtotal); ?></th>
			</tr>
			</tbody>
		</table>
		<?php echo br(1); ?>
		<div>
			<br>
				<span>Total: </span>
				<span id="total-final"><?php printf("$ %.2f", ($sumtotal)); ?></span>
			</b>
		</div>
		<?php echo br(3); ?>
		<p class="mayusculas">
			Folios: 
			<b><?php echo " $folioinicial AL $foliofinal"; ?></b>.
			<br>
			<span class="mayusculas"><b>Nota:</b> A la suma del agua se le a restado la bonificacion</span>
		</p>
		
	</div>
</div>
</body>
</html>