<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<?php echo link_tag('css/rstyle.css')?>
</head>
<body>
	<div id="wrap">

		<div id="header">
		</div>
		<div id="content">
			<div id="area-folio">
				<span id="folio-etiqueta">Folio:</span>
				<span id="folio-numero">
					<?php 
					if (isset($folio_recibo)) {
						echo $folio_recibo;
					}
					?>
				</span>
			</div>
			<div id="area-datos">
				<table>
					<tr>
						<th>Nombre</th>
						<td></td>
						<th>Fecha</th>
						<td><?php echo date("d/m/Y")?></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo $this->session->userdata('nombrecompleto'); ?></td>
						<th>Contrato</th>
						<td><?php echo $this->session->userdata('numero'); ?></td>
					</tr>
					<tr>
						<th>Dirección</th>
						<td></td>
						</td>
					</tr>
					<tr>
						<td colspan="2"><?php echo $this->session->userdata('domicilio'); ?></td>
						<th>Cajero</th>
						<td>1</td>
					</tr>
				</table>
				
			</div>
		</div>
		<div id="items">
			<table>
				<tr>
					<th class="periodo">Periodo</th>
					<th class="cantidad">Cantidad</th>
					<th class="concepto">Concepto</th>
					<th class="precio-unitario">Precio Unitario</th>
					<th class="importe">Importe</th>
				</tr>
				<?php if (isset($items)) { ?>

					<?php foreach ($items as $i) { ?>
				<tr>
					<td class="centered"> <?php echo $i->periodo; ?></td>
					<td class="centered"> <?php echo $i->cantidad; ?></td>
					<td class="centered"> <?php echo $i->concepto; ?></td>
					<td class="dinero"> <?php printf("$ %.2f", $i->preciounitario); ?></td>
					<td class="dinero"> <?php printf("$ %.2f", $i->importe) ?></td>
				</tr>
					<?php } ?>
				<?php } ?>
			</table>
		</div>
		<div id="totales">
			<div id="area-rfc">
			</div>
			<div id="area-cantidades">
				<table>
					<tr>
						<th>Subtotal</th>
						<td class="dinero"><?php printf("$ %.2f",$subtotal); ?></td>
					</tr>
					<tr>
						<th>
							Bonificación
							<br>
							<?php
							$b = $this->session->userdata('bon');
							if($b > 0) {
								echo $b." %";
							}
							?>
						</th>
						<td class="dinero"><?php printf("$ %.2f", $descuento)?></td>
					</tr>
					<tr>
						<th>Total</th>
						<td class="dinero"><?php printf( '$ %.2f' ,($subtotal - $descuento))?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	
</body>
<?php $this->session->set_userdata(array('bon' => 0)); ?>
</html>