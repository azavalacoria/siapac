<style type="text/css">
	#ncontrato {
		border: 0;
		background-color: transparent;
	}
	table{
		font-size: 13px;
	}
</style>
<div id="head-sale">
	<div> <ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul> </div>
	<div>
		<span>
			Enlistando <?php echo $this->session->userdata('inicio'); ?> de <?php echo $this->session->userdata('limite'); ?>
		</span>
	</div>
	<table>
		<thead>
			<th>Número de contrato</th>
			<th>Nombre del contribuyente</th>
			<th>Domicilio</th>
			<th>Fecha de baja</th>
			<th>Motivo de baja</th>
			<th>Constancia de Baja</th>
		</thead>
		<tbody>
			<?php foreach ($contratos as $contrato) { ?>
				<tr>
				<?php echo form_open('catalogos/imprimir_baja_contrato',array('id'=>'contrato-form')) ?>
					<td class="centered">
						<span><?php echo form_input(array('name'=>'numero','READONLY'=>TRUE,'value'=>$contrato->numero,'id'=>'ncontrato')); ?></span>
					</td>
					<td> 
						<?php printf("%s %s %s", $contrato->apellidopaterno, $contrato->apellidomaterno, $contrato->nombres); ?>
					</td>
					<td>
						<?php printf("Calle %s, No. %s %s. Colonia %s", 
						$contrato->calle, $contrato->numeroexterior, $contrato->numerointerior, $contrato->nombrecolonia);?> 
					</td>
					<td class="centered"> <?php echo $contrato->fechabaja; ?> </td>
					<td class="centered"> <?php echo $contrato->motivobaja; ?> </td>
					<td>
						<?php echo form_button(array('name'=>'opcion','value'=>'ant','content'=>'Imprimir','type'=>'submit')); ?>
					<?php echo form_close(); ?>
					</td>
				</tr>
			<?php }?>
		</tbody>
	</table>
	<div id="select-buttons">
		<span>
			<!--<?php echo form_button(array('name'=>'elegir','value'=>'on','type'=>'submit','content'=>'Elegir')); ?>-->
		</span>
		 
	<?php $num_anteriores = $this->session->userdata('inicio');?>

	<?php if (isset($anteriores) && $num_anteriores != 0): ?>
		<?php echo form_open($anteriores); ?>
		<span>
			<?php echo form_button(array('name'=>'opcion','value'=>'ant','content'=>'Anteriores','type'=>'submit')); ?>
		</span>
		<?php echo form_close(); ?>
	<?php endif ?>
	
	<?php if (isset($busqueda_nueva)): ?>
		<?php echo form_open($busqueda_nueva); ?>
		<span>
			<?php echo form_button(array('name'=>'opcion','value'=>'sig','content'=>'Siguientes','type'=>'submit')); ?>
		</span>
		<?php echo form_close(); ?>
	</div>
	<?php endif ?>
</div>