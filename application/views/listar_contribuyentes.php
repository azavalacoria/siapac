<div>
	<h3>
	<?php
	if (isset($mensaje)) {
		echo($mensaje);
	}
	?>
	</h3>
</div>
<div id="report-box">
	<table>
		<thead>
			<th></th>
			<th>Apellidos</th>
			<th>Nombre(s)</th>
			<th>Nacimiento</th>
			<th>Dirección</th>
		</thead>
		<tbody>
			<?php echo form_open('nuevo_contrato/crear') ?>
			<?php foreach ($contribuyentes as $c) { ?>
				<tr>
					<td class="centered">
						<?php echo form_radio(array('name'=>'contribuyente','value'=>$c->idcontribuyente,'group'=>'contribuyentes')); ?>
					</td>
					<td><?php printf("%s %s", $c->apellidopaterno, $c->apellidomaterno); ?></td>
					<td><?php echo $c->nombres; ?></td>
					<td class="centered"><?php echo $c->fechanacimiento; ?></td>
					<td> <?php printf("%s %s %s", $c->colonia, $c->calle, $c->numero); ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<div>
		<div id="select-contrib">
			<?php
			if ( isset($run) &&$run == 1) {
				echo form_button(array('name'=>'opcion','value'=>'elegir', 'type'=>'submit','content'=>'Elegir')); 
			}
			?>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>