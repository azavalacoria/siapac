<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/contrato.css'); ?>">

</head>
<body class="fondo">
	<div class="hoja" style="">
		<br><br><br>
		<div id="report-box">
			<table>
				<tr>
					<th>Nombre del<br>Contribuyente</th>
					<td><?php echo $nombrecompleto; ?></td>
					<th>Contrato</th>
					<td><?php echo $numero; ?></td>
				</tr>
				<tr>
					<th>Último Pago</th>
					<td><?php echo $ultimopago; ?></td>
					<th>Domicilio del<br>Contrato</th>
					<td>Calle <?php echo $domicilio; ?></td>
				</tr>
			</table>
			<br><br><br><br>
			<table>
				<tr>
					<th>Periodo</th>
					<th>Cantidad</th>
					<th>Concepto</th>
					<th>Precio Unitario</th>
					<th>Importe</th>
				</tr>

					<?php
					foreach ($items as $item) {
					?>
					<tr>
						<td><?php echo $item['periodo']; ?></td>
						<td><?php echo $item['cantidad']; ?></td>
						<td><?php echo $item['concepto']; ?></td>
						<td><?php printf("$ %.2f", $item['preciounitario']); ?></td>
						<td><?php printf("$ %.2f", $item['importe']); ?></td>
					</tr>
					<?php
					}
					?>

			</table>
			<div id="gran-total">
				<table>
					<tr>
						<th>Total</th>
						<td><td><?php printf("$ %.2f", $total); ?></td></td>
					</tr>
				</table>
			</div>
		</div>	
	</div>
</body>
</html>