<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/contrato.css'); ?>">

</head>
<body class="fondo">
	<div class="hoja" style="">
		<div id="contrato">
			<?php 
			$contrato = $this->session->userdata('ncontrato');
			if (isset($contrato)) {
				echo 'CONTRATO: '.strtoupper($contrato);
			} else {
				echo "CALK/000";
			} ?>
		</div>
		
		<p id="presentacion">
			<strong>
			CONTRATO DE PRESTACIÓN DE SERVICIO PÚBLICO DE AGUA POTABLE QUE CELEBRAN POR UNA PARTE 
			EL SISTEMA MUNICIPAL DE AGUA POTABLE DE CALKINI A LA QUE EN LO SUCESIVO SE DENOMINARÁ 
			"SMAPCALK", POR LA OTRA EL (LA) CIUDADANO(A) QUIEN EN LO SUCESIVO SE DENOMINARÁ "EL 
			USUARIO" PARTES QUE SE SOMETEN AL TENOR DE LAS SIGUIENTES:
			</strong>
		</p>

		<p id="declaraciones">
			DECLARACIONES
		</p>
		
		<ul id="articulos">
			<li>DECLARA <strong>"SMAPCALK"</strong></li>
			<ul class="subarticulos">
				<li>
					I.I.- Ser un Organismo Público con personalidad jurídica y patrimonio 
					propio y con funciones de autoridad administrativa, de acuerdo a lo establecido 
					en la Ley de Aguas del Estado de Campeche.
				</li>
			</ul>
			<li>
				DECLARA <strong>"El usuario"</strong>
			</li>
			<ul class="subarticulos">
				<li>
					II.I.- Ser propietario o poseedor del predio ubicado en calle <strong>
					<?php echo strtoupper($this->session->userdata('cdireccion')); ?></strong> en 
					donde se instalará el servicio público de agua potable con toma de tipo 
					<strong><?php echo $this->session->userdata('tipotoma'); ?></strong>, 
					en términos del artículo 104, de la propia Ley de Aguas para 
					el Estado Libre y Soberano de Campeche No. 574, tiene capacidad jurídica para ejecutar el presente 
					contrato y está facultado para solicitar los servicios públicos antes descritos.
				</li>
				<li>
					II.II.- Se compromete a la reparación de fugas en el interior de su predio como 
					también a reportar en las oficinas del SMAPCALK las fugas de las que se percate 
					en la localidad.
				</li>
			</ul>
		</ul>

		<p id="declaraciones2">
			Declaran ambas partes reconocerse mutuamente la personalidad con la que se ostentan y una 
			vez que fueron leídas las anteriores declaraciones, las partes están de acuerdo a someterse 
			a las siguientes:
		</p>

		<p id="clausulas-titulo">
			CLAUSULAS
		</p>

		<p id="clausulas-seccion">
			<ul class="clausulas">
				<li>
					PRIMERA.- El objeto del presente instrumento, es la contratación de los servicios que 
					presta el sistema Municipal de Agua Potable del Honorable Ayuntamiento Calkini.
				</li>
				<li>
					SEGUNDA.- El usuario se compromete a pagar los derechos por concepto de contratación 
					de los servicios públicos de agua potable, como lo establecen los Artículos. 104, 105, 
					106, 109 y 121 de la Ley de Aguas para el Estado Libre y Soberano de Campeche número 574, 
					y los artículos 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 
					76, 77, 79, 80, y 81 de la Ley número 547 de Ingreso para el Municipio de Calkini.
				</li>
				<li>
					TERCERA.- El usuario se compromete a pagar mensualmente o por adelantado a partir de la fecha 
					del contrato del servicio, en las oficinas de S.M.A.P.CALK, o en los lugares autorizados 
					conforme a las tarifas vigentes, de acuerdo a la cuota que se aplique para la localidad. 
					El cálculo del cobro será siempre por periodos de Treinta días y en la categoría de usuario 
					que le corresponda o el tipo de servicio indicado, con base a las tarifas que se encuentren 
					vigentes para los ejercicios fiscales correspondientes.
				</li>
				<li>
					CUARTA.- A la falta de pago por dos meses consecutivos, se hará efectivo su cobro con las 
					multas y recargos a que se haga acreedor, así como se determinara la limitación, suspensión 
					provisional o cancelación del servicio, mediante el procedimiento económico coactivo en 
					términos de lo establecido en la Legislación Fiscal aplicable, conforme a lo señalado en la 
					cláusula que antecede.
				</li>
				<li>
					QUINTA.- El SMAPCALK podrá realizar inspecciones a los usuarios y rescindir el presente contrato 
					cuando:
					<ul id="subclausulas">
						<li>
							1) Se detecte que el usuario utilice el servicio en forma distinta a la contratada.
						</li>
						<li>
							2) Cuando el usuario tenga adeudos en otros contratos con el SMAPCALK.
						</li>
						<li>
							3) Cuando el usuario permita la conexión de agua a otros predios de su toma.
						</li>
						<li>
							4) Cuando el usuario sea sorprendido con el uso inadecuado del agua se le notificara, de 
							hacer caso omiso se le sancionara o suspenderá de manera temporal la toma y de reincidir 
							se le suspenderá de forma definitiva.
						</li>
					</ul>
				</li>
			</ul>
		</p>
        <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
		<p id="conclusion">
			Leído que fue por las partes que en él intervienen y enteradas de su contenido y alcance legal, 
			se firma el presente contrato al final y margen en la Ciudad de Calkini, Estado de Campeche a 
			<strong><?php setlocale(LC_ALL, ''); echo strftime("%#d de %B de %Y"); ?></strong>.
		</p>

		<div id="firmas">
			<table id="firmas-tabla">
				<tr>
					<th>"El usuario"</th>
					<th></th>
					<th>SMAPCALK</th>
				</tr>
				<tr>
					<td><hr></td>
					<td></td>
					<td><hr></td>
				</tr>
				<tr>
					<td id="el-usuario">
						<?php 
						$contribuyente = $this->session->userdata('nombrecompleto');
						if (isset($contribuyente)) {
							echo strtoupper($contribuyente);
						} else {
							echo "El solicitante";
						}
						?>
					</td>
					<td id="separador"></td>
					<td id="el-smapcal">
						JUAN PABLO COLLÍ DZUL
						<br>
						Director
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>