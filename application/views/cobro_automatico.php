<div id="head-sale">
	<table>
		<thead>
			<tr>
			<th>No. de Contrato</th>
			<th>Nombre Completo</th>
			<th>Domicilio</th>
			<th>Último Pago</th>
		</tr>
		</thead>
		<tbody>
			<tr>
			<td class="centered"><?php echo $this->session->userdata('contrato'); ?></td>
			<td class="centered"><?php echo $this->session->userdata('nombrecompleto'); ?></td>
			<td class="centered"><?php echo $this->session->userdata('domicilio'); ?></td>
			<td class="centered"><?php echo $this->session->userdata('ultimopago'); ?></td>
		</tr>
		</tbody>
	</table>
</div>
<div id="items">
	<table>
		<thead>
			<!-- <th class="opt">Opción</th> -->
			<th class="period">Periodo</th>
			<th class="number-cell">Cantidad</th>
			<th>Concepto</th>
			<th class="number-cell">Precio</th>
			<th class="number-cell">Importe</th>
		</thead>
		<tbody>
			<?php foreach ($items as $i) { ?>
			<tr>
				<!-- <td class="centered"><?php //echo form_radio(array('name'=>'item','value'=>$i->iditem)); ?></td> -->
				<td class="centered" id="<?php echo $i->iditem; ?>"><?php echo $i->periodo; ?></td>
				<td class="centered"><?php echo $i->cantidad; ?></td>
				<td style="number-content"><?php echo $i->concepto; ?></td>
				<td class="number-content"><?php echo $i->preciounitario; ?></td>
				<td class="number-content"><?php echo $i->importe; ?></td>
			</tr>
			<?php }?>
		</tbody>
	</table>
</div>
<div id="totals">
		<table>
			<tr>
				<th>Subtotal</th>
				<td class="total-number"><?php printf("$ %.2f", $subtotal); ?></td>
			</tr>
			<tr>
				<th>Descuento</th>
				<td class="total-number"><?php printf("$ %.2f", $descuento); ?></td>
			</tr>
			<tr>
				<th>Subtotal</th>
				<td class="total-number"><?php printf("$ %.2f", ($subtotal - $descuento));; ?></td>
			</tr>
		</table>
	</div>
<div id="form-box">
		<?php echo form_open('cobro_automatico'); ?>
		<?php echo form_button(array('name'=>'accion','value'=>'pagar','type'=>'submit','content'=>'Cobrar'));?>
		<?php echo nbs(3); ?>
		<?php echo form_button(array('name'=>'accion','value'=>'cancelar','type'=>'submit','content'=>'Cancelar'));?>
		<?php echo form_close(); ?>
</div>
