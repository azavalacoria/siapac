<div id="form-box">
	<div>
			<ul id="list-errors"><?php echo validation_errors('<li>','</li>')?></ul>
	</div>
	
	<?php echo form_open('buscar_contrato/por_nombre');?>

	<?php //echo base_url();?>
	<h4>Buscar por nombre</h4>
	<div class="form-row">
		<div class="normal-row">
			<label>Apellido Paterno</label>
			<?php echo form_input(array('name'=>'apellidopaterno', 'id'=>'apellidopaterno', 'value'=>set_value('apellidopaterno')))?>
		</div>
		<div class="normal-row">
			<label>Apellido Materno</label>
			<?php echo form_input(array('name'=>'apellidomaterno','value'=>set_value('apellidomaterno')))?>
		</div>
		<div class="normal-row">
			<label>Nombres</label>
			<?php echo form_input(array('name'=>'nombres','value'=>set_value('nombres')))?>
		</div>
	</div>
	<div>
		<?php echo form_button(array('name'=>'pornombre','type'=>'submit','content'=>'Buscar por Nombre'))?>
	</div>
	<?php echo form_close();?>
	<?php echo form_open('buscar_contrato/por_contrato');?>
	<h4>Buscar por número de contrato</h4>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Número de contrato');?>
			<?php echo form_input(array('name'=>'contrato','value'=>set_value('contrato')))?>
		</div>
		<div>
			<?php echo form_button(array('name'=>'porcontrato','type'=>'submit','content'=>'Buscar por Contrato'));?>
		</div>
		
	</div>
	<script type="text/javascript">
	$('#apellidopaterno').focus();
	</script>
	<?php echo form_close();?>
</div>