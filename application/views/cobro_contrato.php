<?php
$subtotal = 0;
$descuento = 0;
?>
<div>
	<ul id="list-errors">
		<?php
		$errores_periodo = $this->session->userdata('errores_periodo');
		if (isset($errores_periodo)) {
			echo $errores_periodo;
		?>
		<li>Agrege tarifas faltantes.</li>
		<?php }
		?>
	</ul>
</div>
<div id="head-sale">
	<table>
		<thead>
			<tr>
			<th>No. de Contrato</th>
			<th>Nombre Completo</th>
			<th>Domicilio</th>
		</tr>
		</thead>
		<tbody>
			<tr>
			<td class="centered"><?php echo $contrato; ?></td>
			<td class="centered"><?php echo $nombrecompleto; ?></td>
			<td class="centered"><?php echo $domicilio; ?></td>
		</tr>
		</tbody>
	</table>
</div>
<div id="items">
	<table>
		<thead>
			<th class="opt">Opción</th>
			<th class="period">Periodo</th>
			<th class="number-cell">Cantidad</th>
			<th>Concepto</th>
			<th class="number-cell">Precio</th>
			<th class="number-cell">Importe</th>
		</thead>
		<tbody>
			<?php foreach ($items as $i) { ?>
			<tr>
				<td class="centered"><?php echo form_radio(array('name'=>'item','value'=>$i->iditem)); ?></td>
				<td class="centered"><?php echo $i->periodo; ?></td>
				<td class="centered"><?php echo $i->cantidad; ?></td>
				<td style="number-content"><?php echo $i->concepto; ?></td>
				<td class="number-content"><?php echo $i->preciounitario; ?></td>
				<td class="number-content"><?php echo $i->importe; ?></td>
				<?php $subtotal = $subtotal + $i->importe; ?>
			</tr>
			<?php }?>
		</tbody>
	</table>
</div>
<div id="totals">
	<?php echo form_open('cobro_parcial/editar'); ?>
		<table>
			<tr>
				<th>Subtotal</th>
				<td class="total-number"><?php printf("$ %.2f", $subtotal); ?></td>
			</tr>
			<tr>
				<th>Bonificación</th>
				<td class="total-number"><?php printf("$ %.2f", $descuento); ?></td>
			</tr>
			<tr>
				<th>Subtotal</th>
				<td class="total-number"><?php printf("$ %.2f", ($subtotal - $descuento));; ?></td>
			</tr>
		</table>
	</div>
<div id="form-box">
	<div style="float: left; padding-bottom: 15px">
		
		<?php echo form_button(array('name'=>'accion','value'=>'agregar','type'=>'submit','content'=>'Agregar Periodo'));?>

		<span>
			<?php 
			if (count($items) > 0) {
				echo form_button(array('name'=>'accion','value'=>'bonificar','type'=>'submit','content'=>'Agregar bonif.'));
			}
			?>
		</span>
		<?php echo nbs(3); ?>
		<?php //echo form_button(array('name'=>'accion','value'=>'eliminar','type'=>'submit','content'=>'Eliminar Item'));?>
		<?php echo form_close(); ?>
	</div>
	<div style="float: right">
		<?php echo form_open('nuevo_contrato/cobrar',array('id'=>'pagarrecibo')); ?>
		<?php
		if (strlen($errores_periodo) == 0) {
			echo form_button(array('name'=>'accion','value'=>'pagar','type'=>'submit','content'=>'Cobrar'));
		}
		?>
		<?php //echo nbs(3); ?>
		<?php //echo form_button(array('name'=>'accion','value'=>'cancelar','type'=>'submit','content'=>'Cancelar'));?>
		<?php echo form_close(); ?>
	</div>
</div>