<div id="form-box">
	<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
	<?php echo form_open('cobro_parcial/agregar_descuento'); ?>
	<div class="form-row">
		<div class="normal-row">
			<label>Porcentaje Descuento</label>
			<?php echo form_input(array('name'=>'descuento', 'value'=>set_value('descuento'))); ?>
		</div>
		<div>
			<?php echo form_submit(array('name'=>'agregar','value'=>'Agregar','id'=>'boton')); ?>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>