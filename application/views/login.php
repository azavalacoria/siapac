<div id="form-box">
	<div>
		<ul id="list-errors"> <?php echo validation_errors('<li>','</li>'); ?> </ul>
	</div>
	<?php echo form_open('login/iniciar'); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Nombre de Usuario'); ?>
			<?php echo form_input( array('name'=>'usuario', 'id'=>'usuario','value'=>set_value('usuario')) ); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Contraseña'); ?>
			<?php echo form_password( array('name'=>'password', 'value'=>set_value('password')) ); ?>
		</div>
	</div>
	<?php echo form_submit( array('name'=>'iniciar', 'value'=>'Iniciar Sesión','id'=>'submit') ) ?>
</div>
	<?php echo form_close(); ?>
<script type="text/javascript">
$('#usuario').focus();
</script>