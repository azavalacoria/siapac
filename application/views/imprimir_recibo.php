<html>
<head>
	<meta charset="UTF-8">
</head>
<body>

<div id="form-box">
	<h3><?php echo $mensaje; ?></h3>
	<p>Puede hacer clic para 
		<a href="<?php echo site_url('nuevo_contrato/imprimir') ?>">imprimir el contrato</a>.
	</p>
	<p>También puede hacer clic para 
		<a href="<?php echo site_url('nuevo_contrato/imprimir_recibo_cobro') ?>">imprimir el recibo de cobro del contrato</a>.
	</p>
</div>
</body>