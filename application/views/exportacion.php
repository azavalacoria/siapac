<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js') ?>" /></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js') ?>"></script>
<?php echo link_tag(base_url('css/jquery-ui.css')) ?>
<div id="form-box">
	<div>
		<ul id="list-errors"> <?php echo validation_errors('<li>','</li>'); ?> </ul>
	</div>
	<?php echo form_open('exportar/manual', array('id'=>'exportacion')); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Fecha inicial'); ?>
			<?php
			if (isset($periodoinicial)) {
				echo form_input( array('name'=>'periodoinicial','READONLY'=>TRUE, 'value'=>$periodoinicial, 'id'=>'periodoinicial' ) );
			} else {
				echo form_input( array('name'=>'periodoinicial','READONLY'=>TRUE, 'value'=>set_value('periodoinicial'), 'id'=>'periodoinicial' ) );
			}
			?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Fecha final'); ?>
			<?php echo form_input( array('name'=>'periodofinal','READONLY'=>TRUE, 'value'=>set_value('periodofinal'), 'id'=>'periodofinal') ); ?>
		</div>
	</div>
	<div>
		<?php echo form_button( array('name'=>'iniciar', 'value'=>'exportar','class'=>'submit', 'id'=>'exportar' ,'content'=>'Exportar')); ?>
	</div>
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
$('#periodoinicial').focus();

$('#exportar').click(
		function () {
			$fechainicial = Date.parse($('#periodoinicial').val());
			$fechafinal = Date.parse($('#periodofinal').val());
			if ( $('#periodoinicial').val() == '' || $('#periodofinal').val() == '' ) {
				alert('Debe ingresar las dos fechas para iniciar la exportación');
			} else {
				if ($fechafinal >= $fechainicial) {
					$('#exportar').attr('type','submit');
					$('#exportacion').submit();
				} else {
					alert('La fecha inicial es mayor a la final, verifíque sus fechas elegidas');
				}
			}
		}
	);
</script>