
<div id="form-box">
	<div class="form-row">
		<span><?php echo $mensaje; ?></span>
		<ul id="list-errors">
			<?php echo validation_errors('<li>','</li>') ?>
		</ul>
	</div>
	<div style="inline">
			<?php echo form_label('Contribuyente: '); ?>
			<?php foreach ($contribuyente as $c) { ?>
					<p><?php printf("%s %s %s", $c->nombres, $c->apellidopaterno, $c->apellidomaterno); ?></p>
					<?php $nombre = strtoupper($c->nombres." ".$c->apellidopaterno." ".$c->apellidomaterno); ?>
					<?php $this->session->set_userdata(array('nombrecompleto'=>$nombre)); ?>
			<?php } ?>

	</div>
	
	<?php echo form_open('nuevo_contrato/agregar', array('name'=>'contrato','id'=>'contrato')) ?>
	<div>
		<?php echo form_input(array('name'=>'contribuyente','value'=>$this->session->userdata('idcontribuyente'),'type'=>'hidden')) ?>
	</div>
	<?php $js = 'id="prefijo" onChange="generar_numero();"';?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Iniciales de Contrato'); ?>
			<?php echo form_dropdown('prefijo', $prefijos, set_value('prefijo'), $js); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Número de Contrato'); ?>
			<?php
			if (isset($numerocontrato)) {
				echo form_input(array('name'=>'numero','value'=>$numerocontrato,'id'=>'numero'));
			} else {
				echo form_input(array('name'=>'numero','value'=>set_value('numero'),'id'=>'numero'));
			}
			?>
		</div>
		<div class="normal-row">
			<?php echo form_button(array('name'=>'generar','value'=>'gen','id'=>'generador','type'=>'button','content'=>'Generar')); ?>
			
		</div>
	</div>
	<br>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Tipo de Servicio'); ?>
			<?php echo form_dropdown('tiposervicio', $servicios, set_value('tiposervicio')) ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Medidor'); ?>
			<span>Si</span>
			<input group="medidor" type="radio" name="medidor" value="si" <?php echo set_radio('medidor', 'si'); ?> />
			<span>No</span>
			<input group="medidor" type="radio" name="medidor" value="no" <?php echo set_radio('medidor', 'no'); ?> />
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Zona'); ?>
			<select name="zona" id="zona">
				<option value="0"></option>
				<?php foreach ($zonas as $zona) { ?>
					<?php
						$vzona = $_POST['zona'];
						if ($vzona == $zona->idzona) {
							$texto = 'value="'.$vzona.'" selected';
						} else {
							$texto = 'value="'.$zona->idzona.'"';
						}
					?>
					<option <?php echo $texto; ?> > <?php echo $zona->nombrezona; ?> </option>
				<?php } ?>
			</select>

		</div>
		<div class="normal-row">
			<?php echo form_label('Colonia'); ?>
			<?php echo form_dropdown('colonia', $colonias, set_value('colonia') ); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Localidad'); ?>
			<?php echo form_input(array('name'=>'localidad', 'id'=>'localidad' ,'value'=>set_value('localidad'))) ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Calle'); ?>
			<?php echo form_input(array('name'=>'calle','value'=>set_value('calle'))) ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Número Exterior'); ?>
			<?php echo form_input(array('name'=>'numeroexterior','value'=>set_value('numeroexterior'))) ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Número Interior'); ?>
			<?php echo form_input(array('name'=>'numerointerior','value'=>set_value('numerointerior'))) ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<label>Referencias</label>
			<?php echo form_textarea(array('name'=>'referencias','value'=>set_value('referencias'),'cols'=>'28','rows'=>'4')); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Codigo Postal'); ?>
			<?php echo form_input(array('name'=>'cp', 'id'=>'codigopostal' ,'value'=>set_value('cp'))) ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Último Pago'); ?>
			<?php echo form_input(array('name'=>'ultimopago','value'=>set_value('ultimopago'),'id'=>'periodoinicial')) ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Observaciones'); ?>
			<?php //echo form_input(array('name'=>'observaciones','value'=>set_value('observaciones'))) ?>
			<?php echo form_textarea(array('name'=>'observaciones','value'=>set_value('observaciones'),'cols'=>'28','rows'=>'4')); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Cobrable'); ?>
			<span>SÍ</span>
			<?php echo form_checkbox('cobrable','1',FALSE); ?>

		</div>
	</div>
	<div>
		<?php echo form_submit(array('name'=>'agregar','id'=>'boton','value'=>'Agregar')); ?>
		<?php echo form_close(); ?>
	</div>
	
</div>

<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js'); ?>"></script>
<?php echo link_tag('css/flick/jquery-ui-1.8.18.custom.css');?>

<script type="text/javascript">
$('#numero').focus();

$('#zona').change(
	function () {
		var zona = $('#zona option:selected').val();
		$('[name="localidad"]').val('');
		$('[name="colonia"]').empty();
		if (zona != 0 ){
			$.ajax({
				url: "<?php echo site_url('peticiones_ajax/obtener_colonias');?>",
				async: false,
				type: "POST",
				data: "zona="+zona,
				dataType: "json",
				success: function (datos) {
					$('[name="colonia"]').append('<option value="0">Seleccionar</option>');
					$.each(datos, function (i, colonia) {
						var html = '<option value="'+colonia.id+'">'+colonia.nombre+"</option>";
						$('[name="colonia"]').append(html);
					});
				}
			});
		} else {
			$('#colonia').empty();
		}
	}
);
function generar_numero() {
	var prefijo =  $('#prefijo option:selected').val();
	if (prefijo == 0) {
		alert('Debe seleccionar las iniciales del contrato');
	} else{
		//alert('selecciono: ' + prefijo);
		datos = "prefijo="+prefijo;
		$.ajax({
			url: 		"<?php echo site_url('peticiones_ajax/generar_numero_contrato'); ?>",
			async: 		true,
			type: 		"POST",
			data: 		datos,
			dataType: 	"json",
			success: function (datos) {
				$('#numero').val(datos.numero);
			}
		});
	};
}

$('#generador').click(
	function () {
		generar_numero();
	}
);
$('#contrato [name="colonia"]').change(
	function () {
		var colonia = $('#contrato [name="colonia"]').val();
		$.ajax({
			url: "<?php echo site_url('obtener_colonias/obtener_loc');?>",
			async: false,
			type: "POST",
			data: "colonia="+colonia,
			dataType: "json",
			success: function (datos) {
				$('#localidad').val(datos.localidad);
				$('#codigopostal').val(datos.codigopostal);
			}
		});
	}
);

$('#numero').change(
	function () {
		var numero = $('#numero').val();
		if (numero.length > 2) {
			$.ajax({
				url: "<?php echo site_url('peticiones_ajax/contrato_existe'); ?>",
				async: false,
				type: "POST",
				data: "numero="+numero,
				dataType: "html",
				success: function (existe) {
					if (existe == 0) {
						$('#numero').removeClass( $('#numero').attr('class') );
						$('#numero').addClass('all-ok');
					} else {
						$('#numero').removeClass( $('#numero').attr('class') );
						$('#numero').addClass('error');
					}
				}
			});
		}
	}
);

$('#cancelar').click(
	function () {
		url = "<?php echo base_url(); ?>";
		document.location.href = url;
	}
);
</script>