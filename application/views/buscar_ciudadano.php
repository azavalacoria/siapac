<div id="head-sale">

	<?php echo form_input(array('name'=>'siteurl','value'=>site_url('busquedas_ajax'), 'type'=>'hidden','id'=>'siteurl'));?>
	<h3>Buscar ciudadano en el padrón</h3>
	<div><ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul></div>
	<?php echo form_open('nuevo_contribuyente'); ?>
	<div id="form-box">
		<div class="form-row">
			<div class="normal-row">
				<label>Apellido Paterno</label>
				<?php echo form_input(
					array('name'=>'apellidopaterno','id'=>'apellidopaterno','value'=>set_value('apellidopaterno'))
				);?>
			</div>
			<div class="normal-row">
				<label>Apellido Materno</label>
				<?php echo form_input(
					array('name'=>'apellidomaterno', 'id'=>'apellidomaterno','value'=>set_value('apellidomaterno'))
				);?>
			</div>
			<div class="normal-row">
				<label>Nombres</label>
				<?php echo form_input(
					array('name'=>'nombres','id'=>'nombres', 'value'=>set_value('nombres'))
				);?>
			</div>
		</div>
		<div>
			<?php //echo form_submit(array('name'=>'buscar','value'=>'Buscar','id'=>'submit')); ?>
			<?php echo form_button(array('name'=>'buscar','content'=>'Buscar','id'=>'buscar','class'=>'submit')); ?>
		</div>
	</div>
	
	<?php echo form_close(); ?>
</div>
<style type="text/css">
#report-box{display: none;}
</style>
<div id="report-box">
	<?php echo form_open('nuevo_contribuyente/seleccionar', array('id'=>'selecciona-ciudadano')); ?>
	<table id="table-grid">
	</table>
	<table>
		<tr>
			<td>
				<?php //echo form_button(array('name'=>'buscar','content'=>'Anteriores','id'=>'ant','class'=>'submit')); ?>
				<?php echo form_button(array('name'=>'buscar','content'=>'Anteriores','id'=>'ant','class'=>'submit')); ?>
			</td>
			<td>
				<?php echo form_button(array('name'=>'buscar','content'=>'Siguientes','id'=>'sig','class'=>'submit')); ?>
			</td>
			<td>
				<?php //echo form_submit(array('name'=>'seleccionar','value'=>'Seleccionar','id'=>'submit')); ?>
				<?php echo form_button(array('name'=>'seleccionar','content'=>'Seleccionar','id'=>'sel', 'class'=>'submit')); ?>
			</td>
		</tr>
	</table>
	<?php form_close(); ?>
</div>
<script type="text/javascript" src="<?php echo base_url('js/contribuyente.js') ?>"></script>
<script type="text/javascript">
$('#apellidopaterno').focus();
</script>