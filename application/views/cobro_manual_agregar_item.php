
<?php echo link_tag('css/jquery-ui.css'); ?>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js'); ?>"></script>
<div id="form-box">
	<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
	<?php echo form_open('cobro_parcial/agregar_item', array('id'=>'agregar_item'))?>
	<div>
	<?php echo form_label('Periodo Inicial') ?>
	<?php
	if (!isset($_POST['periodoinicial'])) {
		// echo form_input(array('name'=>'periodoinicial','id'=>'periodoinicial', 'readonly'=>TRUE,
		// 	'value'=>date_format(new DateTime($this->session->userdata('ultimopago')),'Y-m-d')));
		echo form_input(array('name'=>'periodoinicial','id'=>'periodoinicial', 
			'readonly'=>TRUE,'value'=> $this->session->userdata('periodoinicial')));
	} else {
		echo form_input(array('name'=>'periodoinicial','id'=>'periodoinicial','readonly'=>TRUE,'value'=>set_value('periodoinicial')));
	}
	?>
	</div>
	<div>
	<?php echo form_label('Periodo Final') ?>
	<?php echo form_input(array('name'=>'periodofinal','id'=>'periodofinal','readonly'=>TRUE,'value'=>set_value('periodofinal'))); ?>
	</div>
	<div>

		<?php echo br(3);?>
	<div style="float: left; padding-right: 14px">
		<?php echo form_button(array('name'=>'accion','id'=>'agregar','value'=>'agregar', 'content'=>'Agregar')) ?>
		<?php echo form_close(); ?>
	</div>
	<div>
		<?php echo form_open('cobro_parcial/no_agregar_item'); ?>
		<?php echo form_button(array('name'=>'cancelar','value'=>'cancelar','type'=>'submit','content'=>'Cancelar')) ?>
		<?php echo form_close(); ?>
	</div>
	</div>
	<script type="text/javascript">
	$('#agregar').click(
		function () {
			periodoinicial = Date.parse($('#periodoinicial').val());
			periodofinal = Date.parse($('#periodofinal').val());
			if (periodofinal >= periodoinicial) {
				$('#agregar').attr('type','submit');
				$('#agregar_item').submit();
			} else {
				alert('La fecha inicial es mayor a la final. Por favor verifique sus fechas');
			}
		}
		);
	</script>
</div>