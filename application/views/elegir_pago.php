<div id="head-sale">
	<table>
		<tr>
			<th>No. de Contrato</th>
			<th>Nombre Completo</th>
			<th>Domicilio</th>
			<th>Último Pago</th>
		</tr>
		<tr>
			<td><?php echo $this->session->userdata('numero'); ?></td>
			<td><?php echo $this->session->userdata('nombrecompleto'); ?></td>
			<td><?php echo $this->session->userdata('domicilio'); ?></td>
			<td><?php echo $this->session->userdata('ultimopago'); ?></td>
		</tr>
	</table>
	<?php echo br(4);?>
</div>
<div id="form-box">
	<?php echo form_open('cobrar/elegir_cobro'); ?>
	
	<?php echo form_button(array('name'=>'tipocobro','value'=>'parcial','type'=>'submit','content'=>'Cobro Parcial')); ?>
	<?php echo form_button(array('name'=>'tipocobro','value'=>'presupuesto','type'=>'submit','content'=>'Presupuesto')); ?>
	<?php echo form_close(); ?>
</div>