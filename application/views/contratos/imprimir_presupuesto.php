<div id="form-box">
	<div>
		<div class="form-row">
			<div class="normal-row">
				<label>Contribuyente</label>
				<?php echo form_textarea(array('name'=>'numero', 'value'=> $nombrecompleto, 'cols'=>'28','rows'=>'4')); ?>
			</div>
			<div class="normal-row">
				<label>Contrato</label>
				<?php echo form_input(array('name'=>'numero', 'value'=> $numero)); ?>
			</div>
			<div class="normal-row">
				<label>Último Pago</label>
				<?php echo form_input(array('name'=>'numero', 'value'=> $ultimopago)); ?>
			</div>
		</div>
		<div class="form-row">
			
			<div class="normal-row">
				<label>Domicilio</label>
				<?php echo form_textarea(array('name'=>'numero', 'value'=> $domicilio,'cols'=>'28','rows'=>'4')); ?>
			</div>
		</div>
	</div>
</div>
<div id="report-box">
	<table>
		<tr>
			<th>Nombre del Contribuyente</th>
			<td><?php echo $nombrecompleto; ?></td>
			<th>Contrato</th>
			<td><?php echo $numero; ?></td>
		</tr>
		<tr>
			<th>Último Pago</th>
			<td><?php echo $ultimopago; ?></td>
			<th>Domicilio del contrato</th>
			<td>Calle <?php echo $domicilio; ?></td>
		</tr>
	</table>
	<br><br><br><br>
	<table>
		<thead>
			<th>Periodo</th>
			<th>Cantidad</th>
			<th>Concepto</th>
			<th>Precio Unitario</th>
			<th>Importe</th>
		</thead>
		<tbody>
			<?php
			foreach ($items as $item) {
			?>
			<tr>
				<td><?php echo $item['periodo']; ?></td>
				<td><?php echo $item['cantidad']; ?></td>
				<td><?php echo $item['concepto']; ?></td>
				<td><?php printf("$ %.2f", $item['preciounitario']); ?></td>
				<td><?php printf("$ %.2f", $item['importe']); ?></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
	<div id="gran-total">
		<table>
			<tr>
				<th>Total</th>
				<td><td><?php printf("$ %.2f", $total); ?></td></td>
			</tr>
		</table>
	</div>
	
</div>