<div id="form-box">
	<script type="text/javascript">
	function obtener_zona() {
		var zona = $('#zonas :selected').val();
		if (zona == 0) {
			alert('Seleccione una zona válida');
		} else{
			obtener_colonias(zona);
		};
	}
	</script>
	<div id="ejem">
	</div>
	<div style="padding-top: 50px">
		<div class="form-row">
			<div class="normal-row">
				<?php
				echo form_label('Zona');
				$js = 'id="zonas" onChange="obtener_zona();"';
				echo form_dropdown('zona', $zonas, set_value('zona'), $js);
				?>
			</div>
			<div class="normal-row">
				<?php
				echo form_label('Calle');
				$colonias = array('0'=>'Seleccione');
				$js = 'id="colonia" onChange="obtener_contratos();"';
				echo form_dropdown('colonia', $colonias, set_value('colonia'), $js);
				?>
			</div>
		</div>
	</div>
	<div id="results-table">
		<table id="table-grid">
		</table>
		
	</div>
	<div style="padding-top: 30px">
		<button id="siguientes" type="button">Siguientes Resultados</button>
	</div>
	
	<style type="text/css">
	#set-date{display: none;}
	#results-table{display: table;}
	#table-grid tr:hover{background-color: #FF8585}
	</style>
</div>
<div id="set-date" style="padding-top: 30px">
		<span id="el-contribuyente"></span>
		<br><br>
		<span>Contrato: </span><span id="el-contrato"></span>
		<br><br>
		<span id="el-domicilio"></span>
		<br><br>
		<input type="text" id="periodoinicial" READONLY value="" />
		<button id="apply-update" type="button">Actualizar fecha pago</button>
	</div>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js'); ?>"></script>
<?php echo link_tag('css/flick/jquery-ui-1.8.18.custom.css');?>
<script type="text/javascript">

$('#siguientes').click(function () {
	enlistar_contratos();
});

$('.edit-item').click(function () {
	alert('hola');
});

function obtener_colonias (zona) {
	$.ajax({
		url: "<?php echo site_url('peticiones_ajax/obtener_calles');?>",
		async: false,
		type: "POST",
		data: "zona="+zona,
		dataType: "json",
		success: function (datos) {
			$('[name="colonia"]').empty();
			$('[name="colonia"]').append('<option value="0">Seleccionar</option>');
			$.each(datos, function (i, colonia) {
				var html = '<option value="'+colonia.id+'">'+colonia.nombre+"</option>";
				$('[name="colonia"]').append(html);
			});
			
		}
	});
}

$('#apply-update').click(function () {
	var ultimopago = $('#periodoinicial').val();
	var numero = $('#el-contrato').html();
	$.ajax({
		url: "<?php echo site_url('peticiones_ajax/actualizar_ultimo_pago_contrato');?>",
		async: false,
		type: "POST",
		data: {ultimopago: ultimopago, numero: numero},
		dataType: "json",
		success: function (datos) {
			
		}
	});
	reenlistar_contratos();
});

function obtener_contratos () {
	$('#table-grid').empty();
	enlistar_contratos();
}

function enlistar_contratos () {
	var colonia = $('#colonia :selected').val();
	var limite = $('#table-grid tr').size();
	$.ajax({
		url: "<?php echo site_url('peticiones_ajax/listar_contratos_colonias_up');?>",
		async: false,
		type: "POST",
		data: {colonia: colonia, limite: limite},
		dataType: "json",
		success: function (datos) {
			$('#table-grid').empty();
			$('#table-grid').append("<tr>"+"<th>Número Contrato</th><th>Nombres</th><th>Apellido Paterno</th>"
				+"<th>Apellido Materno</th><th>Dirección</th>"+ "<th>Último Pago</th>"+"<th>Editar</th></tr>");
			$.each(datos, function (i, contrato) {
				var html = 
					'<tr>' + 
						'<td>' + contrato.numero + '</td>' +
						'<td>' + contrato.nombres + '</td>' + 
						'<td>' + contrato.apellidopaterno + '</td>' +
						'<td>' + contrato.apellidomaterno + '</td>' +
						'<td>' + contrato.direccion + '</td>' +
						'<td>' + contrato.ultimopago + '</td>' +
						'<td class="centered">' + '<input type="button" id="' + 
						contrato.numero+'" onClick="actualizar(this.id);">' +'</td>' +
					'</tr>';
				$('#table-grid').append(html);
			});
		}
	});
}

function reenlistar_contratos() {
	var colonia = $('#colonia :selected').val();
	//var limite = $('#table-grid tr').size();
	$.ajax({
		url: "<?php echo site_url('peticiones_ajax/relistar_contratos_colonias');?>",
		async: false,
		type: "POST",
		data: {colonia: colonia},
		dataType: "json",
		success: function (datos) {
			$('#table-grid').empty();
			$('#table-grid').append("<tr>"+"<th>Número Contrato</th><th>Nombres</th><th>Apellido Paterno</th>"
				+"<th>Apellido Materno</th><th>Dirección</th>"+ "<th>Último Pago</th>"+"<th>Editar</th></tr>");
			$.each(datos, function (i, contrato) {
				var html = 
					'<tr>' + 
						'<td>' + contrato.numero + '</td>' +
						'<td>' + contrato.nombres + '</td>' + 
						'<td>' + contrato.apellidopaterno + '</td>' +
						'<td>' + contrato.apellidomaterno + '</td>' +
						'<td>' + contrato.direccion + '</td>' +
						'<td>' + contrato.ultimopago + '</td>' +
						'<td class="centered">' + '<input type="button" id="' + 
						contrato.numero+'" onClick="actualizar(this.id);">' +'</td>' +
					'</tr>';
				$('#table-grid').append(html);
				$('#set-date').fadeOut(500);
				$('#form-box').fadeIn(500);
			});
		}
	});
}

function actualizar (numero) {
	//alert(numero);
	console.log(numero);
	$('#form-box').fadeOut(500);
	$('#set-date').fadeIn(500);
	obtener_datos_contrato(numero);
}

function obtener_datos_contrato (numero) {
	$.ajax({
		url: "<?php echo site_url('peticiones_ajax/obtener_datos_contrato');?>",
		async: false,
		type: "POST",
		data: {numero: numero},
		dataType: "json",
		success: function (datos) {
			$('#el-contribuyente').html(datos.nombre);
			$('#el-contrato').html(datos.contrato)
			$('#el-domicilio').html(datos.domicilio);
			$('#periodoinicial').val(datos.ultimopago);
		}
	});
}
</script>