<?php header("Content-type: text/xml", "charset=utf-8"); ?>
<?php echo '<?'.'xml version="1.0" standalone="yes" '.'?>'; ?>

<contribuyentes>
<?php $c=0; ?>
<?php $cc = 0; ?>
<?php foreach ($contribuyentes as $contribuyente) { ?>
	<contribuyente>
		<id>
			<?php
			$c++;
			echo $c; 
			?>
		</id>
		<clave_elector><?php echo $contribuyente['claveelector']; ?></clave_elector>
		<nombres><?php echo $contribuyente['nombres']; ?></nombres>
		<apellido_paterno><?php echo $contribuyente['apellidopaterno']; ?></apellido_paterno>
		<apellido_materno><?php echo $contribuyente['apellidomaterno']; ?></apellido_materno>
		<fecha_nacimiento><?php echo $contribuyente['fechanacimiento']; ?></fecha_nacimiento>
		<loc><?php echo $contribuyente['localidad']; ?></loc>
		<colonia><?php echo $contribuyente['colonia']; ?></colonia>
		<calle><?php echo $contribuyente['calle']; ?></calle>
		<numero><?php echo $contribuyente['numero']; ?></numero>
		<telefono><?php echo $contribuyente['telefono']; ?></telefono>
		<celular><?php echo $contribuyente['celular']; ?></celular>
		<email><?php echo $contribuyente['email']; ?></email>
		<rfc><?php echo $contribuyente['rfc']; ?></rfc>
		<?php if (sizeof($contribuyente['contratos']) > 0) { ?>
		<contratos>
			
			<?php foreach ($contribuyente['contratos'] as $contrato) {	?>
			<contrato>
				<?php $cc++; ?>
				<contador><?php echo $cc ?></contador>
				<numero><?php echo $contrato['numero']; ?></numero>
				<medidor><?php echo $contrato['medidor']; ?></medidor>
				<loc><?php echo $contrato['loc']; ?></loc>
				<colonia><?php echo $contrato['colonia']; ?></colonia>
				<calle><?php echo $contrato['calle']; ?></calle>
				<numero_exterior><?php echo $contrato['numeroexterior']; ?></numero_exterior>
				<numero_interior><?php echo $contrato['numerointerior']; ?></numero_interior>
				<referencias><?php echo $contrato['referencias']; ?></referencias>
				<codigo_postal><?php echo $contrato['codigopostal']; ?></codigo_postal>
				<fecha><?php echo $contrato['fecha']; ?></fecha>
				<observaciones><?php echo $contrato['observaciones']; ?></observaciones>
				<latitud><?php echo $contrato['latitud']; ?></latitud>
				<longitud><?php echo $contrato['longitud']; ?></longitud>
				<ultimo_pago><?php echo $contrato['ultimopago']; ?></ultimo_pago>
				<zona><?php echo $contrato['zona']; ?></zona>
				<tipo_de_servicio><?php echo $contrato['tipodeservicio']; ?></tipo_de_servicio>
			</contrato>
			<?php } ?>
		</contratos>
		<?php } ?>
	</contribuyente>
<?php } ?>
</contribuyentes>
