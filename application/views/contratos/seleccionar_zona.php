<div id="form-box">
	<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js'); ?>"></script>
	<?php echo link_tag('css/flick/jquery-ui-1.8.18.custom.css');?>
	<div>
		<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
	</div>
	<?php echo form_open('contratos/exportar_contratos/obtener'); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Zona'); ?>
			<?php echo form_dropdown('zona',$zonas, set_value('zona')); ?>
		</div>
	</div>
	<div>
		<div class="form-row">
			<div class="normal-row">
				<?php echo form_label('Fecha Inicial'); ?>
				<?php echo form_input(
					array('name'=>'periodoinicial','value'=>set_value('periodoinicial'),
						'id'=>'periodoinicial','READONLY'=>TRUE)
					); ?>
			</div>
			<div class="normal-row">
				<?php echo form_label('Fecha Final'); ?>
				<?php echo form_input(
					array('name'=>'periodofinal','value'=>set_value('periodofinal'),
						'id'=>'periodofinal','READONLY'=>TRUE)
					); ?>
			</div>
		</div>
	</div>
	<div>
		<?php echo form_submit(array('name'=>'obtener','value'=>'Obtener Contratos','class'=>'submit')); ?>
	</div>
	<?php echo form_close(); ?>
</div>