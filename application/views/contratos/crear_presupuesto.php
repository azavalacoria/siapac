<?php echo link_tag('css/jquery-ui.css'); ?>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js'); ?>"></script>
<div id="form-box">
	<div>
		<div class="form-row">
			<div class="normal-row">
				<label>Contribuyente</label>
				<?php echo form_textarea(array('name'=>'numero', 'value'=> $nombrecompleto, 'cols'=>'28','rows'=>'4')); ?>
			</div>
			<div class="normal-row">
				<label>Contrato</label>
				<?php echo form_input(array('name'=>'numero', 'value'=> $numero)); ?>
			</div>
			<div class="normal-row">
				<label>Último Pago</label>
				<?php echo form_input(array('name'=>'numero', 'value'=> $ultimopago)); ?>
			</div>
		</div>
		<div class="form-row">
			
			<div class="normal-row">
				<label>Domicilio</label>
				<?php echo form_textarea(array('name'=>'numero', 'value'=> $domicilio,'cols'=>'28','rows'=>'4')); ?>
			</div>
		</div>
	</div>
	<?php echo form_open('contratos/crear_presupuesto/visualizar', array('id'=>'crear-presupuesto')) ?>
	<div>
		<div class="normal-row">
			<div class="normal-row">
			<label>Periodo Inicial</label>
			<?php
				if (isset($periodoinicial)) {
					echo form_input(array('name'=>'periodoinicial','id'=>'periodoin', 'READONLY'=>TRUE,'value'=>$periodoinicial));
				} else {
					echo form_input(array('name'=>'periodoinicial','id'=>'periodoin','READONLY'=>TRUE,'value'=>set_value('periodoinicial')));
				}
				
			?>
			</div>
		</div>
		<div class="normal-row">
			<label>Periodo Final</label>
			<?php
				echo form_input(array('name'=>'periodofinal','id'=>'periodofinal','READONLY'=>TRUE,'value'=>set_value('periodofinal')));
			?>
		</div>
		<div>
			<?php echo form_button(array('name'=>'crear','value'=>'crear','id'=>'crear','class'=>'submit','content'=>'Generar Presupuesto')); ?>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>

<script type="text/javascript">
$('#crear').click(function () {
	periodoinicial = Date.parse($('#periodoin').val());
	periodofinal = Date.parse($('#periodofinal').val());
	if (periodofinal >= periodoinicial) {
		$('#crear').attr('type','submit');
		$('#crear-presupuesto').submit();
	} else {
		alert('La fecha inicial es mayor a la final. Por favor verifique sus fechas');
	}
});
</script>