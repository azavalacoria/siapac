<div id="report-box">
	<table>
		<thead>
			<th>Contrato</th>
			<th>Contribuyente</th>
			<th>Dirección</th>
			<th>Tipo de Servicio</th>
			<th>Último Pago</th>
		</thead>
		<tbody>
			<?php
			foreach ($contratos as $contrato) {
			?>
			<tr>
				<td><?php echo $contrato['numerocontrato']; ?></td>
				<td><?php echo $contrato['nombres']." ".$contrato['apellidopaterno']." ".$contrato['apellidomaterno']; ?></td>
				<td><?php echo $contrato['colonia']." # ".$contrato['numero']; ?></td>
				<td><?php echo $contrato['servicio']; ?></td>
				<td><?php echo $contrato['ultimopago']; ?></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
	<div>
		<?php
		echo form_open('contratos/exportar_contratos/generar_archivo');
		echo form_hidden('periodoinicial', $periodoinicial);
		echo form_hidden('periodofinal', $periodofinal);
		echo form_hidden('zona', $zona);
		echo form_submit(array('name'=>'generar','value'=>'Generar Archivo','class'=>'submit'));
		echo form_close();
		?>
	</div>
</div>