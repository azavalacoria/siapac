<h3>Nuevo Contribuyente</h3>
<div id="form-box">
<hr>
<div>
	<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
</div>
<?php echo link_tag('css/jquery-ui.css'); ?>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.fechanacimiento.js'); ?>"></script>

<?php echo form_open('nuevo_contribuyente/agregar', array('id'=>'contribuyente','name'=>'contribuyente')) ?>
	<?php foreach ($contribuyente as $c) { ?>
	
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label("Nombres"); ?>
			<?php if (!isset($_POST['nombres'])) {
				echo form_input(array('name'=>'nombres', 'id'=>'nombres', 'READONLY'=>TRUE, 'value'=>$c->nombre));
			} else {
				echo form_input(array('name'=>'nombres', 'id'=>'nombres', 'READONLY'=>TRUE, 'value'=>set_value('nombres')));
			} ?> 
		</div>
		<div class="normal-row">
			<?php echo form_label('Apellido Paterno'); ?>
			<?php echo form_input(array('name'=>'apellidopaterno', 'READONLY'=>TRUE, 'value'=>$c->apellidopaterno)); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Apellido Materno'); ?>
			<?php echo form_input(array('name'=>'apellidomaterno', 'READONLY'=>TRUE, 'value'=>$c->apellidomaterno)); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Fecha Nacimiento'); ?>
			<?php echo form_input(array('name'=>'fechanacimiento','value'=>$c->fec_nac,'id'=>'fechanacimiento')); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('RFC'); ?>
			<?php echo form_input(array('name'=>'rfc', 'value'=>set_value('rfc'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Clave Elector');?>
			<?php if ( !isset( $_POST['claveelector'] ) ) {
				echo form_input(array('name'=>'claveelector', 'READONLY'=>TRUE, 'value'=>$c->cve_elect));
			} else {
				echo form_input(array('name'=>'claveelector', 'READONLY'=>TRUE, 'value'=>set_value('claveelector')));
			}?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Colonia'); ?>
			<?php echo form_input(array('name'=>'colonia','value'=>$c->colonia)); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Localidad'); ?>
			<?php echo form_input(array('name'=>'localidad','value'=>$c->local)); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Calle - Número'); ?>
			<?php echo form_input(array('name'=>'calle','id'=>'calle','value'=>$c->calle)); ?>
			<?php echo form_input(array('name'=>'numero','id'=>'num-int','value'=>$c->num_a)); ?>
		</div>
	</div>
	<?php } ?>

	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Núm telefónico'); ?>
			<?php echo form_input(array('name'=>'telefono', 'value'=>set_value('telefono'))) ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Celular'); ?>
			<?php echo form_input(array('name'=>'celular', 'value'=>set_value('celular'))) ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Correo electrónico'); ?>
			<?php echo form_input(array('name'=>'email', 'value'=>set_value('email'))) ?>
		</div>
	</div>
	<?php echo form_button(array('name'=>'agregar','content'=>'Agregar','id'=>'submit')); ?>
	<?php echo form_close(); ?>
<hr>
<script>
$('#calle').attr('style','width: 150px');
$('#num-int').attr('style','width: 50px');
$('#nombres').focus();
$(function () {
	$('#submit').click(function () {
		confirmacion = confirm("¿Desea agregar el contribuyente?");
		if (confirmacion) {
			$('#submit').attr('type','submit');
			$('#contribuyente').submit();

		} else {
			$('#list-errors').append('<li>Cancelado por el usuario</li>');
		}
		
	});
});
</script>
</div>
