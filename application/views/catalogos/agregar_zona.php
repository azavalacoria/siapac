<div id="form-box">
	<ul id="list-errors">
		<?php if(isset($error)) { ?>
			<li><?php echo $error; ?></li>
		<?php } ?>
		<?php echo validation_errors('<li>','</li>'); ?>
	</ul>
	<h3>Agregar nueva zona</h3>
	<?php echo form_open('catalogos/agregar_zona'); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Nombre de la zona'); ?>
			<?php echo form_input(array('name'=>'nombre','value'=>set_value('nombre'))); ?>
		</div>
		<div>
			<?php echo form_button(array('name'=>'agregar','content'=>'Agregar','id'=>'submit')); ?>
		</div>
	<?php echo form_close(); ?>
	</div>
	<script>
		$(function () {
			$('#submit').click(function () {
				confirmacion = confirm("¿Desea agregar la nueva zona?");
				if (confirmacion) {
					$('#submit').attr('type','submit');
					$('#contribuyente').submit();	
				} else {
					$('#list-errors').html('<li>Cancelado por el usuario</li>');
				}
				
			});
		});
</script>
</div>