<ul id="list-errors"><?php echo validation_errors('<li>','</li>') ?></ul>
<h3>Editar datos del contrato</h3>
<?php echo form_open('catalogos/editar_contrato',array('id'=>'contrato')) ?>


<?php echo form_input(array('name'=>'idcontrato','type'=>'hidden','value'=>set_value('idcontrato'))); ?>
<div class="form-row">
	<div class="normal-row">
		<?php echo form_label('Nombre del contribuyente'); ?>
		<?php
		echo form_input(array('name'=>'contribuyente', 'value' => $this->session->userdata('nombrecontribuyente')));
		?>
	</div>
</div>
<div class="form-row">
	<div class="normal-row">
		<?php echo form_label('Número de contrato'); ?>
		<?php echo form_input(array('name' => 'numero', 'value' =>set_value('numero'), 'id'=>'numero','READONLY'=>TRUE)); ?>
	</div>
	<div class="normal-row">
		<?php echo form_label('Medidor'); ?>
		<?php echo form_input(array('name'=>'medidor','value'=>set_value('medidor') )); ?>
	</div>
	<div class="normal-row">
		<?php echo form_label('Tipo de servicio'); ?>
		<?php echo form_dropdown('tiposervicio',$servicios, set_value('tiposervicio')); ?>
	</div>
</div>
<div class="form-row">
	<div class="normal-row">
		<?php echo form_label('Calle'); ?>
		<?php echo form_input(array('name'=>'calle', 'value'=>set_value('calle') )); ?>
	</div>
	<div class="normal-row">
		<?php echo form_label('Número exterior'); ?>
		<?php echo form_input(array('name'=>'numeroexterior', 'value'=>set_value('numeroexterior') )); ?>
	</div>
	<div class="normal-row">
		<?php echo form_label('Número interior'); ?>
		<?php echo form_input(array('name'=>'numerointerior', 'value'=>set_value('numerointerior') )); ?>
	</div>
</div>
<div class="form-row">
	<div class="normal-row">
		<?php echo form_label('Zona'); ?>
		<?php $js ='id="zona" onChange="cambiar_zona()"'; ?>
		<?php echo form_dropdown('zona',$zonas, set_value('zona'), $js); ?>
	</div>
	<div class="normal-row">
		<?php echo form_label('Colonia'); ?>
		<?php echo form_dropdown('colonia',$colonias, set_value('colonia')); ?>
	</div>
	<div class="normal-row">
		<?php echo form_label('Localidad'); ?>
		<?php echo form_input(array('name'=>'localidad', 'value'=>set_value('localidad'), 'id'=>'localidad')); ?>
	</div>
</div>
<div class="form-row">
	<div class="normal-row">
		<?php echo form_label('Código Postal'); ?>
		<?php echo form_input(array('name'=>'codigopostal', 'value'=>set_value('codigopostal'), 'id'=>'codigopostal')); ?>
	</div>
	<div class="normal-row">
		<?php echo form_label('Referencias'); ?>
		<?php echo form_textarea(array('name'=>'referencias', 'value' => set_value('referencias'))); ?>
	</div>
</div>
<div class="form-row">
	<div class="normal-row">
		<?php echo form_label('Último pago'); ?>
		<?php echo form_input(array('name'=>'ultimopago','value'=>set_value('ultimopago'), 'id'=>'periodoinicial', 'READONLY'=>TRUE)); ?>
	</div>
	<div class="normal-row">
		<?php echo form_label('Observaciones'); ?>
		<?php echo form_textarea(array('name'=>'observaciones','value'=>set_value('observaciones'))); ?>
	</div>
</div>
<div>
	<?php
	echo form_button(array('name'=>'editar', 'value'=>'editar','id'=>'editar', 'content'=>'Aplicar cambios','class'=>'submit'));
	?>
</div>

<?php echo form_close(); ?>
</div>

<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js'); ?>"></script>
<?php echo link_tag('css/flick/jquery-ui-1.8.18.custom.css');?>

<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js'); ?>"></script>
<?php echo link_tag('css/flick/jquery-ui-1.8.18.custom.css');?>

<script type="text/javascript">
$('#numero').focus();

function cambiar_zona() {
	var zona = $('#contrato [name="zona"] option:selected').val();
	$('#localidad').val('');
	$('#codigopostal').val('');
	$('[name="colonia"]').empty();

	if (zona == 0 ){
		$('#list-errors').append('<li>Debe seleccionar una zona válida. </li>');
		$('#contrato [name="colonia"]')
		$('#contrato [name="colonia"]').html('<option value="0">Seleccionar</option>');
	} else if (zona != 0 ){
		$.ajax({
			url: "<?php echo site_url('peticiones_ajax/obtener_colonias');?>",
			async: false,
			type: "POST",
			data: "zona="+zona,
			dataType: "json",
			success: function (datos) {
				$('[name="colonia"]').append('<option value="0">Seleccionar</option>');
				$.each(datos, function (i, colonia) {
					var html = '<option value="'+colonia.id+'">'+colonia.nombre+"</option>";
					$('[name="colonia"]').append(html);
				});
			}
		});
	}
}

$('#contrato [name="colonia"]').change(
	function () {
		var colonia = $('#contrato [name="colonia"]').val();
		$.ajax({
			url: "<?php echo site_url('obtener_colonias/obtener_loc');?>",
			async: false,
			type: "POST",
			data: "colonia="+colonia,
			dataType: "json",
			success: function (datos) {
				$('#localidad').val(datos.localidad);
				$('#codigopostal').val(datos.codigopostal);
			}
		});
	}
);

$('#numero').change(
	function () {
		var numero = $('#numero').val();
		if (numero.length > 2) {
			$.ajax({
				url: "<?php echo site_url('peticiones_ajax/contrato_existe'); ?>",
				async: false,
				type: "POST",
				data: "numero="+numero,
				dataType: "html",
				success: function (existe) {
					if (existe == 0) {
						$('#numero').removeClass( $('#numero').attr('class') );
						$('#numero').addClass('all-ok');
					} else {
						$('#numero').removeClass( $('#numero').attr('class') );
						$('#numero').addClass('error');
					}
				}
			});
		}
	}
);

$('#cancelar').click(
	function () {
		url = "<?php echo base_url(); ?>";
		document.location.href = url;
	}
);

$('#editar').click(
	function () {
		zona = $('#zona').val();
		colonia = $('#contrato [name="colonia"] option:selected').val();
		tiposervicio = $('#tiposervicio').val();
		if (zona != 0 && colonia > 1 && tiposervicio !=0  ) {
			$('#editar').attr('type','submit');
			$('#contrato').submit();
		} else {
			$('#list-errors').html('');
			if (tiposervicio == 0) {
				$('#list-errors').append('<li>Debe seleccionar una tipo de servicio válido.</li>');	
			};
			if (zona == 0) {
				$('#list-errors').append('<li>Debe seleccionar una zona válida. </li>');
			};
			if (colonia <= 0 || !colonia ) {
				$('#list-errors').append('<li>Debe seleccionar una colonia válida. </li>');	
			};
		}
	}
);

function cambiar_tipo_servicio () {
	tiposervicio = $('#tiposervicio').val();
	if (tiposervicio == 0) {
		$('#list-errors').append('<li>Debe seleccionar una tipo de servicio válido.</li>');	
	};
}
</script>