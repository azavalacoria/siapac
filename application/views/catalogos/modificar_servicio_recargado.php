<div id="form-box">
	<div>
		<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
	</div>
	<?php echo form_open('catalogos/modificar_servicio/actualizar') ?>
	<div class="form-row">

			<div class="normal-row">
				<label>Nombre:</label>
				<?php echo form_input(array('name'=>'nombre', 'value'=>set_value('nombre'))); ?>
			</div>
			<div class="normal-row">
				<label>Descripcion</label>
				<?php echo form_input(array('name'=>'descripcion', 'value'=>set_value('descripcion'))); ?>
			</div>
			<div class="normal-row">
				<label>Cobro mensual <?php echo $this->input->post('cobromensual') ?></label>
				<?php
				if (set_value('cobromensual') ==  1) {
					echo form_checkbox('cobromensual', '1' , TRUE);
				} else {
					echo form_checkbox('cobromensual', '1' , FALSE);
				}
				?>
			</div>
		<div>
			<?php echo form_submit(array('name'=>'actualizar','value'=>'Actualizar','id'=>'submit')); ?>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>