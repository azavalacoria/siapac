<div id="form-box">
	<ul id="list-errors">
		<?php if(isset($error)) { ?>
			<li><?php echo $error; ?></li>
		<?php } ?>
		<?php echo validation_errors('<li>','</li>'); ?>
	</ul>
	<h3>Agregar nueva bonificación</h3>
	<?php echo form_open('catalogos/agregar_descuento'); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Descripción'); ?>
			<?php echo form_input(array('name'=>'descripcion', 'id' =>'descripcion', 'value'=>set_value('descripcion'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Porcentaje'); ?>
			<?php echo form_input(array('name'=>'porcentaje', 'value'=>set_value('porcentaje'),'id'=>'porcentaje')); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Evidencia'); ?>
			<?php echo form_textarea(array('name'=>'evidencia','value'=>set_value('evidencia'),'cols'=>'28','rows'=>'4')); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Activo'); ?>
			<?php
			if (isset($_POST['activo'])) {
				echo form_checkbox('activo','1', TRUE);
			} else {
				echo form_checkbox('activo','1', FALSE);
			}
			?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Aplicar a monto total'); ?>
			<?php
			if (isset($_POST['total'])) {
				echo form_checkbox('total','1', TRUE);
			} else {
				echo form_checkbox('total','1', FALSE);
			}
			?>
		</div>
	</div>

	<div>
		<?php echo form_submit(array('name'=>'agregar','value'=>'Agregar','class'=>'boton')); ?>
	</div>
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
$('#descripcion').focus();

$('#total').click(function () {
	//var total = $('#porcentaje').val();
	ch = $('#total').is(':checked');
	if (ch) {
		$('#porcentaje').attr('value','100');
		$('#porcentaje').attr('readonly', true);
	} else {
		$('#porcentaje').removeAttr("readonly");
		$('#porcentaje').attr('readonly',false);
		$('#porcentaje').attr('value', '0');
	};
});
</script>