<div id="form-box"> 
	<div>
		<ul id="list-errors"><?php echo validation_errors('<li>','</li>') ?></ul>
	</div>
	<?php echo form_open($destino) ?>
	<div class="form-row">
		<div class="normal-row">
			<label>Apellido Paterno</label>
			<?php echo form_input(array( 'name'=>'apellidopaterno','id'=>'apellidopaterno','value'=>set_value('apellidopaterno') )); ?>
		</div>
		<div class="normal-row">
			<label>Apellido Materno</label>
			<?php echo form_input(array( 'name'=>'apellidomaterno','value'=>set_value('apellidomaterno') )); ?>
		</div>
		<div class="normal-row">
			<label>Nombres</label>
			<?php echo form_input(array( 'name'=>'nombres','value'=>set_value('nombres') )); ?>
		</div>
	</div>
	<div class="normal-row">
		<?php echo form_submit(array('name'=>'buscar','value'=>'Buscar','id'=>'submit')) ?>
	</div>
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
$('#apellidopaterno').focus();
</script>