<div id="report-box">
	<?php echo form_open($destino) ?>
	<table>
		<thead>
			<th>Opción</th>
			<th>Nombre</th>
			<th>Descripción</th>
		</thead>
		<tbody>
			<?php foreach ($servicios as $servicio) { ?>
				<tr>
					<td>
<?php echo form_radio(array('name'=>'id','value'=>$servicio->idservicio,'group'=>'servicios')); ?>
					</td>
					<td><?php echo $servicio->nombreservicio; ?></td>
					<td><?php echo $servicio->descripcion; ?></td>
				</tr>
			<?php }	?>
		</tbody>
	</table>
	<div>
		<?php echo form_submit(array('name'=>'elegir','value'=>'Seleccionar','id'=>'submit')) ?>
	</div>
	<?php echo form_close(); ?>
</div>