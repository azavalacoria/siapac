<div id="form-box">
	<?php echo form_open('catalogos/modificar_servicio/actualizar') ?>
	<div class="form-row">
		<?php foreach ($servicio as $s) { ?>
			<div class="normal-row">
				<label>Nombre:</label>
				<?php echo form_input(array('name'=>'nombre','value'=>$s->nombreservicio)); ?>
			</div>
			<div class="normal-row">
				<label>Descripcion</label>
				<?php echo form_input(array('name'=>'descripcion','value'=>$s->descripcion)); ?>
			</div>
			<div class="normal-row">
				<label>Cobro mensual</label>
				<?php
				if ($s->cobromensual == 1) {
					echo form_checkbox('cobromensual', $s->cobromensual , TRUE);
				} else {
					echo form_checkbox('cobromensual', '1' , FALSE);
				}
				?>
			</div>
		<?php } ?>
		<div>
			<?php echo form_submit(array('name'=>'actualizar','value'=>'Actualizar','id'=>'submit')); ?>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>