<style type="text/css">
	#ncontrato {
		border: 0;
		background-color: transparent;
	}
	#txt_motivo::-webkit-input-placeholder{
		text-align: center;
		font-size: 20px;
		padding: 20px;
	}
	#txt_motivo::-moz-placeholder{
		text-align: center;
		font-size: 20px;
		padding: 20px;
	}
	#txt_motivo{
		text-align: left;
		font-size: 15px;
		width: 400px;
		height: 120px;
		border: 3px solid #cccccc;
		border-radius: 10px;
		outline:0px;
		padding: 5px;
		font-family: Tahoma, sans-serif;
		background-image: url(bg.gif);
		background-position: bottom right;
		background-repeat: no-repeat;
	}
	.modalDialog {
		position: fixed;
		font-family: Arial, Helvetica, sans-serif;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		background: rgba(0,0,0,0.8);
		z-index: 99999;
		opacity:0;
		-webkit-transition: opacity 400ms ease-in;
		-moz-transition: opacity 400ms ease-in;
		transition: opacity 400ms ease-in;
		pointer-events: none;
	}
	.modalDialog:target {
		opacity:1;
		pointer-events: auto;
	}

	.modalDialog > div {
		width: 400px;
		position: relative;
		margin: 10% auto;
		padding: 5px 20px 13px 20px;
		border-radius: 10px;
		background: #fff;
		background: -moz-linear-gradient(#fff, #999);
		background: -webkit-linear-gradient(#fff, #999);
		background: -o-linear-gradient(#fff, #999);
	}
	.close {
		background: #606061;
		color: #FFFFFF;
		line-height: 25px;
		position: absolute;
		right: -12px;
		text-align: center;
		top: -10px;
		width: 24px;
		text-decoration: none;
		font-weight: bold;
		-webkit-border-radius: 12px;
		-moz-border-radius: 12px;
		border-radius: 12px;
		-moz-box-shadow: 1px 1px 3px #000;
		-webkit-box-shadow: 1px 1px 3px #000;
		box-shadow: 1px 1px 3px #000;
	}

	.close:hover { background: red; }
</style>
<div id="form-box">
	
	<?php foreach ($contrato as $c) { ?>
	<div class="form-row">
		<?php echo form_open('catalogos/imprimir_contrato',array('id'=>'contrato-form')) ?>
		<div class="normal-row">
			<label>Número de Contrato</label>
			<span><?php echo form_input(array('name'=>'numero','READONLY'=>TRUE,'value'=>$c->numero,'id'=>'ncontrato')); ?></span>
		</div>
		<div class="normal-row">
			<label>Medidor</label>
			<span><?php echo $c->medidor; ?></span>
		</div>
		<div class="normal-row">
			<label></label>
			<span><?php echo form_submit('imprimir','Reimprimir Contrato'); ?></span>
		</div>
		<?php echo form_close(); ?>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<label>Colonia</label>
			<span><?php echo $c->nombrecolonia; ?></span>
		</div>
		<div class="normal-row">
			<label>Calle</label>
			<span><?php echo $c->calle ?></span>
		</div>
		<div class="normal-row">
			<label>Número</label>
			<span><?php printf("%s - %s", $c->numeroexterior, $c->numerointerior);  ?></span>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<label>Código Postal</label>
			<span><?php echo $c->codigopostal; ?></span>
		</div>
		<div class="normal-row">
			<label>Referencias</label>
			<span><?php echo $c->referencias; ?></span>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<label>Último Pago</label>
			<span><?php echo $c->ultimopago; ?></span>
		</div>
		<div class="normal-row">
			<label>Zona</label>
			<span><?php echo $c->nombrezona; ?></span>
		</div>
		<div class="normal-row">
			<label>Tipo de Servicio</label>
			<span><?php echo $c->nombreservicio; ?></span>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<label>Observaciones</label>
			<span><?php echo $c->observaciones ?></span>
		</div>
	</div>
	<?php } ?>
	<div>
		<button id="editar">Editar Contrato</button>
		<a href="#Modal_motivo_baja"><button id="baja">Baja de Contrato</button></a>
	</div>

	<div id="Modal_motivo_baja" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<h2>Motivo de baja del contrato</h2>
			<textarea id="txt_motivo"></textarea><br><br>
			<button id="_baja" style="margin:0 auto;">Dar de baja</button>
		</div>
	</div>

	<script type="text/javascript">
	$('#editar').click(
		function () {
			confirmacion = confirm("¿Desea editar este contrato?");
			if (confirmacion) {
				editar($('#ncontrato').val());
			}
		}
	);

	function editar (numerocontrato) {
		$('#form-box').empty();
		$.ajax({
			url: "<?php echo site_url('peticiones_ajax/obtener_contrato'); ?>",
			async: false,
			type: "POST",
			data: "contrato="+numerocontrato,
			dataType: "HTML",
			success: function (datos) {
				$('#form-box').html(datos);
			}
		});
	}

	$('#_baja').click(
		function () {
			var motivo = $($.trim('#txt_motivo')).val();
			if (motivo == ''){
				$('#txt_motivo').css({'border':'3px solid red'});
            	$('#txt_motivo').attr('placeholder', 'Motivo requerido');
			}else{
				confirmacion = confirm("¿Está seguro de querer dar de baja este contrato?");
				if (confirmacion) {
					baja($('#ncontrato').val(),motivo);
				}
			};
			
		}
	);

	function baja (numerocontrato,motivo) {
		$('#form-box').empty();
		$.ajax({
			url: "<?php echo site_url('catalogos/baja_contrato'); ?>",
			async: false,
			type: "POST",
			data: {
				contrato: numerocontrato,
				motivo: motivo
			},
			dataType: "HTML",
			success: function (datos) {
				$('#form-box').html(datos);
			}
		});
	}
	</script>
</div>
