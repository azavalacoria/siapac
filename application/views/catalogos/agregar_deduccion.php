<div id="form-box">
	<ul id="list-errors">
		<?php if(isset($error)) { ?>
			<li><?php echo $error; ?></li>
		<?php } ?>
		<?php echo validation_errors('<li>','</li>'); ?>
	</ul>
	<h3>Agregar nueva tarifa</h3>
	<?php echo form_open('catalogos/agregar_deduccion'); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Descripción'); ?>
			<?php echo form_input(array('name'=>'descripcion', 'id' =>'descripcion', 'value'=>set_value('descripcion'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Porcentaje'); ?>
			<?php echo form_input(array('name'=>'porcentaje', 'value'=>set_value('porcentaje'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Evidencia'); ?>
			<?php echo form_textarea(array('name'=>'evidencia','value'=>set_value('evidencia'),'cols'=>'28','rows'=>'4')); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Aplicar descuento'); ?>
			<?php echo form_checkbox('activo','activo', TRUE); ?>
		</div>
	</div>
	<div>
		<?php echo form_submit(array('name'=>'agregar','value'=>'Agregar','class'=>'boton')); ?>
	</div>
	<?php echo form_close(); ?>
</div>
<script type="text/javascript">
$('#descripcion').focus();
</script>