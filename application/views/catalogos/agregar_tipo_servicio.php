<div id="form-box">
	<ul id="list-errors">
		<?php if(isset($error)) { ?>
			<li><?php echo $error; ?></li>
		<?php } ?>
		<?php echo validation_errors('<li>','</li>'); ?>
	</ul>
	<h3>Agregar nuevo tipo de servicio</h3>
	<?php echo form_open('catalogos/agregar_tipo_servicio'); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Nombre del servicio'); ?>
			<?php echo form_input(array('name'=>'nombre','value'=>set_value('nombre'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Descripción'); ?>
			<?php echo form_input(array('name'=>'descripcion','value'=>set_value('descripcion'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Cobro mensual'); ?>
			<div><?php echo form_checkbox('cobromensual','1',FALSE); ?></div>
			
		</div>
	</div>
	
	<div>
		<?php echo form_submit(array('name'=>'agregar','value'=>'Agregar','class'=>'boton')); ?>
	</div>
	<?php echo form_close(); ?>
</div>