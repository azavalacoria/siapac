<div id="form-box">
	<ul id="list-errors">
		<?php if (isset($error)) { ?>
			<li><?php echo $error; ?></li>
		<?php } ?>
		<?php echo validation_errors('<li>','</li>'); ?>
	</ul>
	<?php echo form_open('catalogos/agregar_colonia'); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Nombre de la colonia'); ?>
		<?php echo form_input(array('name'=>'nombre','value'=>set_value('nombre'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Zona'); ?>
			<?php echo form_dropdown('zona',$zonas,set_value('zona')); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Clave de la localidad'); ?>
			<?php echo form_input(array('name'=>'localidad', 'value'=>set_value('localidad'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Código postal'); ?>
			<?php echo form_input(array('name'=>'cp','value'=>set_value('cp'))); ?>
		</div>
	</div>
	<div>
		<?php echo form_submit(array('name'=>'agregar','value'=>'Agregar','class'=>'boton')); ?>
	</div>
	<?php form_close(); ?>
</div>