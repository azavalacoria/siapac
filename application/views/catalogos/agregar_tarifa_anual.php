<div id="form-box">
	<ul id="list-errors">
		<?php if(isset($error)) { ?>
			<li><?php echo $error; ?></li>
		<?php } ?>
		<?php echo validation_errors('<li>','</li>'); ?>
	</ul>
	<h3>Agregar nueva tarifa</h3>
	<?php echo form_open('catalogos/agregar_tarifa_anual'); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Zona'); ?>
			<?php echo form_dropdown('zona',$zonas,set_value('zona')); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Tipo de servicio'); ?>
			<?php echo form_dropdown('servicio',$servicios,set_value('servicio')); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Periodo inicial'); ?>
			<?php echo form_input(array('name'=>'fechainicial','id'=>'periodoinicial','value'=>set_value('fechainicial'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Periodo final'); ?>
			<?php echo form_input(array('name'=>'fechafinal','id'=>'periodofinal','value'=>set_value('fechafinal'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Precio'); ?>
			<?php echo form_input(array('name'=>'precio','value'=>set_value('precio'))); ?>
		</div>
	</div>
	<!-- 
	 -->
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Cargo al contratar'); ?>
			<?php
			if (isset($_POST['cobroalcontratar'])) {
				echo form_checkbox('cobroalcontratar','1',TRUE);
			} else {
				echo form_checkbox('cobroalcontratar','1',FALSE);
			}
			?>
		</div>
	</div>
	<div>
		<?php echo form_submit(array('name'=>'agregar','value'=>'Agregar','class'=>'boton')); ?>
	</div>
	<?php echo form_close(); ?>
</div>
<?php echo link_tag('css/jquery-ui.css'); ?>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.start.js'); ?>"></script>