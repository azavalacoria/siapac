<div id="form-box">
	<h3>Modificar Zona</h3>
	<?php form_open('catalogos/actualizar_zona_colonia'); ?>
	<div class="form-row">
		<div class="normal-row">
			<label>Zona</label>
			<select name="zona" id="zona">
				<option value="0">Seleccionar</option>
			<?php foreach ($zonas as $z) { ?>
				<option value="<?php echo $z->idzona; ?>">
					<?php echo $z->nombrezona ?>
				</option>
			<?php }	?>
			</select>
		</div>
	</div>
	<?php form_close(); ?>
	<div id="box">
		<div id="colonias">
			<form>

			</form>
		</div>
		<div id="boton-container">
			<button id="submit">Cambiar de Zona</button>
		</div>
		<div id="zonas">
			<form>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
$('#zona').change(
	function () {
		zona = $('#zona').val();
		if (zona == 0) {
			alert('Debe seleccionar una zona.');
		} else {
			$.ajax({
				url: "<?php echo site_url('peticiones_ajax/obtener_colonias'); ?>",
				async: false,
				type: "POST",
				data: 'zona='+zona,
				dataType: "json",
				success: function (datos) {
					if (datos.encontradas == 0) {
						alert("no hay nada");
					} else {
						$('#colonias form').empty();
						$.each(datos, function (i, colonia) {
							contenedor = '<div id="'+colonia.id+'">'
							input = '<input type="radio" name="colonia" value="'+colonia.id+'" group="colonias">';
							nombre = colonia.nombre + '<br> </div>'
							$('#colonias form').append(contenedor + input + nombre);
						});
					}
				}
			});
			$.ajax({
				url: "<?php echo site_url('peticiones_ajax/obtener_zonas_restantes'); ?>",
				async: false,
				type: "POST",
				data: "zona="+zona,
				dataType: "json",
				success: function (datos) {
					if (datos.encontrados == 0) {
						alert("error");
					} else {
						$('#zonas form').empty();
						$.each(datos.zona, function (i, zona) {
							contenedor = '<div id="'+zona.id+'">'
							input = '<input type="radio" name="zona" value="'+zona.id+'" group="zonas">';
							nombre = zona.nombre + '<br> </div>'
							$('#zonas form').append(contenedor + input + nombre);
						});
					}
				}
			});
		}
	}
);
</script>
<script type="text/javascript">
$('#submit').click(function () {
	colonia = $("#colonias input:checked").val();
	zona = $("#zonas input:checked").val();
	
	if (typeof(zona) === "undefined" || typeof(colonia) === "undefined") {
		alert("Error, no ha selecionado una zona o colonia.")
	} else {
		$.ajax({
			url: "<?php echo site_url('peticiones_ajax/actualizar_zona_colonia') ?>",
			async: false,
			type: "POST",
			data: "idcolonia=" + colonia + "& zona=" + zona,
			dataType: "json",
			success: function (datos) {
				if (datos.afectadas >= 1) {
					elemento = "#" + $('#colonias input:checked').val();
					$(elemento).empty();
					$(elemento).attr('class','off');
				} else {
					alert("Hubo un error al actualizar los datos \n Intente en otro momento");
				}
			}
		});
	}
});
</script>