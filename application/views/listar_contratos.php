<div id="head-sale">
	<div> <ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul> </div>
	<div>
		<span>
			Enlistando <?php echo $this->session->userdata('inicio'); ?> de <?php echo $this->session->userdata('limite'); ?>
		</span>
	</div>
	<?php echo form_open("$destino"); ?>
	<table>
		<thead>
			<th>Número de contrato</th>
			<th>Nombre del contribuyente</th>
			<th>Domicilio</th>
			<th>Último pago</th>
		</thead>
		<tbody>
			<?php foreach ($contratos as $contrato) { ?>
				<tr>
					<td class="centered">
						<?php echo form_radio(array('name'=>'contrato','value'=>$contrato->numero)) ?>
						<?php echo $contrato->numero; ?>
					</td>
					<td> 
						<?php printf("%s %s %s", $contrato->apellidopaterno, $contrato->apellidomaterno, $contrato->nombres); ?>
					</td>
					<td>
						<?php printf("Calle %s, No. %s %s. Colonia %s", 
						$contrato->calle, $contrato->numeroexterior, $contrato->numerointerior, $contrato->nombrecolonia);?> 
					</td>
					<td class="centered"> <?php echo $contrato->ultimopago; ?> </td>
				</tr>
			<?php }?>
		</tbody>
	</table>
	<div id="select-buttons">
		<span>
			<?php echo form_button(array('name'=>'elegir','value'=>'on','type'=>'submit','content'=>'Elegir')); ?>
		</span>
		
		<?php echo form_close(); ?>
	<?php if (isset($busqueda_nueva)): ?>
		<?php echo form_open($busqueda_nueva); ?>
		<span>
			<?php echo form_button(array('name'=>'opcion','value'=>'sig','content'=>'Siguientes','type'=>'submit')); ?>
		</span>
		<?php echo form_close(); ?>
	</div>
	<?php endif ?>
</div>