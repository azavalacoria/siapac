<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/contrato.css'); ?>">

</head>
<body class="fondo">
	<div class="hoja" style="">
		<div id="contrato" style="text-align:left;">
			<?php 
			if (isset($numero)) {
				echo 'C. '.strtoupper($nombrecompleto);
				//echo '<br>CONTRATO: '.strtoupper($numero);
				echo '<br><strong>PRESENTE</strong>';
			} else {
				echo "CALK/000";
			} ?>
		</div>
		
		<p id="presentacion">
			
				EN ATENCIÓN A SU OFICIO CON FECHA <strong><?php echo strtoupper($fechabaja); ?></strong> EN DONDE NOS SOLICITA LA CANCELACIÓN 
				DE SU TOMA TIPO <strong><?php echo strtoupper($servicio); ?></strong> UBICADO EN <strong><?php echo strtoupper($direccion); ?></strong>, 
				POR EL MOTIVO DE <strong><?php echo strtoupper($motivobaja); ?></strong>, LE INFORMO QUE SU SOLICITUD HA SIDO ATENDIDA Y SU 
				SERVICIO DE AGUA CON NUMERO DE CONTRATO <strong><?php echo strtoupper($numero); ?></strong>  HA SIDO  CANCELADO FÍSICAMENTE Y
				DE LA BASE DE DATOS.
		</p>
		<br><br><br><br>
		<div id="firmas">
			<table id="firmas-tabla">
				<tr>
					<th>"El usuario"</th>
					<th></th>
					<th>SMAPCALK</th>
				</tr>
				<tr>
					<td><hr></td>
					<td></td>
					<td><hr></td>
				</tr>
				<tr>
					<td id="el-usuario">
						<?php 
						if (isset($nombrecompleto)) {
							echo strtoupper($nombrecompleto);
						} else {
							echo "El solicitante";
						}
						?>
					</td>
					<td id="separador"></td>
					<td id="el-smapcal">
						JUAN PABLO COLLÍ DZUL
						<br>
						Director
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>