<div id="form-box">
	<h3>Nuevo Contribuyente</h3>
	<?php echo link_tag('css/jquery-ui.css'); ?>
	<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min-1.10.3.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('js/jquery-ui.fechanacimiento.js'); ?>"></script>
	<div>
		<ul id="list-errors"><?php echo validation_errors('<li>','</li>'); ?></ul>
		
	</div>
	<?php
	if (isset($_POST['fechanacimiento'])) {
		$fechanacimiento = $_POST['fechanacimiento'];
	} else {
		$fechanacimiento = '';
	}
	?>
<hr>
<?php echo form_open('nuevo_contribuyente/agregar') ?>
	
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label("Nombres"); ?>
			<?php echo form_input(array('name'=>'nombres', 'id'=>'nombres', 'value'=>set_value('nombres'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Apellido Paterno'); ?>
			<?php echo form_input(array('name'=>'apellidopaterno','value'=>set_value('apellidopaterno'))); ?>
		</div>
		<div class="normal-row">
			<?php //echo form_label('Apellido Materno'); ?>
			<label>Apellido Materno</label>
			<?php echo form_input(array('name'=>'apellidomaterno','value'=>set_value('apellidomaterno'))); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Fecha Nacimiento'); ?>
			<?php echo form_input(array('name'=>'fechanacimiento','value'=> set_value('fechanacimiento'),'id'=>'fechanacimiento' )); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('RFC'); ?>
			<?php echo form_input(array('name'=>'rfc', 'value'=>set_value('rfc'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Clave Elector');?>
			<?php echo form_input(array('name'=>'claveelector','value'=>set_value('claveelector')));?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Colonia'); ?>
			<?php echo form_input(array('name'=>'colonia','value'=>set_value('colonia'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Calle'); ?>
			<?php echo form_input(array('name'=>'calle','id'=>'calle','value'=>set_value('calle'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Número'); ?>
			<?php echo form_input(array('name'=>'numero','id'=>'num-int','value'=>set_value('numero'))); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Localidad'); ?>
			<?php echo form_input(array('name'=>'localidad','value'=>set_value('localidad'))); ?>
		</div>
	</div>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Núm telefónico'); ?>
			<?php echo form_input(array('name'=>'telefono', 'value'=>set_value('telefono'))) ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Celular'); ?>
			<?php echo form_input(array('name'=>'celular', 'value'=>set_value('celular'))) ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Correo electrónico'); ?>
			<?php echo form_input(array('name'=>'email', 'value'=>set_value('email'))) ?>
		</div>
	</div>
	<?php echo form_submit(array('name'=>'agregar','value'=>'Agregar','id'=>'submit')); ?>
	<?php echo form_close(); ?>
<hr>
<script>

$('#nombres').focus();

$(function () {
	$('#submit').click(function () {
		confirmacion = confirm("¿Desea agregar el contribuyente?");
		if (confirmacion) {
			$('#submit').attr('type','submit');
			$('#contribuyente').submit();	
		} else {
			$('#list-errors').append('<li>Cancelado por el usuario</li>');
		}
		
	});
});
</script>
</div>