<?php header("Content-type: text/xml", "charset=utf-8"); ?>
<?php echo '<?'.'xml version="1.0" standalone="yes" '.'?>'; ?>
<?php $cc = 0; ?>

<contratos>
	<?php foreach ($contratos as $contrato) {	?>
	<contrato>
		<?php $cc++; ?>
		<contador><?php echo $cc ?></contador>
		<numero><?php echo $contrato['numero']; ?></numero>
		<ultimo_pago><?php echo $contrato['ultimopago']; ?></ultimo_pago>
		<?php if (sizeof($contrato['recibos']) > 0) { ?>
		<recibos>
			<?php foreach ($contrato['recibos'] as $recibo) { ?>
			<recibo>
				<folio><?php echo $recibo['idrecibo']; ?></folio>
				<fecha_cobro><?php echo $recibo['fechacobro']; ?></fecha_cobro>
				<periodo><?php echo $recibo['periodo']; ?></periodo>
				<subtotal><?php echo $recibo['subtotal']; ?></subtotal>
				<descuento><?php echo $recibo['descuento']; ?></descuento>
				<total><?php echo $recibo['total']; ?></total>
				<observaciones><?php echo $recibo['observaciones']; ?></observaciones>
				<?php  if (sizeof($recibo['items']) > 0 ) { ?>
				<items>
					<?php foreach ($recibo['items'] as $item) { ?>
					<item>
						<periodo><?php echo $item['periodo']; ?></periodo>
						<cantidad><?php echo $item['cantidad']; ?></cantidad>
						<concepto><?php echo $item['concepto']; ?></concepto>
						<precio_unitario><?php echo $item['preciounitario']; ?></precio_unitario>
						<importe><?php echo $item['importe']; ?></importe>
						<armonizacion_uno><?php echo $item['armonizacionuno']; ?></armonizacion_uno>
						<armonizacion_dos><?php echo $item['armonizaciondos']; ?></armonizacion_dos>
					</item>
					<?php } ?>
				</items>
				<?php } ?>
			</recibo>
			<?php } ?>
		</recibos>
		<?php } ?>
	</contrato>
	<?php } ?>
</contratos>

