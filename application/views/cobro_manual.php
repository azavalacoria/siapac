<input type="hidden" value="<?php echo site_url('cobro_parcial'); ?>" id="siteurl">
<div>
	<ul id="list-errors">
		<?php
		$errores_periodo = $this->session->userdata('errores_periodo');
		if (isset($errores_periodo)) {
			echo $errores_periodo;
		?>
		
		<?php }
		?>
	</ul>
</div>
<div id="form-comp">

	<div id="head-sale">
		<table>
			<thead>
				<tr>
				<th>No. de Contrato</th>
				<th>Nombre Completo</th>
				<th>Domicilio</th>
				<th>Último Pago</th>
			</tr>
			</thead>
			<tbody>
				<tr>
				<td class="centered"><?php echo $this->session->userdata('numero'); ?></td>
				<td class="centered"><?php echo $this->session->userdata('nombrecompleto'); ?></td>
				<td class="centered"><?php echo $this->session->userdata('domicilio'); ?></td>
				<td class="centered"><?php echo $this->session->userdata('ultimopago'); ?></td>
			</tr>
			</tbody>
		</table>
	</div>
	<div id="items">
		<table>
			<thead>
				<!--<th class="opt">Opción</th>-->
				<th class="period">Periodo</th>
				<th class="number-cell">Cantidad</th>
				<th>Concepto</th>
				<th class="number-cell">Precio</th>
				<th class="number-cell">Importe</th>
			</thead>
			<tbody>
				<?php foreach ($items as $i) { ?>
				<tr>
					<!--<td class="centered"><?php //echo form_radio(array('name'=>'item','value'=>$i->iditem)); ?></td>-->
					<td class="centered"><?php echo $i->periodo; ?></td>
					<td class="centered"><?php echo $i->cantidad; ?></td>
					<td style="number-content"><?php echo $i->concepto; ?></td>
					<td class="number-content"><?php echo $i->preciounitario; ?></td>
					<td class="number-content"><?php echo $i->importe; ?></td>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
	<div id="totals">
		<?php echo form_open('cobro_parcial/editar'); ?>
			<table>
				<tr>
					<th>Subtotal</th>
					<td class="total-number"><?php printf("$ %.2f", $subtotal); ?></td>
				</tr>
				<tr>
					<th>Bonificación</th>
					<td class="total-number"><?php printf("$ %.2f", $descuento); ?></td>
				</tr>
				<tr>
					<th>Subtotal</th>
					<td class="total-number"><?php printf("$ %.2f", ($subtotal - $descuento));; ?></td>
				</tr>
			</table>
		</div>
	<div id="form-box">
		<div style="float: left; padding-bottom: 15px">
			
			<?php echo form_button(array('name'=>'accion','value'=>'agregar','type'=>'submit','content'=>'Agregar Periodo'));?>

			<span>
				<?php 
				if (count($items) > 0) {
					echo form_button(array('name'=>'accion','value'=>'bonificar','type'=>'submit','content'=>'Agregar bonif.'));
				}
				?>
			</span>
			<?php echo nbs(3); ?>
			<?php //echo form_button(array('name'=>'accion','value'=>'eliminar','type'=>'submit','content'=>'Eliminar Item'));?>
			<?php echo form_close(); ?>
		</div>
		<div style="float: right">
			<?php echo form_open('cobro_parcial',array('id'=>'pagarrecibo')); ?>
			<?php
			if (strlen($errores_periodo) == 0) {
				echo form_button(array('name'=>'accion','id'=>'pagar' , 'value'=>'pagar','content'=>'Cobrar'));
			}
			?>
			<?php echo nbs(3); ?>
			<?php echo form_button(array('name'=>'accion','value'=>'cancelar','id'=>'cancelar', 'content'=>'Cancelar'));?>
			<?php echo form_close(); ?>
		</div>
	</div>

</div>
<script type="text/javascript" src="<?php echo base_url('js/cobros-contrato.js') ?>"></script>