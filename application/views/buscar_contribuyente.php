<div id="form-box">
	<h3>Buscar contribuyente en el padrón</h3>
	<ul id="list-errors">
		<?php echo validation_errors('<li>','</li>') ?>
	</ul>
	<?php echo form_open('nuevo_contrato/buscar_contribuyente'); ?>
	<div class="form-row">
		<div class="normal-row">
			<?php echo form_label('Apellido Paterno'); ?>
			<?php echo form_input(array('name'=>'apellidopaterno', 'id'=>'apellidopaterno', 'value'=>set_value('apellidopaterno'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Apellido Materno') ?>
			<?php echo form_input(array('name'=>'apellidomaterno','value'=>set_value('apellidomaterno'))); ?>
		</div>
		<div class="normal-row">
			<?php echo form_label('Nombres') ?>
			<?php echo form_input(array('name'=>'nombres','value'=>set_value('nombres'))) ?>
		</div>
		<div>
			<?php echo form_submit(array('name'=>'buscar','value'=>'Buscar','id'=>'submit')) ?>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
	
<script type="text/javascript">
$('#apellidopaterno').focus();
</script>